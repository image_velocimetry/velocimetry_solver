########################
       F-PyIV
########################
G.Bodart 	    
------------------------
v0.2		    2024 
-------------------------
For support feel free to contact me at guillaume.bodart@edf.fr

FPyV is an object-oriented library used to extract 
and play with Fudaa-LSPIV measurements in Python

# REQUIREMENTS 
--------------
The following libraries are needed : numpy, tqdm, opencv-python, scipy

pip install numpy tqdm opencv-python scipy

