#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#######################################
####    FPyV -- VELOCITIES MODULE
#######################################
Python LSPIV tools

Contains all definitions/functions that deals with 
velocities, from determination to representation 

"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import FPyV.transform 
from tkinter.filedialog import askdirectory,askopenfilename
from tkinter import Tk
from scipy.interpolate import griddata
from tqdm import tqdm 
import cv2 as cv
import os 
import scipy

class vel:
        """ Velocity class """
        
        def __init__(self,nS = 1,nP = 0):
            # Image Coordinate
            self.i     = np.zeros((nS,nP), dtype = int) 
            self.j     = np.zeros((nS,nP), dtype = int)
            self.di    = np.zeros((nS,nP), dtype = float) # Displacement
            self.dj    = np.zeros((nS,nP), dtype = float)
            self.vi    = np.zeros((nS,nP), dtype = float) # Velocity 
            self.vj    = np.zeros((nS,nP), dtype = float)
            self.nij   = np.zeros((nS,nP), dtype = float) # Norm
            # World Coordinate
            self.x     = np.zeros((nS,nP), dtype = float)
            self.y     = np.zeros((nS,nP), dtype = float)
            self.dx    = np.zeros((nS,nP), dtype = float) # Displacement
            self.dy    = np.zeros((nS,nP), dtype = float)
            self.vx    = np.zeros((nS,nP), dtype = float) # Velocity 
            self.vy    = np.zeros((nS,nP), dtype = float)
            self.n     = np.zeros((nS,nP), dtype = float) # Norm 
            self.theta = np.zeros((nS,nP), dtype = float) # Angle 
            # Correlation (from piv algorithm)
            self.r     = np.zeros((nS,nP), dtype = float)
            self.Ar    = np.zeros((nS,nP), dtype = float) # Area > 0.8r_peak
            
        def copy(self):
            
            out = vel()
            # Image Coordinate
            out.i     = self.i.copy() 
            out.j     = self.j.copy() 
            out.di    = self.di.copy()  # Displacement
            out.dj    = self.dj.copy() 
            out.vi    = self.vi.copy()  # Velocity 
            out.vj    = self.vj.copy() 
            out.nij   = self.nij.copy()  # Norm
            # World Coordinate
            out.x     = self.x.copy() 
            out.y     = self.y.copy() 
            out.dx    = self.dx.copy()  # Displacement
            out.dy    = self.dy.copy() 
            out.vx    = self.vx.copy()  # Velocity 
            out.vy    = self.vy.copy() 
            out.n     = self.n.copy()  # Norm 
            out.theta = self.theta.copy()  # Angle 
            # Correlation (from piv algorithm)
            out.r     = self.r.copy()
            if hasattr(self,'Ar'):
                out.Ar    = self.Ar.copy()
            # Crms value (contrast in IA)
            #out.crms  = self.crms.copy()
            
            return out
        
        def remove_filter(self):
            """ Remove filtered velocities from list """
            
            f          = np.logical_and(self.vx == 0, self.vy == 0)
            self.i     = np.delete(self.i,f)
            self.j     = np.delete(self.j,f)
            self.di    = np.delete(self.di,f)
            self.dj    = np.delete(self.dj,f)
            self.vi    =  np.delete(self.vi,f)
            self.vj    =  np.delete(self.vj,f)
            self.nij   =  np.delete(self.nij,f)
            self.x     =  np.delete(self.x,f)
            self.y     =  np.delete(self.y,f)
            self.dx    =  np.delete(self.dx,f)
            self.dy    =  np.delete(self.dy,f)
            self.vx    =  np.delete(self.vx,f)
            self.vy    =  np.delete(self.vy,f)
            self.n     =  np.delete(self.n,f)
            self.theta =  np.delete(self.theta,f)
            self.r     =  np.delete(self.r,f)
            self.crms  =  np.delete(self.crms,f)
            
            return
        
        def filter(self, qty, thresMin, thresMax):
            """ Apply filter on velocities """
            dictVel    = {"di"      : self.di,
                          "dj"      : self.dj,
                          "x"       : self.x,
                          "y"       : self.y,
                          "vi"      : self.vi,
                          "vj"      : self.vj,
                          "nij"     : self.nij,
                          "dx"      : self.dx,
                          "dy"      : self.dy,
                          "vx"      : self.vx,
                          "vy"      : self.vy,
                          "n"       : self.n,
                          "theta"   : self.theta,
                          "r"       : self.r,
                          "Ar"      : self.Ar}
            dictFiltre = {"di"      : self.di,
                          "dj"      : self.dj,
                          "vi"      : self.vi,
                          "vj"      : self.vj,
                          "nij"     : self.nij,
                          "dx"      : self.dx,
                          "dy"      : self.dy,
                          "vx"      : self.vx,
                          "vy"      : self.vy,
                          "n"       : self.n,
                          "theta"   : self.theta,
                          "r"       : self.r,
                          "Ar"      : self.Ar}
            # Checkings 
            if not qty in ["Ar","x","y","di","dj","vi","vj","nij","dx","dy","vx","vy","n","theta","r"]:
                   print("ERROR: qty isn't correct. expected : 'di','dj','vi','vj','nij','dx','dy','vx','vy','n','theta','r','Ar','x','y'")
                   return 1 
            # Apply condition and get logical mask
            f = np.logical_or(dictVel[qty] < thresMin,dictVel[qty] > thresMax)
            # Apply filter to each qty 
            for i in dictFiltre:
                if np.shape(dictFiltre[i])== np.shape(f):
                    dictFiltre[i][f] = np.nan
                    
            return 0 
         
        def medianTest(self, dist_neighbor=10,nb_neighbor = 9, epsilon=0.1, thres=2,dest = "world",treeQuery = False):
            """
            Apply median test (Westerweel and Scarano 2005) to remove spurious vectors. 
            KD-Tree algorithm (from scipy) is used to identify the neighboors of each points. 
            See help of kdTree function from scipy for more info.

            Parameters
            ----------
            dist_neighbor : int, optional
                The maximum distance (in [m]) to consider a point as neighbor. The default is 10.
            
            nb_neighbor : int, optional
                Defines the maximum number of points identified as neighbor (n) such as n = nb_neighbor-1.
                A value of 9 ensures that 8 neighbors will be identified.The default is 9.
           
            epsilon : float, optional
                Parameter of the median test, see (Westerweel and Scarano 2005). The default is 0.1.
            
            thres : int, optional
                 Parameter of the median test, see (Westerweel and Scarano 2005). The default is 2.
            
            direction : TYPE, optional
                TODO... The default is None.

            Returns
            -------
            int
                DESCRIPTION.

            """
            if dest == "world":
                grid    = np.array([self.x[:,0], self.y[:,0]]).T
            else:
                grid    = np.array([self.i[:,0], self.j[:,0]]).T
                          
            # Use KDTree to identify neighbors
                # Optimization : the tree is passed as argument 
            if isinstance(treeQuery,bool):
                kdTree = scipy.spatial.KDTree(grid)
                res = kdTree.query(grid,nb_neighbor,distance_upper_bound = dist_neighbor)
            else:
                res = treeQuery
                
                
            # Arrange grid and velocities in 2D arrays
            for k in tqdm(range(np.shape(self.x)[1])):
                if dest == "world":
                    vel     = np.array([self.vx[:,k].copy(),self.vy[:,k].copy()]).T
                else:
                    vel     = np.array([self.di[:,k].copy(),self.dj[:,k].copy()]).T

                # Look for neighbors 
                for ptInd,distInd in zip(res[1],res[0]):
                    # v0, velocity at point
                    v0x,v0y = vel[ptInd[0],0],vel[ptInd[0],1]
                    # Store neighbors velocities 
                    vx,vy = [],[]
                    for el in ptInd:
                        if el < len(grid):
                            vx.append(vel[el,0])
                            vy.append(vel[el,1])
                    vx = np.array(vx)
                    vy = np.array(vy)
                    
                    
                    # X 
                    # ####
                    # Calculate ref velocity
                    vxr = np.nanmedian(vx)
                    # Calculate residual
                    rix = []
                    for el in ptInd:
                        if el < len(grid):
                            rix.append(vel[el,0]-vxr) 
                    rix = np.array(rix)
                    # Normalize target residual 
                    r0x = np.abs(v0x-vxr)/(np.nanmedian(rix)+epsilon)
    
        
                    # Y 
                    # ####
                    # Calculate ref velocity
                    vyr = np.nanmedian(vy)
                    # Calculate residual
                    riy = []
                    for el in ptInd:
                        if el < len(grid):
                            riy.append(vel[el,1]-vyr) 
                    riy = np.array(riy)
                    # Normalize target residual 
                    r0y = np.abs(v0y-vyr)/(np.nanmedian(riy)+epsilon)
                    
                    # Filter data
                    if r0x > thres or r0y > thres:
                        self.di[ptInd[0],k] = np.nan
                        self.dj[ptInd[0],k] = np.nan
                        if len(self.dx) > 0:
                            self.dx[ptInd[0],k] = np.nan
                            self.dy[ptInd[0],k] = np.nan
                            self.vx[ptInd[0],k] = np.nan
                            self.vy[ptInd[0],k] = np.nan
                            self.n[ptInd[0],k] = np.nan
                        self.r[ptInd[0],k]  = np.nan
                    
            return 0 
          
        def streamlines(self,   
                       ax      = None,
                       cmap    = "viridis"):
       
           ## Init
           x,y,u,v = self.x[:,0], self.y[:,0], self.vx[:,0],self.vy[:,0]
           
           # interpolation on a regular grid 
           xx      = np.linspace(x.min(),x.max(),int((x.max()-x.min())*50))
           yy      = np.linspace(y.min(),y.max(),int((y.max()-y.min())*50))
       
           xy      = np.meshgrid(xx,yy)    # Construction grille 
           vel     = griddata(np.stack((x,y),axis=-1), 
                              np.stack((u,v),axis=-1),
                              np.stack(xy,axis=-1),
                              'nearest')   # Interpolation des résultats lspiv (u,v) sur la nouvelle grille 

           # Plot streamlines 
           if isinstance(ax,type(None)):
               fig,ax  = plt.subplots()
           
           ax.streamplot(xx,
                         yy,
                         vel[:,:,0],
                         vel[:,:,1],
                         density = 1.25,
                         color = "k",
                         linewidth=0.5,
                         minlength=0.1,
                         maxlength=10.0)
           norm    = np.sqrt(vel[:,:,0]**2+vel[:,:,1]**2)
           CN      = ax.contourf(xx,
                                 yy,
                                 norm,
                                 np.linspace(np.nanmin(norm),np.nanmax(norm),70),
                                 cmap = cmap,
                                 alpha = 0.5)
           ax.grid(False)
           plt.colorbar(CN,ax=ax,label="Norme [m/s]")
           ax.set_ylabel("$h$ [m]")
           ax.set_xlabel("$l$ [m]")
           plt.tight_layout()
           
           return ax
           
        def arrow2D(self,
                    pos         = 0,
                    plot        = "vXY", 
                    title       = "", 
                    xlbl        = "", 
                    ylbl        = "", 
                    ax          = None, 
                    color       = "red",
                    cmapData    = "norm",
                    cmap        = "viridis", 
                    scale       = 55,
                    width       = 0.0045,
                    headwidth   = 4, 
                    lw          = 0.15,
                    ec="k",
                    **param_dict):
            """
            Plot the data. Option plot allow to choose the quantity to be ploted: velocity (v) or displacement (d) and the units: world (XY) or image (IJ)
    
            Parameters
            ----------
                - plot [str (len:3)], optional
                    Data to be ploted (length = 3char). 
                    First indicates the quantity: velocity (v) or displacement (d)
                    After indicates the units: meters (XY) or pixels (IJ)
                    The default is "vXY"
                    
                - title [str], optional
                    Title of the plot. 
                    The default is ""
                    
                - xlbl [str], optional
                    X label of the plot. 
                    The default value is ""
                    
                - ylbl [str], optional
                    Y label of the plot. 
                    The default value is ""
                    
                - ax [plt.ax], optional
                    ax where the data will be ploted. If None then a new plot will be create 
                    The default is None.
                    
                - color [str], optional
                    Color of the plot. If cmap is given then cmap will be applied instead
                    The default is "red".
                
                - cmap [str], optional
                    cmap applied to the data. 
                    The default is None.
                
                - scale [int], optional
                    Scale value used for arrows. If None then scale is automatic
                    The default is None.
    
            Returns
            -------
                - qv [ax.quiver or ax.scatter]
                    Plot of the data
                - errno [int]
                    Error number :
                        - 1 - input problem 
                    
    
            """
            # ---- CHECK INPUTS ----
            # label of colorbar
            if "lblClbr" in param_dict.keys():
                lblClbr = param_dict["lblClbr"]
                del param_dict["lblClbr"]
            else:
                lblClbr = "Velocity [m/s]"
            # label of colorbar
            if "vmin" in param_dict.keys():
                vmin = param_dict["vmin"]
                del param_dict["vmin"]
            else:
                vmin = None
            # label of colorbar
            if "vmax" in param_dict.keys():
                vmax = param_dict["vmax"]
                del param_dict["vmax"]
            else:
                vmax = None
            # first check on nS,nP 
            if len(plot)!=3:
                print("\nERROR: wrong format of variable 'plot'. See help file for more information.")
                return 1
            # then check contents
            mtype       = plot[:1].lower() # Case sensitivity 
            maxes       = plot[1:].lower()
            
            if np.logical_not(mtype in ["v","d"]) or np.logical_not(maxes in ["xy","ij"]):
                print("\nERROR: wrong format of variable 'plot'. See help file for more information.")
                return 1
            
            if not isinstance(cmapData,str) and len(cmapData) != len(self.x):
                print("\nERROR : nS,nP of cmapData doesn't match data.")
                return 1 
            
            # check axes  
            if ax == None: # Creates figure and ax
                fig = plt.figure()
                ax = fig.add_subplot(111)
                plt.axis("equal")
                ax.set_title(title)
                ax.set_xlabel(xlbl)
                ax.set_ylabel(ylbl)
                
            # if type(ax) != matplotlib.axes._subplots.Subplot:
            #     print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            #     return 1
            # ---- PLOT ----
            if mtype == "v":
                if maxes == "xy":
                    # Compute norm     
                    norm = self.n[:,pos]
                    if cmap == None: 
                        try:
                            qv = ax.quiver(self.x[:,pos],
                                           self.y[:,pos],
                                           self.vx[:,pos],
                                           self.vy[:,pos],
                                           color        = color,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           lw           = lw,
                                           ec           = ec,
                                           **param_dict)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
                    else:
                        if cmapData == 'norm':
                            cmapData = norm
                        elif cmapData == "r":
                            cmapData = self.r[:,pos]
                        try: 
                            if vmin == None:
                                vmin = cmapData[np.isfinite(cmapData)].min()
                            if vmax == None:
                                vmax = cmapData[np.isfinite(cmapData)].max()
                            qv = ax.quiver(self.x[:,pos],
                                           self.y[:,pos],
                                           self.vx[:,pos],
                                           self.vy[:,pos],
                                           cmapData,
                                           cmap         = cmap,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           clim         = (vmin,vmax),
                                           **param_dict)
                            
                            mapp = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin = vmin,vmax = vmax),cmap = cmap)
                            # Add colorbar 
                            plt.colorbar(mapp,ax=ax,label = lblClbr)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
                else:
                    # Compute norm     
                    norm = self.n[:,pos]
                    if cmap == None: 
                        try:
                            qv = ax.quiver(self.j[:,pos],
                                           self.i[:,pos],
                                           self.vx[:,pos],
                                           self.vy[:,pos],
                                           color        = color,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           **param_dict)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
                    else:
                        if cmapData == 'norm':
                            cmapData = norm
                        elif cmapData == "r":
                            cmapData = self.r[:,pos]
                            cmapData[cmapData == -99] = np.nan
                        try: 
                            if vmin == None:
                                vmin = cmapData[np.isfinite(cmapData)].min()
                            if vmax == None:
                                vmax = cmapData[np.isfinite(cmapData)].max()
                            qv = ax.quiver(self.j[:,pos],
                                           self.i[:,pos],
                                           self.vx[:,pos],
                                           self.vy[:,pos],
                                           cmapData,
                                           cmap         = cmap,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           clim         = (vmin,vmax),
                                           **param_dict)
                            if vmin == None:
                                vmin = cmapData[np.isfinite(cmapData)].min()
                            if vmax == None:
                                vmax = cmapData[np.isfinite(cmapData)].max()
                            mapp = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin = vmin,vmax = vmax),cmap = cmap)
                            # Add colorbar 
                            plt.colorbar(mapp,ax=ax,label = lblClbr)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
            else:
                if maxes == "xy":
                    # Compute norm     
                    norm = self.n[:,pos]
                    if cmap == None: 
                        try:
                            qv = ax.quiver(self.x[:,pos],
                                           self.y[:,pos],
                                           self.dx[:,pos],
                                           self.dy[:,pos],
                                           norm,
                                           color        = color,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           **param_dict)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
                    else:
                        if cmapData == 'norm':
                            cmapData = norm
                        elif cmapData == "r":
                            cmapData = self.r[:,pos]
                            cmapData[cmapData == -99] = np.nan
                        try: 
                            if vmin == None:
                                vmin = cmapData[np.isfinite(cmapData)].min()
                            if vmax == None:
                                vmax = cmapData[np.isfinite(cmapData)].max()
                            qv = ax.quiver(self.x[:,pos],
                                           self.y[:,pos],
                                           self.dx[:,pos],
                                           self.dy[:,pos],
                                           cmapData,
                                           cmap         = cmap,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           clim         = (vmin,vmax),
                                           **param_dict)
                           
                            mapp = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin = vmin,vmax = vmax),cmap = cmap)
                            # Add colorbar 
                            plt.colorbar(mapp,ax=ax,label = lblClbr)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
                else:
                    # Compute norm     
                    norm = self.n[:,pos]
                    if cmap == None: 
                        try:
                            qv = ax.quiver(self.j[:,pos],
                                           self.i[:,pos],
                                           self.dj[:,pos],
                                           self.di[:,pos],
                                           color        = color,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           **param_dict)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2
                    else:
                        if cmapData == 'norm':
                            cmapData = norm
                        elif cmapData == "r":
                            cmapData = self.r[:,pos]
                            cmapData[cmapData == -99] = np.nan
                        try: 
                            if vmin == None:
                                vmin = cmapData[np.isfinite(cmapData)].min()
                            if vmax == None:
                                vmax = cmapData[np.isfinite(cmapData)].max()
                            qv = ax.quiver(self.j[:,pos],
                                           self.i[:,pos],
                                           self.dj[:,pos],
                                           self.di[:,pos],
                                           cmapData,
                                           cmap         = cmap,
                                           scale        = scale,
                                           scale_units  = "width",
                                           width        = width,
                                           headwidth    = headwidth,
                                           clim         = (vmin,vmax),
                                           **param_dict)
                            
                            mapp = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin = vmin,vmax = vmax),cmap = cmap)
                            # Add colorbar 
                            plt.colorbar(mapp,ax=ax,label = lblClbr)
                        except ValueError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except TypeError as e:
                            print("\nERROR : " + str(e))
                            return 2
                        except:
                            print("\nERROR: Problems while plotting data.")
                            return 2    
            return qv
      
                
class pivParam:
    """ PIV parameters class """
    
    def __init__(self):
        # Image Coordinate
        self.SA         ={
                         'Sim' : None,
                         'Sip' : None,
                         'Sjm' : None,
                         'Sjp' : None
                         } 
        self.IA         = None
        self.dt         = None 
        self.rMaxMin    =(None,None)
        self.ninj       =(None   ,None)
        self.nMaxMin    =(None,None) # norm filter
        self.vxMaxMin   =(None,None)
        self.vyMaxMin   =(None,None)
        self.alphaCoeff = None 
        self.rxyMax     =(None,None)
        self.dxp        = None
        
    def getPivParam(self,wd_p0):
        """
        Load PIV param from a PIV_param.dat file 
        /!\ FileVersion <=2

        Parameters
        ----------
        wd_p0 : TYPE
            DESCRIPTION.

        Returns
        -------
        int
            DESCRIPTION.

        """
        # Checking
        if not isinstance(wd_p0,str):
            print("ERROR: wd_p0 isn't a string")
            return 1 
        # Ensure path is ended by a '/'
        if (wd_p0[-1:]) != "/":
            wd_p0 = wd_p0 + "/"
            
        # Open file 
        f       = open(wd_p0 + "PIV_param.dat",'r')
        buf     = f.read().split("\n")
        f.close()
        self.IA = int(buf[1])
        self.SA = {"Sim" : int(buf[3]),
                   "Sip" : int(buf[4]),
                   "Sjm" : int(buf[5]),
                   "Sjp" : int(buf[6])}
        self.dt = float(buf[8])
        self.rMaxMin    =(float(buf[10]),float(buf[12]))
        self.ninj       =(int(buf[14])  ,int(buf[15]))
        self.nMaxMin    =(float(buf[17]),float(buf[18])) # norm filter
        self.vxMaxMin   =(float(buf[20]),float(buf[21]))
        self.vyMaxMin   =(float(buf[23]),float(buf[24]))
        self.alphaCoeff = float(buf[26])
        tmp = buf[28].split()
        self.rxyMax     =(float(tmp[0]),float(tmp[1]))
        self.dxp        = float(buf[30])
        

def getGrid(file,outFormat = "image"):
     
    f      = open(file,"r")
    buf    = f.read().split()
    f.close()
        
    i,j    = [],[]
    for ind,l in enumerate(buf):
        if ind%2 == 0:
             j.append(int(l))
        else:
             i.append(int(l))
    
    i,j = np.array(i),np.array(j)    
    
    if outFormat == "image":
        f      = open(file[:-8] + "PIV_param.dat","r") 
        for i in range(14):
            buff   = f.readline()
        nj      = int(f.readline())
        ni      = int(f.readline())
        i = ni - i
        grid = np.array([i,j]).T
    elif outFormat == "fudaa":
        
        grid = np.array([i,j]).T
    
    return grid

def genRegGrid(i0,j0,i1,j1,nj,ni,save=False):
    """
    Create a regular grid on the rectangular zone delimited by i0,j0,i1,j1. 
    Grid can be exported to "grid.dat" file for Fudaa-LSPIV. 
    Units considered are pixels 

    Parameters
    ----------
        - i0 : [int]
            upper left corner, i coordinate in pixels
        - j0 : [int]
            upper left corner, j coordinate in pixels 
        - i1 : [int]
            lower right corner, i coordinate in pixels
        - j1 : [int]
            lower right corner, j coordinate in pixels 
        - nj : [int]
            number of points in j direction (equivalent to x in world units).
        - ni : [int]
            number of points in i direction (equivalent to y in world units).
        - save : [bool], optional
            If True then grid will be saved in a "grid.dat" file (in Fudaa-LSPIV format). A GUI will be open to set location of the saved file.
            The default is False.

    Returns
    -------
        - grid [npArrayInt]
        - errno : [int]
            - 1 - wrong input format
            - 2 - Problem while exporting to grid.dat 

    """
    # Checkings 
    if not (isinstance(i0,int) and isinstance(i1,int) and isinstance(j0,int) and isinstance(j1,int) and isinstance(nj,int) and isinstance(ni,int)):
        print("\nERROR: Wrong input format. Integers are expected.")
        return 1 
    
    # List points 
    ii = np.linspace(i0,i1,ni,dtype = int)
    jj = np.linspace(j0,j1,nj,dtype = int)
    # arrange in mesh grid 
    meshgrid = np.meshgrid(ii,jj,indexing="ij")
    # final grid 
    grid = np.zeros((ni,nj,2),dtype=int)
    grid[:,:,0] = meshgrid[0] 
    grid[:,:,1] = meshgrid[1]
        
    if save:
        # Ask image nS,nP to rectify i axes of Fudaa...
        print("Dimension of image on i (y)?")
        dim = int(input())
        # Copy data of grid 
        fgrid           = grid.copy()
         # Change ij indexing to xy (compatibility with Fudaa)
        fgrid[:,:,0]    = grid[:,:,1]
        fgrid[:,:,1]    = dim - grid[:,:,0]
        # Ask dir 
        Tk().withdraw()
        path = askdirectory(title = "Save grid.dat to...")
        # open file 
        try:
            f = open(path + "/grid.dat","w")
            for k in fgrid:
                tmp = k.__str__()
                tmp = tmp.replace("[","")
                tmp = tmp.replace("]","")
                f.write(tmp + "\n")
            f.close()
        except Exception as e:
            print("\nERROR: Problem while saving grid to grid.dat. \nException: " + str(e))

    return grid

def dynExpans(im):
    """
    Apply dynamic expansion on image 

    Parameters
    ----------
    im : npArray[uint8]
        Grey level image

    Returns
    -------
    imOut : npArrat[uint8]
        Expansed imaged.

    """
    mx, mn = im.max(),im.min()
    return 255*((im-mn)/(mx-mn)) 


def NCC_at(im1,im2,ii,jj,IA,Sim,Sip,Sjm,Sjp):
    """
    Perform normalized cross-correlation at point (ii, jj). IA is the side of the block to be matched, Sim-ip-jm-jp defines the searching area around point ii and jj 
    Return the correlation field. 

    Parameters
    ----------
        - im1 : [npArray(unint8)]
            Image 1
        - im2 : [npArray(unint8)]
            Image 2 
        - IA :[int]
            Side of the squarre describing the block to be matched (Interrogation Area).
        - Sim : [int]
            Length of searching area in i negative distance.
        - Sip : [int]
            Length of searching area in i positive distance.
        - Sjm : [int]
            Length of searching area in j negative distance.
        - Sjp : [int]
            Length of searching area in j positive distance.

    Returns
    -------
         - c : [npArray(float)]
             correlation field at point (ii,jj)

    """
    # ---- Crop IA and SA 
    ni,nj = np.shape(im1)
    # Define indexes of edges 
    h2      = int(IA/2)
    i1,i2   = ii-h2, ii+h2
    j1,j2   = jj-h2, jj+h2
    # Cut image
    if i1 >=0 and i2+1 < ni and j1 >=0 and j2+1 < nj and i1-Sim >=0 and j1-Sjm>=0 and i2+Sip+1 < ni and j2+Sjp+1 < nj :
        imgIA   = im1[i1:i2+1,j1:j2+1]
        imgSA   = im2[i1-Sim:i2+Sip+1,j1-Sjm:j2+Sjp+1]
        res = cv.matchTemplate(np.uint8(imgSA),np.uint8(imgIA),cv.TM_CCOEFF_NORMED)
    else:
        res = []
 
    return res

def subPix(res,Sim,Sjm):
    """
    SubPixel fit for LSPIV calculation. 

    Parameters
    ----------
    res : npArray(float)
        Correlation field obtained by template matching.
    Sim : int
        Negative bound on i of SA (Searching Area).
    Sjm : int
        Negative bound on j of SA (Searching Area)..

    Returns
    -------
    di : float
        Displacement on i with sub-pixel accuracy.
    dj : float
        Displacement on j with sub-pixel accuracy..
    r : float
        Correlation value interpolated at point (di,dj).

    """
    # Get max/min of correlation field
    min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
    # Set values
    jmax    = max_loc[0]
    imax    = max_loc[1]
    r       = max_val 
    
    ## Gaussian sub-pixel fit ---
    if jmax>0 and imax>0 and jmax < res.shape[1]-1 and imax < res.shape[0] - 1 and max_val < 1 :
        # 2D 
        dn = 0.5*( np.log(res[imax-1,jmax]) - np.log(res[imax+1,jmax]) ) / ( np.log(res[imax-1,jmax])+np.log(res[imax+1,jmax])-2*np.log(res[imax,jmax])   )
        dm = 0.5*( np.log(res[imax,jmax-1]) - np.log(res[imax,jmax+1]) ) / ( np.log(res[imax,jmax-1])+np.log(res[imax,jmax+1])-2*np.log(res[imax,jmax])   )
    elif jmax>0 and imax == 0 and jmax < res.shape[1]-1 and max_val < 1 :
        # 1D
        dm = 0.5*( np.log(res[imax,jmax-1]) - np.log(res[imax,jmax+1]) ) / ( np.log(res[imax,jmax-1])+np.log(res[imax,jmax+1])-2*np.log(res[imax,jmax])   )
        dn = 0
    elif jmax==0 and imax>0 and imax < res.shape[0] - 1 and max_val < 1 :
        # 1D
        dn = 0.5*( np.log(res[imax-1,jmax]) - np.log(res[imax+1,jmax]) ) / ( np.log(res[imax-1,jmax])+np.log(res[imax+1,jmax])-2*np.log(res[imax,jmax])   )
        dm = 0
    else:
        dm = 0
        dn = 0
        
    ## Interpolate correlation ---
    if abs(dm) < 1 and abs(dn) < 1 : 
        di = imax + dn
        dj = jmax + dm
        # 2D
        if di >= 1 and dj >=1 and imax < res.shape[0]-2 and jmax < res.shape[1]-2:
            r = cub_conv2d(di, dj, res)
        # 1D
        elif imax == 0 and dj >=1 and jmax < res.shape[1]-2:
            r = cub_conv1dx(dj, imax, res)
        # 1D
        elif jmax == 0 and di >=1 and imax < res.shape[0]-2:
            r = cub_conv1dy(di, jmax, res)
    else: 
        di = imax
        dj = jmax
        r = max_val
        
    # Correct the displacement     
    di = di-Sim
    dj = dj-Sjm
    
    return di,dj,r
                
def cub_conv2d(dn,dm,c,a = -1):
    """
    Bi-cubic interpolation of correlation at point (dn,dm). 

    Parameters
    ----------
    - dn : [float]
        Position of correlation peak (raw index).
        
    - dm : [float]
        Position of correlation peak (column index).
        
    - c : npArray[float]
        Correlation field.
        
    - a : [float]
        Constant used for bi-cubic interpolation. 
        Value used in Fudaa-LSPIV : -1 
        Typical values : -0.75 < a < -0.5

    Returns
    -------
    - r : [float]
        Correlation value at coordinate (dn,dm).

    """
    intN = int(dn)
    intM = int(dm)
    
    sum_fx = 0
    for k in [0,1,2,3]:
        for l in [0,1,2,3]:
            
            sx      = abs(intM+k-1-dm)
            if sx>=0 and sx <1:
                Cx  = (a+2)*sx**3 - (a+3)*sx**2 + 1
            elif sx >=1 and sx < 2:
                Cx  = a*sx**3 - 5*a*sx**2 + 8*a*sx - 4*a
            elif sx > 2:
                Cx  = 0
                
            sy      = abs(intN+l-1-dn)
            if sy>=0 and sy <1:
                Cy  = (a+2)*sy**3 - (a+3)*sy**2 + 1
            elif sy >=1 and sy < 2:
                Cy  = a*sy**3 - 5*a*sy**2 + 8*a*sy - 4*a
            elif sy > 2:
                Cy  = 0    
                
            sum_fx  = sum_fx + c[intN+l-1,intM+k-1] *Cx *Cy
            
    if sum_fx > 1:
        return 1
    elif sum_fx <0:
        return 0
    else:
        return sum_fx
                   
def cub_conv1dy(dn,m,c,a = -1):
    """
    cubic interpolation of correlation at point (dn,m). 

    Parameters
    ----------
    - dn : [float]
        Position of correlation peak (raw index).
        
    - m : [int]
        Position of correlation peak (column index).
        
    - c : npArray[float]
        Correlation field.
        
    - a : [float]
        Constant used for bi-cubic interpolation. 
        Value used in Fudaa-LSPIV : -1 
        Typical values : -0.75 < a < -0.5

    Returns
    -------
    - r : [float]
        Correlation value at coordinate (dn,dm).

    """
    intN = int(dn)
    
    sum_fx = 0
    for l in [0,1,2,3]:
        

        sy      = abs(intN+l-1-dn)
        if sy>=0 and sy <1:
            Cy  = (a+2)*sy**3 - (a+3)*sy**2 + 1
        elif sy >=1 and sy < 2:
            Cy  = a*sy**3 - 5*a*sy**2 + 8*a*sy - 4*a
        elif sy > 2:
            Cy  = 0    
            
        sum_fx  = sum_fx + c[intN+l-1,m]  *Cy
        
    if sum_fx > 1:
        return 1
    elif sum_fx <0:
        return 0
    else:
        return sum_fx
     
def cub_conv1dx(dm,n,c,a = -1):
    """
    Cubic interpolation of correlation at point (dn,dm). 

    Parameters
    ----------
    - n : [int]
        Position of correlation peak (raw index).
        
    - dm : [float]
        Position of correlation peak (column index).
        
    - c : npArray[float]
        Correlation field.
        
    - a : [float]
        Constant used for bi-cubic interpolation. 
        Value used in Fudaa-LSPIV : -1 
        Typical values : -0.75 < a < -0.5

    Returns
    -------
    - r : [float]
        Correlation value at coordinate (dn,dm).

    """
    intM = int(dm)
    
    sum_fx = 0
    for k in [0,1,2,3]:     
        sx      = abs(intM+k-1-dm)
        if sx>=0 and sx <1:
            Cx  = (a+2)*sx**3 - (a+3)*sx**2 + 1
        elif sx >=1 and sx < 2:
            Cx  = a*sx**3 - 5*a*sx**2 + 8*a*sx - 4*a
        elif sx > 2:
            Cx  = 0
                         
        sum_fx  = sum_fx + c[n,intM+k-1] *Cx
            
    if sum_fx > 1:
        return 1
    elif sum_fx <0:
        return 0
    else:
        return sum_fx
  