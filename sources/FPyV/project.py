#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#######################################
####      FPyV -- PROJECT MODULE
#######################################
Python LSPIV tools for Fudaa-LSPIV

Defines the lspivProject class. Contains all the functions used
to extract and diplay datas from an existing Fudaa-LSPIV project. 

G.Bodart - 2024
"""

import numpy as np
import os 
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from zipfile import ZipFile
from FPyV import velocities,transform
from tqdm import tqdm 
from tkinter.filedialog import askdirectory
import glob
from PIL import Image
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
import scipy.stats
import io
import fnmatch

class lspivProject():
     """
     Fudaa-LSPIV project class:
     
    Contains 
    ---------------------
        - grid [npArrayInt]
            Grid points indexes -> stored [[index],[i,j]] 
        
        - h [float]
            Water level [m]
        
        - list_avg [npArrayStr]
            List of velocity fields used to compute mean velocity field
            
        - stab_param [dict]
            Dictionary containing information about stabilisation module
            
            - 'enable' [bool]
            
            - 'keypointDens' [str]
                Density of keypoints used (similar to "quality")
            
            - 'transfModel' [str]
                Transformation model used 
            
        - GRP [grp class]
            Class containing information about Ground Reference Points
            
            - X [float]
                X position in world [m]
            - Y [float]
                Y position in world [m]
            - Z [float]
                Z position in world [m]
            - i [int]
                i index on image [pixel]
            - j [float]
                j index on image [pixel]
        
        - img_ref [imgRef class]
            Class containing image references:  
                
            - xyMin [tuple]
                Minimum values of X and Y used for ortho-rectification -> stored (X,Y)
            
            - xyMax [tuple]
                Maximum values of X and Y used for ortho-rectification -> stored (X,Y)
            
            - res [float]
                Ortho-rectification resolution [m/pix]  
            
            - ninj [tuple]
                Size of the ortho-rectified image [pix] -> stored (ni,nj)
                
        - PIV_param [pivParam class]
            Class containing PIV parameters:
                
            - SA [dict]
                Dictionary containing Searching Area informations 
                ( S + i,j (in image order) + m,p ("minus", "plus") )
                
                ------'Sim'------
                \       \        \
               'Sjm'    xx      'Sjp'
                 \       \        \
                  ------'Sip'------   
                                    
            - IA [int]
                 Size of Interrogation Area (squarred area of side IA) 
                    
            - dt [float]
                Time interval [sec]
                
            - rMaxMin [tupleFloat]
                Filters on correlation -> stored (rMin,rMax)
                
            - nMaxMin [tupleFloat]
                Filters on norm of velocity [m/s] -> stored (nMin,nMax)
                    
            - vxMaxMin [tupleFloat]
                Filters on X velocity [m/s] -> stored (vxMin,vxMax)
                    
            - vyMaxMin [tuple]
                Filters on Y velocity [m/s] -> stored (vyMin,vyMax)
                    
            - alphaCoeff [float]
                Surface coefficient used to determine height average velocity  
                    
            - rxyMax [tupleFloat]
                Averaging radius around bathy point
                    
            - Dxp [float]
                Interpolation step for bathy points 
    ---------------------------                      
    
    """
    
     def __init__(self): 
        self.dir        = ""  
        self.grid       = np.empty(1, dtype = int)
        self.h          = -99.99
        self.list_avg   = np.empty(1, dtype = str)
        self.stab_param = {
                          'enable'      :False,
                          'keypointDens':"average",
                          'tranfModel'  :"Projective"
                            }
        self.GRP        = []                    # list of class GRP()
        self.img_ref    = transform.imgRef()    # Image parameters 
        self.PIV_param  = velocities.pivParam() # PIV parameters
        self.vel_raw    = velocities.vel()      # list of class vel()
        self.vel_filtre = velocities.vel()      # list of class vel()
        self.vel_moy    = velocities.vel()      # list of average velocities 
        self.contains   = {}                    # Dictionnary resuming files in the project 
        self.img_src    = []                    # List of source images 
        self.img_stab   = []
        self.img_transf = []                    # List of transformed images 
        self.mask       = []                    # Mask of river. Needed for several routines
        self.transect   = []                    # List of bathy() 
        self.Q          = {}
        self.rGrid      = []                    # correlation field (ensemble)
     
 
     def readfile(self,filename = ""):
        """ Read data from a Fudaa-LSPIV project (.lspiv.zip) 
        
        Parameters
        ----------------------
         
            filename [str] 
                - Path to the Fudaa-LSPIV project. 
                  If filename = "" then a GUI will be open to find the file  
                  
        Returns
        ----------------------
        
            errno [int]
                - 0 - No errors
                - 1 - Format isn't ".lspiv.zip" 
                - 2 - File "img_ref.dat" is missing
                - 3 - File "PIV_param.dat is missing
                
        Notes
        -----------------------
        Files could be missing such as grid.dat, h.dat, average_vel.out... 
        Still extraction will be performed and a warning message will be raised.
        To check the files that the lspivProject contains look at the .contains dictionnary
        
        """
        # ---- Initialisation 
        isExist = os.path.exists(filename) # Check if file exists 
        
        if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            root = Tk()
            root.withdraw()
            root.wm_attributes('-topmost', 1)     
            filename = askopenfilename(title = "Open Fudaa-LSPIV Project", filetypes = (("lspiv project","*.lspiv.zip"),("all files","*.*"))) # Get file path
        
        if (filename[-10:] != ".lspiv.zip" ):
            print("ERROR : file format doesn't match\n")
            return 1
                
        # ---- Read ZIP
        # Open project using ZipFile 
        with ZipFile(filename,'r') as foldzip:
            self.dir = filename
            # ---- -- Checkings 
            dictFiles = { 
                    'img_src'       :foldzip.namelist().count("img_pgm/image0002.png"),
                    'img_stab'      :foldzip.namelist().count("img_stab/cache/__image0001_stab.gif"),
                    'img_transf'    :foldzip.namelist().count("img_transf/cache/__image0001_transf.gif"),
                    'img_ref'       :foldzip.namelist().count("outputs.dir/img_ref.dat"),
                    'PIV_param'     :foldzip.namelist().count("outputs.dir/PIV_param.dat"),
                    'h'             :foldzip.namelist().count("outputs.dir/h.dat"),
                    'list_avg'      :foldzip.namelist().count("outputs.dir/list_avg.dat"),
                    'grid'          :foldzip.namelist().count("outputs.dir/grid.dat"),
                    'average_vel'   :foldzip.namelist().count("outputs.dir/average_vel.out"),
                    'average_scal'  :foldzip.namelist().count("outputs.dir/average_scal.out"),
                    'GRP'           :foldzip.namelist().count("outputs.dir/GRP.dat"),
                    'bathy_p'       :foldzip.namelist().count("outputs.dir/bathy_p.dat")
                    }
            
            # Read one image of each kind (src/stab/transf) 
            # --> To load all the images use the .loadImages function 
            if dictFiles['img_src']:
                data   = io.BytesIO(foldzip.read("img_pgm/image0002.png"))
                img    = Image.open(data)
                img    = np.array(img)
                self.img_src.append(img)      
                
            if dictFiles['img_stab']:
                data   = io.BytesIO(foldzip.read("img_stab/cache/__image0001_stab.gif"))
                img    = Image.open(data)
                img    = np.array(img)
                self.img_stab.append(img) 
            
            if dictFiles['img_transf']:
                data   = io.BytesIO(foldzip.read("img_transf/cache/__image0001_transf.gif"))
                img    = Image.open(data)
                img    = np.flipud(np.array(img))
                self.img_transf.append(img) 
   
                    
            # Count the number of files trans****.bth_dat in the archive
            nTrsct = [s.find('bth_dat')!=-1 for s in foldzip.namelist()].count(True)
            
            # ---- -- transect 
            for k in range(nTrsct):
                bthTmp = bathy()
                # Read bathy
                buff   = foldzip.read("transects/trans%04d.bth_dat"%(k+1)).decode("utf-8") 
                data   = np.array(buff.split(),float).reshape(-1,4)
                # Load info in object
                bthTmp.X        = data[:,0]
                bthTmp.Y        = data[:,1]
                bthTmp.Z        = data[:,2]
                bthTmp.alpha    = data[:,3]
                
                # Read parameters
                buff   = foldzip.read("transects/trans%04d.par_dat"%(k+1)).decode("utf-8")
                # Check file version 
                if buff.find("FileVersion") != -1:
                    fileVers = float(buff[14:17])
                if fileVers >= 4 :
                    buff = buff.split("\n")
                    # rxymax
                    
                    tmp         = buff[15]
                    tmp         = tmp.split()
                    bthTmp.rx   = float(tmp[0])
                    bthTmp.ry   = float(tmp[1])
                    # Dxp; npt Max
                    tmp         = buff[17]
                    tmp         = tmp.split()
                    bthTmp.dxp    = float(tmp[0])
                    bthTmp.nptMax = float(tmp[1])
                
                # Read results 
                buff         = foldzip.read("transects/trans%04d.res_dat"%(k+1)).decode("utf-8")
                data         = np.array(buff.split("\n")[0].split(),float)
                bthTmp.Q     = data[1]
                bthTmp.A_wet = data[2]
                bthTmp.vmean = data[3]

                self.transect.append(bthTmp)
                
            # ---- -- bathy_p 
            if dictFiles['bathy_p']:
                buff    = foldzip.read("outputs.dir/bathy_p.dat").decode("utf-8").split("\n")
                if len(self.transect) > 0:
                    tmp = buff[4].split()
                    self.transect[0].a = float(tmp[0])
                    self.transect[0].b = float(tmp[1])
                
            # ---- -- img ref
            if dictFiles['img_ref']:
                data    = foldzip.read("outputs.dir/img_ref.dat").decode("utf-8") 
                data    = data.split("\n")
                # xy min
                tmp     = data[1]
                tmp     = tmp.split()   
                self.img_ref.xyMin  = (float(tmp[0]),float(tmp[1])) 
                    # xy max    
                tmp     = data[3]
                tmp     = tmp.split()
                self.img_ref.xyMax  = (float(tmp[0]),float(tmp[1])) 
                    # res
                tmp     = data[5]
                self.img_ref.res    = float(tmp)
                    # ninj
                tmp     = data[7]
                tmp     = tmp.split()
                self.img_ref.ninj   = (int(tmp[0]),int(tmp[1]))   
            else:
                print("WARNING : file img_ref.dat missing\n")
 
            
             # ---- -- PIV param 
            if dictFiles['PIV_param']:
                data = foldzip.read("outputs.dir/PIV_param.dat").decode("utf-8") 
                data = data.split("\n")
                
                tmp = data[0]
                # Check file version 
                if tmp.find("FileVersion") != -1:
                    fileVers = float(tmp[14:17])
                # Read IA
                self.PIV_param.IA           = int(data[1]) 
                    # SA
                self.PIV_param.SA['Sim']    = int(data[3])   
                self.PIV_param.SA['Sip']    = int(data[4])
                self.PIV_param.SA['Sjm']    = int(data[5])
                self.PIV_param.SA['Sjp']    = int(data[6])
                if fileVers < 3 :
                        # dt
                    self.PIV_param.dt           = float(data[8])
                        # r
                    self.PIV_param.rMaxMin      = (float(data[10]),float(data[12]))
                        # image size     
                    self.PIV_param.ninj         = (int(data[15]),int(data[14]))
                        # s
                    self.PIV_param.nMaxMin      = (float(data[17]),float(data[18]))
                        # Vx
                    self.PIV_param.vxMaxMin     = (float(data[20]),float(data[21]))
                        # Vy 
                    self.PIV_param.vyMaxMin     = (float(data[23]),float(data[24]))
                        # Coeff Surf
                    self.PIV_param.alphaCoeff   = float(data[26])
                        # rxymax
                    tmp = data[28]
                    tmp = tmp.split()
                    self.PIV_param.rxyMax       = (float(tmp[0]),float(tmp[1]))
                        # Dxp
                    self.PIV_param.dxp          = float(data[30])
                elif fileVers == 3 :
                    tmp = data[8].split()
                    self.PIV_param.dt           = float(tmp[0])
                    self.img_ref.fstp           = int(tmp[1])
                    # r
                    self.PIV_param.rMaxMin      = (float(data[10]),float(data[12]))
                    # image size     
                    self.PIV_param.ninj         = (int(data[15]),int(data[14]))
                    # s
                    self.PIV_param.nMaxMin      = (float(data[17]),float(data[18]))
                    # Vx
                    self.PIV_param.vxMaxMin     = (float(data[20]),float(data[21]))
                    # Vy 
                    self.PIV_param.vyMaxMin     = (float(data[23]),float(data[24]))
                    # Coeff Surf
                    self.PIV_param.alphaCoeff   = float(data[26])
                    # rxymax
                    tmp = data[28]
                    tmp = tmp.split()
                    self.PIV_param.rxyMax       = (float(tmp[0]),float(tmp[1]))
                    tmp = data[30]
                    tmp = tmp.split()
                    # Dxp
                    self.PIV_param.dxp          = float(tmp[0])
                    self.PIV_param.mxPts        = float(tmp[1])
                else:
                    tmp = data[8].split()
                    self.PIV_param.dt           = float(tmp[0])
                    self.img_ref.fstp           = int(tmp[1])
                    # image size     
                    self.PIV_param.ninj         = (int(data[11]),int(data[10]))
                    # Coeff Surf
                    self.PIV_param.alphaCoeff   = float(data[13])
                    # rxymax
                    tmp = data[15]
                    tmp = tmp.split()
                    self.PIV_param.rxyMax       = (float(tmp[0]),float(tmp[1]))
                    tmp = data[17]
                    tmp = tmp.split()
                    # Dxp
                    self.PIV_param.dxp          = float(tmp[0])
                    self.PIV_param.mxPts        = float(tmp[1])
            else:
                print("WARNING : file PIV_param.dat missing, impossible to go further.\n")
                
                
            # ---- -- GRP.dat 
            if dictFiles['GRP']:
                buff = foldzip.read("outputs.dir/GRP.dat").decode("utf-8")
                i0 = buff.find("\n")
                i1 = i0 + buff[i0+1:].find("\n")
                nbGRP = int(buff[i0+1:i1+1])
                # Read file 
                if0 = buff.find("j")
                tmp     = np.array(buff[if0+2:].split(),float)
                # Re-arrange data
                lstGRP  = tmp.reshape((nbGRP,5))
                self.GRP = lstGRP
                
            # ---- -- h.dat 
            if dictFiles['h']:
                data = foldzip.read("outputs.dir/h.dat").decode("utf-8") 
                self.h = float(data)
            else:
                print("WARNING : file h.dat is missing.\n")
            
            # ---- -- list avg
            if dictFiles['list_avg']:
                buff = foldzip.read("outputs.dir/list_avg.dat").decode("utf-8") 
                data = buff.split()
                self.list_avg = np.asarray(data,dtype = str)
            else:
                print("WARNING : file list_avg.dat is missing.\n")
            
            # ---- -- grid.dat 
            if dictFiles['grid']:
                    # Data extraction 
                buff = foldzip.read("outputs.dir/grid.dat").decode("utf-8")    
                data = buff.split()
                   # Create an array with datas
                data = np.asarray(data,dtype = int)
                    # Reshape array to get 2D array
                data = np.reshape(data,(-1,2))
                    # Reshape data 
                grid = np.array([data[:,1],data[:,0]]).T # Swap columns to get (i,j)
                grid[:,0] = self.PIV_param.ninj[0] - grid[:,0] # Inverse i axes to get proper image coordinate
                self.grid = grid 
            else:
                print("WARNING : file grid.dat is missing.\n")
    
            
            # ---- -- vel_moy 
            if dictFiles['average_vel']:
                    # average_vel.out
                buff = foldzip.read("outputs.dir/average_vel.out") 
                tmp = buff.split()
                data = np.reshape(np.asarray(tmp,dtype = float),(-1,4))
                self.vel_moy.x  = data[:,0].copy()
                self.vel_moy.y  = data[:,1].copy()
                self.vel_moy.vx = data[:,2].copy()
                self.vel_moy.vy = data[:,3].copy()
            else:
                print("WARNING : file average_vel.out is missing.\n")
            if dictFiles['average_scal']:
                    # average_scal.out
                buff = foldzip.read("outputs.dir/average_scal.out") 
                tmp = buff.split()
                data = np.reshape(np.asarray(tmp,dtype = float),(-1,6))
                self.vel_moy.n      = data[:,2].copy()
                self.vel_moy.r      = data[:,3].copy()
                    # Compute other quantities 
                self.vel_moy.theta  = np.arctan2(self.vel_moy.vy,self.vel_moy.vx)
                self.vel_moy.dx     = self.vel_moy.vx * self.PIV_param.dt
                self.vel_moy.dy     = self.vel_moy.vy * self.PIV_param.dt
                self.vel_moy.di     = -self.vel_moy.dy / self.img_ref.res
                self.vel_moy.dj     = self.vel_moy.dx / self.img_ref.res 
                self.vel_moy.vi    = self.vel_moy.di / self.PIV_param.dt 
                self.vel_moy.vj    = self.vel_moy.dj / self.PIV_param.dt
                self.vel_moy.i      = self.PIV_param.ninj[0] - ((self.vel_moy.y - self.img_ref.xyMin[1]) / self.img_ref.res)
                self.vel_moy.j      = (self.vel_moy.x - self.img_ref.xyMin[0]) / self.img_ref.res
                self.vel_moy.nij    = np.sqrt(self.vel_moy.di**2 + self.vel_moy.dj**2)
                self.vel_moy.theij  = np.arctan2(self.vel_moy.di, self.vel_moy.dj)
            else:
                print("WARNING : file average_scal.out is missing.\n")
            
            print("\nExtraction of parameters -> OK")
            # ---- -- vel_raw 
                # Get "vel_raw" files 
            lstzip = foldzip.namelist()
            lstraw = [s for s in lstzip if "vel_raw" in s]
            lstraw = np.sort(lstraw[1:]) # Sort names 
            dictFiles["vel_raw"] = len(lstraw) # Add entry to follow-up checking dictionnary
           
            if dictFiles["vel_raw"]>0:
                    # Adapt size of vel raw arrays
                buff    = foldzip.read(lstraw[0]).decode()
                nPt     = len(buff.split("\n"))-1
                self.vel_raw = velocities.vel(nPt,len(lstraw))
                
                     # Extract data, field by field
                m = 0
                for fich in tqdm(lstraw,desc="Raw Velocities"):
                    buff    = foldzip.read(fich).decode()
                    sLi     = len((buff.split("\n")[0].split()))
                    tmp     = buff.split()
                    data    = np.reshape(np.asarray(tmp,dtype = float),(-1,sLi)) # Shaping data
                        # Create the vel() element containing the datas 
                    #self.vel_raw[]
                        # Get data from extraction 
                    self.vel_raw.i[:,m]     = data[:,0].copy()# copy the values -> avoid references troubles 
                    self.vel_raw.j[:,m]     = data[:,1].copy()
                    self.vel_raw.di[:,m]    = -1*data[:,2].copy()
                    self.vel_raw.dj[:,m]    = data[:,3].copy()
                    self.vel_raw.r[:,m]     = data[:,4].copy()
                    #elVelRaw.Ar    = data[:,7].copy() 
                    self.vel_raw.vi[:,m]    = self.vel_raw.di[:,m] / self.PIV_param.dt 
                    self.vel_raw.vj[:,m]    = self.vel_raw.dj[:,m] / self.PIV_param.dt
                        # Compute other quantities 
                    self.vel_raw.nij[:,m]   = np.sqrt(self.vel_raw.di[:,m]**2 + self.vel_raw.dj[:,m]**2)
                    self.vel_raw.x[:,m]     = (self.vel_raw.j[:,m] * self.img_ref.res) + self.img_ref.xyMin[0] 
                    self.vel_raw.y[:,m]     = (self.vel_raw.i[:,m] * self.img_ref.res) + self.img_ref.xyMin[1] 
                    self.vel_raw.dx[:,m]    = self.vel_raw.dj[:,m] * self.img_ref.res
                    self.vel_raw.dy[:,m]    = -self.vel_raw.di[:,m] * self.img_ref.res
                    self.vel_raw.vx[:,m]    = self.vel_raw.dx[:,m] / self.PIV_param.dt
                    self.vel_raw.vy[:,m]    = self.vel_raw.dy[:,m] / self.PIV_param.dt
                    self.vel_raw.n[:,m]     = np.sqrt(self.vel_raw.vx[:,m]**2 + self.vel_raw.vy[:,m]**2)
                    self.vel_raw.theta[:,m] = np.arctan2(self.vel_raw.vy[:,m],self.vel_raw.vx[:,m])
                    m = m+1
            
            # ---- -- vel_filter 
                # Get "vel_filter" files 
            lstfilt = [s for s in lstzip if "vel_filter" in s]
            lstfilt = np.sort(lstfilt[1:]) # Sort
            dictFiles["vel_filter"] = len(lstfilt) # Add entry to follow-up checking dictionnary
           
            if dictFiles["vel_filter"]>0:
                # Adapt size of vel raw arrays
                buff    = foldzip.read(lstfilt[0]).decode()
                nPt     = len(buff.split("\n"))-1
                self.vel_filtre = velocities.vel(nPt,len(lstraw)) 
                
                # Extract data, field by field
                m = 0
                for fich in tqdm (lstfilt,desc="Filtered Velocities"):
                    buff    = foldzip.read(fich).decode()
                    sLi     = len((buff.split("\n")[0].split()))
                    tmp     = buff.split()
                    data    = np.reshape(np.asarray(tmp,dtype = float),(-1,sLi)) # Shaping data
                        # Create the vel() element containing the datas 
                    #self.vel_raw[]
                        # Get data from extraction 
                    self.vel_filtre.x[:,m]     = data[:,0].copy()# copy the values -> avoid references troubles 
                    self.vel_filtre.y[:,m]     = data[:,1].copy()
                    self.vel_filtre.vx[:,m]    = data[:,2].copy()
                    self.vel_filtre.vy[:,m]    = data[:,3].copy()
                    self.vel_filtre.r[:,m]     = data[:,4].copy()
                    #elVelRaw.Ar    = data[:,7].copy() 
                    self.vel_filtre.vi[:,m]    = self.vel_filtre.di[:,m] / self.PIV_param.dt 
                    self.vel_filtre.vj[:,m]    = self.vel_filtre.dj[:,m] / self.PIV_param.dt
                        # Compute other quantities 
                    self.vel_filtre.nij[:,m]   = np.sqrt(self.vel_filtre.di[:,m]**2 + self.vel_filtre.dj[:,m]**2)
                    self.vel_filtre.j[:,m]     = (self.vel_filtre.x[:,m] - self.img_ref.xyMin[0]) / self.img_ref.res
                    self.vel_filtre.i[:,m]     = (self.vel_filtre.y[:,m] -self.img_ref.xyMin[1] ) / self.img_ref.res
                    self.vel_filtre.dj[:,m]    = self.vel_filtre.dx[:,m] / self.img_ref.res
                    self.vel_filtre.di[:,m]    = -self.vel_filtre.dy[:,m] / self.img_ref.res
                    self.vel_filtre.dx[:,m]    = self.vel_filtre.vx[:,m] * self.PIV_param.dt
                    self.vel_filtre.dy[:,m]    = self.vel_filtre.vy[:,m] * self.PIV_param.dt
                    self.vel_filtre.n[:,m]     = np.sqrt(self.vel_filtre.vx[:,m]**2 + self.vel_filtre.vy[:,m]**2)
                    self.vel_filtre.theta[:,m] = np.arctan2(self.vel_filtre.vy[:,m],self.vel_filtre.vx[:,m])
                   
                    m = m+1
            # ---- Verification
            self.contains = dictFiles
        
        return 0
    
     def readfileStrmlt(self,filename):
       """ Read data from a Fudaa-LSPIV project (.lspiv.zip) 
       for streamlit app
         
       Returns
       ----------------------
       
           errno [int]
               - 0 - No errors
               - 1 - Format isn't ".lspiv.zip" 
               - 2 - File "img_ref.dat" is missing
               - 3 - File "PIV_param.dat is missing
               
       Notes
       -----------------------
       Files could be missing such as grid.dat, h.dat, average_vel.out... 
       Still extraction will be performed and a warning message will be raised.
       To check the files that the lspivProject contains look at the .contains dictionnary
       
       """
              
       # ---- Read ZIP
       # Open project using ZipFile 
       with ZipFile(filename,'r') as foldzip:
           self.dir = filename
           # ---- -- Checkings 
           dictFiles = { 
                   'img_src'       :foldzip.namelist().count("img_pgm/image0002.png"),
                   'img_stab'      :foldzip.namelist().count("img_stab/cache/__image0001_stab.gif"),
                   'img_transf'    :foldzip.namelist().count("img_transf/cache/__image0001_transf.gif"),
                   'img_ref'       :foldzip.namelist().count("outputs.dir/img_ref.dat"),
                   'PIV_param'     :foldzip.namelist().count("outputs.dir/PIV_param.dat"),
                   'h'             :foldzip.namelist().count("outputs.dir/h.dat"),
                   'list_avg'      :foldzip.namelist().count("outputs.dir/list_avg.dat"),
                   'grid'          :foldzip.namelist().count("outputs.dir/grid.dat"),
                   'average_vel'   :foldzip.namelist().count("outputs.dir/average_vel.out"),
                   'average_scal'  :foldzip.namelist().count("outputs.dir/average_scal.out"),
                   'GRP'           :foldzip.namelist().count("outputs.dir/GRP.dat"),
                   'bathy_p'       :foldzip.namelist().count("outputs.dir/bathy_p.dat")
                   }
           
           # Read one image of each kind (src/stab/transf) 
           # --> To load all the images use the .loadImages function 
           if dictFiles['img_src']:
               data   = io.BytesIO(foldzip.read("img_pgm/image0002.png"))
               img    = Image.open(data)
               img    = np.array(img)
               self.img_src.append(img)      
               
           if dictFiles['img_stab']:
               data   = io.BytesIO(foldzip.read("img_stab/cache/__image0001_stab.gif"))
               img    = Image.open(data)
               img    = np.array(img)
               self.img_stab.append(img) 
           
           if dictFiles['img_transf']:
               data   = io.BytesIO(foldzip.read("img_transf/cache/__image0001_transf.gif"))
               img    = Image.open(data)
               img    = np.flipud(np.array(img))
               self.img_transf.append(img) 
  
                   
           # Count the number of files trans****.bth_dat in the archive
           nTrsct = [s.find('bth_dat')!=-1 for s in foldzip.namelist()].count(True)
           
           # ---- -- transect 
           for k in range(nTrsct):
               bthTmp = bathy()
               # Read bathy
               buff   = foldzip.read("transects/trans%04d.bth_dat"%(k+1)).decode("utf-8") 
               data   = np.array(buff.split(),float).reshape(-1,4)
               # Load info in object
               bthTmp.X        = data[:,0]
               bthTmp.Y        = data[:,1]
               bthTmp.Z        = data[:,2]
               bthTmp.alpha    = data[:,3]
               
               # Read parameters
               buff   = foldzip.read("transects/trans%04d.par_dat"%(k+1)).decode("utf-8")
               # Check file version 
               if buff.find("FileVersion") != -1:
                   fileVers = float(buff[14:17])
               if fileVers >= 4 :
                   buff = buff.split("\n")
                   # rxymax
                   
                   tmp         = buff[15]
                   tmp         = tmp.split()
                   bthTmp.rx   = float(tmp[0])
                   bthTmp.ry   = float(tmp[1])
                   # Dxp; npt Max
                   tmp         = buff[17]
                   tmp         = tmp.split()
                   bthTmp.dxp    = float(tmp[0])
                   bthTmp.nptMax = float(tmp[1])
               
               # Read results 
               buff         = foldzip.read("transects/trans%04d.res_dat"%(k+1)).decode("utf-8")
               data         = np.array(buff.split("\n")[0].split(),float)
               bthTmp.Q     = data[1]
               bthTmp.A_wet = data[2]
               bthTmp.vmean = data[3]

               self.transect.append(bthTmp)
               
           # ---- -- bathy_p 
           if dictFiles['bathy_p']:
               buff    = foldzip.read("outputs.dir/bathy_p.dat").decode("utf-8").split("\n")
               if len(self.transect) > 0:
                   tmp = buff[4].split()
                   self.transect[0].a = float(tmp[0])
                   self.transect[0].b = float(tmp[1])
               
           # ---- -- img ref
           if dictFiles['img_ref']:
               data    = foldzip.read("outputs.dir/img_ref.dat").decode("utf-8") 
               data    = data.split("\n")
               # xy min
               tmp     = data[1]
               tmp     = tmp.split()   
               self.img_ref.xyMin  = (float(tmp[0]),float(tmp[1])) 
                   # xy max    
               tmp     = data[3]
               tmp     = tmp.split()
               self.img_ref.xyMax  = (float(tmp[0]),float(tmp[1])) 
                   # res
               tmp     = data[5]
               self.img_ref.res    = float(tmp)
                   # ninj
               tmp     = data[7]
               tmp     = tmp.split()
               self.img_ref.ninj   = (int(tmp[0]),int(tmp[1]))   
           else:
               print("WARNING : file img_ref.dat missing\n")

           
            # ---- -- PIV param 
           if dictFiles['PIV_param']:
               data = foldzip.read("outputs.dir/PIV_param.dat").decode("utf-8") 
               data = data.split("\n")
               
               tmp = data[0]
               # Check file version 
               if tmp.find("FileVersion") != -1:
                   fileVers = float(tmp[14:17])
               # Read IA
               self.PIV_param.IA           = int(data[1]) 
                   # SA
               self.PIV_param.SA['Sim']    = int(data[3])   
               self.PIV_param.SA['Sip']    = int(data[4])
               self.PIV_param.SA['Sjm']    = int(data[5])
               self.PIV_param.SA['Sjp']    = int(data[6])
               if fileVers < 4 :
                       # dt
                   self.PIV_param.dt           = float(data[8])
                       # r
                   self.PIV_param.rMaxMin      = (float(data[10]),float(data[12]))
                       # image size     
                   self.PIV_param.ninj         = (int(data[15]),int(data[14]))
                       # s
                   self.PIV_param.nMaxMin      = (float(data[17]),float(data[18]))
                       # Vx
                   self.PIV_param.vxMaxMin     = (float(data[20]),float(data[21]))
                       # Vy 
                   self.PIV_param.vyMaxMin     = (float(data[23]),float(data[24]))
                       # Coeff Surf
                   self.PIV_param.alphaCoeff   = float(data[26])
                       # rxymax
                   tmp = data[28]
                   tmp = tmp.split()
                   self.PIV_param.rxyMax       = (float(tmp[0]),float(tmp[1]))
                       # Dxp
                   self.PIV_param.dxp          = float(data[30])
               else:
                   tmp = data[8].split()
                   self.PIV_param.dt           = float(tmp[0])
                   self.img_ref.fstp           = int(tmp[1])
                   # image size     
                   self.PIV_param.ninj         = (int(data[11]),int(data[10]))
                   # Coeff Surf
                   self.PIV_param.alphaCoeff   = float(data[13])
                   # rxymax
                   tmp = data[15]
                   tmp = tmp.split()
                   self.PIV_param.rxyMax       = (float(tmp[0]),float(tmp[1]))
                   tmp = data[17]
                   tmp = tmp.split()
                   # Dxp
                   self.PIV_param.dxp          = float(tmp[0])
                   self.PIV_param.mxPts        = float(tmp[1])
           else:
               print("WARNING : file PIV_param.dat missing, impossible to go further.\n")
               
               
           # ---- -- GRP.dat 
           if dictFiles['GRP']:
               buff = foldzip.read("outputs.dir/GRP.dat").decode("utf-8")
               i0 = buff.find("\n")
               i1 = i0 + buff[i0+1:].find("\n")
               nbGRP = int(buff[i0+1:i1+1])
               # Read file 
               if0 = buff.find("j")
               tmp     = np.array(buff[if0+2:].split(),float)
               # Re-arrange data
               lstGRP  = tmp.reshape((nbGRP,5))
               self.GRP = lstGRP
               
           # ---- -- h.dat 
           if dictFiles['h']:
               data = foldzip.read("outputs.dir/h.dat").decode("utf-8") 
               self.h = float(data)
           else:
               print("WARNING : file h.dat is missing.\n")
           
           # ---- -- list avg
           if dictFiles['list_avg']:
               buff = foldzip.read("outputs.dir/list_avg.dat").decode("utf-8") 
               data = buff.split()
               self.list_avg = np.asarray(data,dtype = str)
           else:
               print("WARNING : file list_avg.dat is missing.\n")
           
           # ---- -- grid.dat 
           if dictFiles['grid']:
                   # Data extraction 
               buff = foldzip.read("outputs.dir/grid.dat").decode("utf-8")    
               data = buff.split()
                  # Create an array with datas
               data = np.asarray(data,dtype = int)
                   # Reshape array to get 2D array
               data = np.reshape(data,(-1,2))
                   # Reshape data 
               grid = np.array([data[:,1],data[:,0]]).T # Swap columns to get (i,j)
               grid[:,0] = self.PIV_param.ninj[0] - grid[:,0] # Inverse i axes to get proper image coordinate
               self.grid = grid 
           else:
               print("WARNING : file grid.dat is missing.\n")
   
           
           # ---- -- vel_moy 
           if dictFiles['average_vel']:
                   # average_vel.out
               buff = foldzip.read("outputs.dir/average_vel.out") 
               tmp = buff.split()
               data = np.reshape(np.asarray(tmp,dtype = float),(-1,4))
               self.vel_moy.x  = data[:,0].copy()
               self.vel_moy.y  = data[:,1].copy()
               self.vel_moy.vx = data[:,2].copy()
               self.vel_moy.vy = data[:,3].copy()
           else:
               print("WARNING : file average_vel.out is missing.\n")
           if dictFiles['average_scal']:
                   # average_scal.out
               buff = foldzip.read("outputs.dir/average_scal.out") 
               tmp = buff.split()
               data = np.reshape(np.asarray(tmp,dtype = float),(-1,6))
               self.vel_moy.n      = data[:,2].copy()
               self.vel_moy.r      = data[:,3].copy()
                   # Compute other quantities 
               self.vel_moy.theta  = np.arctan2(self.vel_moy.vy,self.vel_moy.vx)
               self.vel_moy.dx     = self.vel_moy.vx * self.PIV_param.dt
               self.vel_moy.dy     = self.vel_moy.vy * self.PIV_param.dt
               self.vel_moy.di     = -self.vel_moy.dy / self.img_ref.res
               self.vel_moy.dj     = self.vel_moy.dx / self.img_ref.res 
               self.vel_moy.vi    = self.vel_moy.di / self.PIV_param.dt 
               self.vel_moy.vj    = self.vel_moy.dj / self.PIV_param.dt
               self.vel_moy.i      = self.PIV_param.ninj[0] - ((self.vel_moy.y - self.img_ref.xyMin[1]) / self.img_ref.res)
               self.vel_moy.j      = (self.vel_moy.x - self.img_ref.xyMin[0]) / self.img_ref.res
               self.vel_moy.nij    = np.sqrt(self.vel_moy.di**2 + self.vel_moy.dj**2)
               self.vel_moy.theij  = np.arctan2(self.vel_moy.di, self.vel_moy.dj)
           else:
               print("WARNING : file average_scal.out is missing.\n")
           
           print("\nExtraction of parameters -> OK")
           # ---- -- vel_raw 
               # Get "vel_raw" files 
           lstzip = foldzip.namelist()
           lstraw = [s for s in lstzip if "vel_raw" in s]
           lstraw = np.sort(lstraw[1:]) # Sort names 
           dictFiles["vel_raw"] = len(lstraw) # Add entry to follow-up checking dictionnary
          
           if dictFiles["vel_raw"]>0:
                   # Adapt size of vel raw arrays
               buff    = foldzip.read(lstraw[0]).decode()
               nPt     = len(buff.split("\n"))-1
               self.vel_raw = velocities.vel(nPt,len(lstraw))
               
                    # Extract data, field by field
               m = 0
               for fich in tqdm(lstraw,desc="Raw Velocities"):
                   buff    = foldzip.read(fich).decode()
                   sLi     = len((buff.split("\n")[0].split()))
                   tmp     = buff.split()
                   data    = np.reshape(np.asarray(tmp,dtype = float),(-1,sLi)) # Shaping data
                       # Create the vel() element containing the datas 
                   #self.vel_raw[]
                       # Get data from extraction 
                   self.vel_raw.i[:,m]     = data[:,0].copy()# copy the values -> avoid references troubles 
                   self.vel_raw.j[:,m]     = data[:,1].copy()
                   self.vel_raw.di[:,m]    = -1*data[:,2].copy()
                   self.vel_raw.dj[:,m]    = data[:,3].copy()
                   self.vel_raw.r[:,m]     = data[:,4].copy()
                   #elVelRaw.Ar    = data[:,7].copy() 
                   self.vel_raw.vi[:,m]    = self.vel_raw.di[:,m] / self.PIV_param.dt 
                   self.vel_raw.vj[:,m]    = self.vel_raw.dj[:,m] / self.PIV_param.dt
                       # Compute other quantities 
                   self.vel_raw.nij[:,m]   = np.sqrt(self.vel_raw.di[:,m]**2 + self.vel_raw.dj[:,m]**2)
                   self.vel_raw.x[:,m]     = (self.vel_raw.j[:,m] * self.img_ref.res) + self.img_ref.xyMin[0] 
                   self.vel_raw.y[:,m]     = (self.vel_raw.i[:,m] * self.img_ref.res) + self.img_ref.xyMin[1] 
                   self.vel_raw.dx[:,m]    = self.vel_raw.dj[:,m] * self.img_ref.res
                   self.vel_raw.dy[:,m]    = -self.vel_raw.di[:,m] * self.img_ref.res
                   self.vel_raw.vx[:,m]    = self.vel_raw.dx[:,m] / self.PIV_param.dt
                   self.vel_raw.vy[:,m]    = self.vel_raw.dy[:,m] / self.PIV_param.dt
                   self.vel_raw.n[:,m]     = np.sqrt(self.vel_raw.vx[:,m]**2 + self.vel_raw.vy[:,m]**2)
                   self.vel_raw.theta[:,m] = np.arctan2(self.vel_raw.vy[:,m],self.vel_raw.vx[:,m])
                   m = m+1
           
           # ---- -- vel_filter 
               # Get "vel_filter" files 
           lstfilt = [s for s in lstzip if "vel_filter" in s]
           lstfilt = np.sort(lstfilt[1:]) # Sort
           dictFiles["vel_filter"] = len(lstfilt) # Add entry to follow-up checking dictionnary
          
           if dictFiles["vel_filter"]>0:
               # Adapt size of vel raw arrays
               buff    = foldzip.read(lstfilt[0]).decode()
               nPt     = len(buff.split("\n"))-1
               self.vel_filtre = velocities.vel(nPt,len(lstraw)) 
               
               # Extract data, field by field
               m = 0
               for fich in tqdm (lstfilt,desc="Filtered Velocities"):
                   buff    = foldzip.read(fich).decode()
                   sLi     = len((buff.split("\n")[0].split()))
                   tmp     = buff.split()
                   data    = np.reshape(np.asarray(tmp,dtype = float),(-1,sLi)) # Shaping data
                       # Create the vel() element containing the datas 
                   #self.vel_raw[]
                       # Get data from extraction 
                   self.vel_filtre.x[:,m]     = data[:,0].copy()# copy the values -> avoid references troubles 
                   self.vel_filtre.y[:,m]     = data[:,1].copy()
                   self.vel_filtre.vx[:,m]    = data[:,2].copy()
                   self.vel_filtre.vy[:,m]    = data[:,3].copy()
                   self.vel_filtre.r[:,m]     = data[:,4].copy()
                   #elVelRaw.Ar    = data[:,7].copy() 
                   self.vel_filtre.vi[:,m]    = self.vel_filtre.di[:,m] / self.PIV_param.dt 
                   self.vel_filtre.vj[:,m]    = self.vel_filtre.dj[:,m] / self.PIV_param.dt
                       # Compute other quantities 
                   self.vel_filtre.nij[:,m]   = np.sqrt(self.vel_filtre.di[:,m]**2 + self.vel_filtre.dj[:,m]**2)
                   self.vel_filtre.j[:,m]     = (self.vel_filtre.x[:,m] - self.img_ref.xyMin[0]) / self.img_ref.res
                   self.vel_filtre.i[:,m]     = (self.vel_filtre.y[:,m] -self.img_ref.xyMin[1] ) / self.img_ref.res
                   self.vel_filtre.dj[:,m]    = self.vel_filtre.dx[:,m] / self.img_ref.res
                   self.vel_filtre.di[:,m]    = -self.vel_filtre.dy[:,m] / self.img_ref.res
                   self.vel_filtre.dx[:,m]    = self.vel_filtre.vx[:,m] * self.PIV_param.dt
                   self.vel_filtre.dy[:,m]    = self.vel_filtre.vy[:,m] * self.PIV_param.dt
                   self.vel_filtre.n[:,m]     = np.sqrt(self.vel_filtre.vx[:,m]**2 + self.vel_filtre.vy[:,m]**2)
                   self.vel_filtre.theta[:,m] = np.arctan2(self.vel_filtre.vy[:,m],self.vel_filtre.vx[:,m])
                  
                   m = m+1
           # ---- Verification
           self.contains = dictFiles
       
       return 0
    
     def streamlines(self,  
                     data   = "moy",
                    ax      = None,
                    cmap    = "viridis"):
         
         if isinstance(ax,type(None)):
             fig,ax  = plt.subplots()
         
        # plot image
         if len(self.img_transf) > 0:
            
            ax.imshow(self.img_transf[0],extent = [self.img_ref.xyMin[0],self.img_ref.xyMax[0],
                                                   self.img_ref.xyMin[1],self.img_ref.xyMax[1]],
                      cmap = "gray")
            ax.set_xlabel("X [m]")
            ax.set_ylabel("Y [m]")
            ax.grid(False)
            
         if data =="moy":
            fig = self.vel_moy.streamlines(ax,cmap)
           
         return fig
            
        
                
     def arrow2D(self,
                 data        = "raw",
                 pos         = 0,
                 plot        = "vXY", 
                 title       = "", 
                 xlbl        = "", 
                 ylbl        = "", 
                 ax          = None, 
                 color       = "red",
                 cmapData    = "norm",
                 cmap        = "viridis", 
                 scale       = 55,
                 width       = 0.0045,
                 headwidth   = 4, 
                 lw          = 0.15,
                 ec="k",
                 **param_dict):
         
         # check axes  
         if ax == None: # Creates figure and ax
             fig = plt.figure()
             ax = fig.add_subplot(111)
             plt.axis("equal")
             ax.set_title(title)
             ax.set_xlabel(xlbl)
             ax.set_ylabel(ylbl)
             
         # plot image
         if len(self.img_transf) > 0:
             
             ax.imshow(self.img_transf[0],extent = [self.img_ref.xyMin[0],self.img_ref.xyMax[0],
                                                    self.img_ref.xyMin[1],self.img_ref.xyMax[1]],
                       cmap = "gray")
             ax.set_xlabel("X [m]")
             ax.set_ylabel("Y [m]")
             ax.grid(False)
             
         if data == "raw":
            qv = self.vel_raw.arrow2D(pos,plot,title,ax = ax,color = color,cmapData = cmapData,cmap = cmap,scale = scale,width = width,headwidth = headwidth,lw = lw)
         
         if data == "filtre":
            qv = self.vel_filtre.arrow2D(pos,plot,title,ax = ax,color = color,cmapData = cmapData,cmap = cmap,scale = scale,width = width,headwidth = headwidth,lw = lw)
       
         if data == "moy":
            qv = self.vel_moy.arrow2D(0,plot,title,ax = ax,color = color,cmapData = cmapData,cmap = cmap,scale = scale,width = width,headwidth = headwidth,lw = lw)
               
         return fig
 
     
     def aggreg(self, median = True):
         
        tmp = self.vel_filtre.copy() 
        
        self.vel_moy   = velocities.vel(np.shape(tmp.i)[0],1)
        
        self.vel_moy.i = tmp.i[:,0].reshape(-1,1)
        self.vel_moy.j = tmp.j[:,0].reshape(-1,1)
        self.vel_moy.x = tmp.x[:,0].reshape(-1,1)
        self.vel_moy.y = tmp.y[:,0].reshape(-1,1)
        
        # Compute median /mean
        if median:
            self.vel_moy.vx    = np.nanmedian(tmp.vx,axis = 1).reshape(-1,1)
            self.vel_moy.vy    = np.nanmedian(tmp.vy,axis = 1).reshape(-1,1)
            self.vel_moy.vi    = np.nanmedian(tmp.vi,axis = 1).reshape(-1,1)
            self.vel_moy.vj    = np.nanmedian(tmp.vj,axis = 1).reshape(-1,1)
            self.vel_moy.di    = np.nanmedian(tmp.di,axis = 1).reshape(-1,1)
            self.vel_moy.dj    = np.nanmedian(tmp.dj,axis = 1).reshape(-1,1)
            self.vel_moy.nij   = np.nanmedian(tmp.nij,axis = 1).reshape(-1,1)
            self.vel_moy.dx    = np.nanmedian(tmp.dx,axis = 1).reshape(-1,1)
            self.vel_moy.dy    = np.nanmedian(tmp.dy,axis = 1).reshape(-1,1)
            self.vel_moy.n     = np.nanmedian(tmp.n,axis = 1).reshape(-1,1)
            self.vel_moy.r     = np.nanmedian(tmp.r,axis = 1).reshape(-1,1)
            self.vel_moy.theta = np.nanmedian(tmp.theta,axis = 1).reshape(-1,1)
            self.vel_moy.Ar    = np.nanmedian(tmp.Ar,axis = 1).reshape(-1,1)
        else:
            self.vel_moy.vx    = np.nanmean(tmp.vx,axis = 1).reshape(-1,1)
            self.vel_moy.vy    = np.nanmean(tmp.vy,axis = 1).reshape(-1,1)
            self.vel_moy.vi    = np.nanmean(tmp.vi,axis = 1).reshape(-1,1)
            self.vel_moy.vj    = np.nanmean(tmp.vj,axis = 1).reshape(-1,1)
            self.vel_moy.di    = np.nanmean(tmp.di,axis = 1).reshape(-1,1)
            self.vel_moy.dj    = np.nanmean(tmp.dj,axis = 1).reshape(-1,1)
            self.vel_moy.nij   = np.nanmean(tmp.nij,axis = 1).reshape(-1,1)
            self.vel_moy.dx    = np.nanmean(tmp.dx,axis = 1).reshape(-1,1)
            self.vel_moy.dy    = np.nanmean(tmp.dy,axis = 1).reshape(-1,1)
            self.vel_moy.n     = np.nanmean(tmp.n,axis = 1).reshape(-1,1)
            self.vel_moy.r     = np.nanmean(tmp.r,axis = 1).reshape(-1,1)
            self.vel_moy.theta = np.nanmean(tmp.theta,axis = 1).reshape(-1,1)
            self.vel_moy.Ar    = np.nanmean(tmp.Ar,axis = 1).reshape(-1,1)
        
        # count amount of data per point : remove points with less than 5 vectors
        yo = np.logical_not(np.isnan(tmp.vx))
        return 1
        
     def writeAverageOut(self,wd):
         """
         Write file average_vel.out in specified folder (wd). 
         This function is mainly used to compute discharge out of F-LSPIV as average_vel.out is used to obtain surface velocities on the transect

         Parameters
         ----------
         wd : str
             Path to the folder were average_vel.out will be written.

         Returns
         -------
         int
             errno.

         """
         if wd[-1] == "/":
             wd = wd[:-1]
             
         if not isinstance(self.vel_moy,list):
             f      = open(wd + "/average_vel.out","w")
             for ind,el in enumerate(self.vel_moy.x):
                 x  = np.round(self.vel_moy.x[ind],4)
                 y  = np.round(self.vel_moy.y[ind],4)
                 vx = np.round(self.vel_moy.vx[ind],4)
                 vy = np.round(self.vel_moy.vy[ind],4)
                 li = "{} {} {} {}\n".format(x,y,vx,vy)
                 li.replace("nan","0.0")
                 f.write(li.replace("nan","0.0"))
             f.close()
            
         return 0
     
     def writeFilterPiv(self,wd):
         
         if wd[-1] == "/":
             wd = wd[:-1]
             
         if len(self.vel_filtre) > 0:
             for ind,el in enumerate(self.vel_filtre):
                 f      = open(wd + "/filter_piv%04d.dat" % (ind+1),"w")
                 for x,y,vx,vy,r in zip(el.x,el.y,el.vx,el.vy,el.r):
                     x  = np.round(x,4)
                     y  = np.round(y,4)
                     vx = np.round(vx,4)
                     vy = np.round(vy,4)
                     li = "{} {} {} {}\n".format(x,y,vx,vy)
                     li.replace("nan","0.0")
                     f.write(li.replace("nan","0.0"))
                 f.close()
         else: 
             print("ERROR: no filter velocities")
             return 1
         
         return 0
     
     def writeRawPiv(self,wd):
         
         if wd[-1] == "/":
             wd = wd[:-1]
             
         if len(self.vel_raw) > 0:
             for ind,el in enumerate(self.vel_raw):
                 f      = open(wd + "/piv%04d.dat" % (ind+1),"w")
                 for x,y,vx,vy,r in zip(el.x,el.y,el.vx,el.vy,el.r):
                     x  = np.round(x,4)
                     y  = np.round(y,4)
                     vx = np.round(vx,4)
                     vy = np.round(vy,4)
                     li = "{} {} {} {}\n".format(x,y,vx,vy)
                     li.replace("nan","0.0")
                     f.write(li.replace("nan","0.0"))
                 f.close()
         else: 
             print("ERROR: no filter velocities")
             return 1
         
         return 0
           
     def loadImg_src(self):
         """
         Load the source images from the .lspiv.zip archive. 

         Returns
         -------
         None.

         """
         # Get path to the fudaa archive 
         filename  = self.dir
         
         # ---- Initialisation 
         isExist = os.path.exists(filename) # Check if file exists 
         
         if (filename == "") or (isExist == False):
             # Open GUI to find file - tkinker lib is used 
             Tk().withdraw()     
             filename = askopenfilename(title = "Open Fudaa-LSPIV Project", filetypes = (("lspiv project","*.lspiv.zip"),("all files","*.*"))) # Get file path
         
         if (filename[-10:] != ".lspiv.zip" ):
             print("ERROR : file format doesn't match\n")
             return 1
                 
         # ---- Read ZIP
         # Open project using ZipFile 
         with ZipFile(filename,'r') as foldzip:
             # Get list of images in archive 
             lstImg = fnmatch.filter(foldzip.namelist(),"img_pgm/image*.png")
             # empty data src
             self.img_src = []  
             # Load all images founded
             for elname in tqdm(lstImg):
                 data   = io.BytesIO(foldzip.read(elname))
                 img    = Image.open(data)
                 img    = np.array(img)
                 self.img_src.append(img)      
                 
         return None
         
     def loadImg_stab(self):
         """
         Load the stabilized images from the .lspiv.zip archive. 

         Returns
         -------
         None.

         """
         # Get path to the fudaa archive 
         filename  = self.dir
         
         # ---- Initialisation 
         isExist = os.path.exists(filename) # Check if file exists 
         
         if (filename == "") or (isExist == False):
             # Open GUI to find file - tkinker lib is used 
             Tk().withdraw()     
             filename = askopenfilename(title = "Open Fudaa-LSPIV Project", filetypes = (("lspiv project","*.lspiv.zip"),("all files","*.*"))) # Get file path
         
         if (filename[-10:] != ".lspiv.zip" ):
             print("ERROR : file format doesn't match\n")
             return 1
                 
         # ---- Read ZIP
         # Open project using ZipFile 
         with ZipFile(filename,'r') as foldzip:
             # Get list of images in archive 
             lstImg = fnmatch.filter(foldzip.namelist(),"img_stab/cache/__image*_stab.gif")
             # empty data src
             self.img_stab = []  
             # Load all images founded
             for elname in tqdm(lstImg):
                   data   = io.BytesIO(foldzip.read(elname))
                   img    = Image.open(data)
                   img    = np.array(img)
                   self.img_stab.append(img)      
                 
         return None
     
     def loadImg_transf(self):
         """
         Load the orthorectified images from the .lspiv.zip archive. 

         Returns
         -------
         None.

         """
         # Get path to the fudaa archive 
         filename  = self.dir
         
         # ---- Initialisation 
         isExist = os.path.exists(filename) # Check if file exists 
         
         if (filename == "") or (isExist == False):
             # Open GUI to find file - tkinker lib is used 
             Tk().withdraw()     
             filename = askopenfilename(title = "Open Fudaa-LSPIV Project", filetypes = (("lspiv project","*.lspiv.zip"),("all files","*.*"))) # Get file path
         
         if (filename[-10:] != ".lspiv.zip" ):
             print("ERROR : file format doesn't match\n")
             return 1
                 
         # ---- Read ZIP
         # Open project using ZipFile 
         with ZipFile(filename,'r') as foldzip:
             # Get list of images in archive 
             lstImg = fnmatch.filter(foldzip.namelist(),"img_transf/cache/__image*_transf.gif")
             # empty data src
             self.img_transf = []  
             # Load all images founded
             for elname in tqdm(lstImg):
                   data   = io.BytesIO(foldzip.read(elname))
                   img    = Image.open(data)
                   img    = np.array(img)
                   self.img_transf.append(img)      
                 
         return None
     
     def import_img_src(self,path = ""):
        """
        Import source images to the lspiv project. path identifies the folder containing the images. 
        If no path is given then a GUI will be open to find it
    
        Parameters
        ----------
            - path : [str], optional
                path to the folder containing the source images. If no path is given then a GUI will be open 
                The default is "".
    
        Returns
        -------
            - errno [int]
                - Error number 
                    - 0 - No errors 
                    - 1 - Wrong format 

        """
        if len(self.img_src) != 0:
            print("\nWARNING: source images already exist. Overwrite ? (y|n)")
            rep = input().lower()
            if rep == "y":
                self.img_src = []
            elif rep == "n":
                print("\nAbort import")
                return 0
         # ---- Initialisation 
        isExist = os.path.exists(path) # Check if file exists 
        
        if (path == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            path = askdirectory(title='Select Image Folder') 
            
        # Checkings
        lstfiles = glob.glob(path + "/*.png")
        if(len(lstfiles)==0):
            print("\nERROR: No images located in the scpecified folder. Image format expected: image*.png !")
            return 1 
        
        # ---- Read images 
        lstfiles = np.sort(lstfiles) # sort names
        try:
            for name in lstfiles:
                im      = Image.open(name)
                imArray = np.asarray(im)
                self.img_src.append(imArray)
        except Exception as e:
            print("\nERROR: Problem while reading images\n Exception: " + str(e))
            return 1
            
        return 0
    
     def import_img_transf(self,path = ""):
        """
        Import transformed images to the lspiv project. path identifies the folder containing the images. 
        If no path is given then a GUI will be open to find it
    
        Parameters
        ----------
            - path : [str], optional
                path to the folder containing the source images. If no path is given then a GUI will be open 
                The default is "".
    
        Returns
        -------
            - errno [int]
                - Error number 
                    - 0 - No errors 
                    - 1 - Wrong format 

        """
        # Checkings
        if len(self.img_transf) != 0:
            print("\nWARNING: source images already exist. Overwrite ? (y|n)")
            rep = input().lower()
            if rep == "y":
                self.img_transf = []
            elif rep == "n":
                print("\nAbort import")
                return 0
        
         # ---- Initialisation 
        isExist = os.path.exists(path) # Check if file exists 
        
        if (path == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            path = askdirectory(title='Select Image Folder') 
            
        # Checkings
        lstfiles = glob.glob(path + "/image*.png")
        if(len(lstfiles)==0):
            print("\nERROR: No images located in the scpecified folder. Image format expected: image*.png !")
            return 1 
        
        # ---- Read images 
        lstfiles = np.sort(lstfiles) # sort names
        try:
            for name in lstfiles:
                im      = Image.open(name)
                imArray = np.asarray(im)
                self.img_transf.append(imArray)
        except Exception as e:
            print("\nERROR: Problem while reading images\n Exception: " + str(e))
            return 1
            
        return 0
     def import_mask(self,filename = ""):
        """
        Import a mask (binary image) of the river flow

        Parameters
        ----------
            filename : [str], optional
                Path to the file 'mask.png'. If "" then a GUI will be opened to select file
                The default is "".

        Returns
        -------
            - errno [int]
                Error number. Possible values : 
                    - 0 - no errors 
                    - 1 - wrong format contains file coeff.datfilename : TYPE, optional
        """
        # ---- Initialisation 
        isExist = os.path.exists(filename) # Check if file exists 
        
        if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            filename = askopenfilename(title = "Select mask.png", filetypes = (("mask","mask.png"),("all files","*.*"))) # Get file path
        
        # ---- Read 
        try: 
            self.mask = Image.open(filename)
        except Exception as e:
            print("\nERROR: Problem while reading images\n Exception: " + str(e))
            return 1 
    
            
     def temporalFiltering(self,dest,k=3,tCoV = 0.4, tR = 0.25,CoV = True,Ang = True, dev = True):
         """
         

         Parameters
         ----------
         dest : TYPE
             DESCRIPTION.
         k : TYPE, optional
             DESCRIPTION. The default is 3.
         tCoV : TYPE, optional
             DESCRIPTION. The default is 0.4.
         tR : TYPE, optional
             DESCRIPTION. The default is 0.25.
         CoV : TYPE, optional
             DESCRIPTION. The default is True.
         dev : TYPE, optional
             DESCRIPTION. The default is True.

         Returns
         -------
         int
             DESCRIPTION.

         """
         # Initialisation 
         fDev = None
         fAng = None
         fCoV = None 
         
         transect = False
         if len(self.transect) > 0:
             a,b = self.transect[0].a, self.transect[0].b
             # Ensure that a and b are not default values 
             if a!=0 and b!=0:
                 transect = True
                
             
         if dest == "raw":
             # Create filter velocities
             self.vel_filtre = self.vel_raw.copy()

         di = self.vel_filtre.di
         dj = self.vel_filtre.dj
         vx = self.vel_filtre.vx
         vy = self.vel_filtre.vy
        # Force non measured points (where di==0 and dj==0) to nan before mean computation
         cond   = np.logical_or(vx==0,vy ==0)
         vx[cond] = np.nan
         vy[cond] = np.nan
        
        # Set Fudaa-LSPIV filtered results to nan 
         cond   = np.logical_or(di==0,dj ==0)
         di[cond] = np.nan
         dj[cond] = np.nan
        # Count the amount of data per points
         nVal = np.count_nonzero(~np.isnan(di),axis = 1)
        
         if dev: 
          # ## FILTER 1 - DEVIATION FROM MEAN VELOCITY
          # ##################################
               # Evaluate mean and std at each point for di/dj 
               meanI  = np.nanmean(di,axis=1)
               stdI   = np.nanstd(di,axis=1)
               meanJ  = np.nanmean(dj,axis=1)
               stdJ   = np.nanstd(dj,axis=1)
               # Temporal filter (k*sigma tolerance)
               mnI,mxI= meanI-(k*stdI),meanI+(k*stdI)
               mnJ,mxJ= meanJ-(k*stdJ),meanJ+(k*stdJ)
               # Arrange data before filter 
               tmnI   = np.vstack((mnI,) * np.shape(di)[1]).T
               tmnJ   = np.vstack((mnJ,) * np.shape(di)[1]).T
               tmxI   = np.vstack((mxI,) * np.shape(di)[1]).T
               tmxJ   = np.vstack((mxJ,) * np.shape(di)[1]).T
               
               # Identify data to filter 
               fDev   = np.logical_or(np.logical_or(di>tmxI,di<tmnI),np.logical_or(dj>tmxJ,dj<tmnJ))
             
               # Apply filter 
               self.vel_filtre.di[fDev] = np.nan
               self.vel_filtre.dj[fDev] = np.nan
               self.vel_filtre.r[fDev]  = np.nan
               self.vel_filtre.vx[fDev] = np.nan
               self.vel_filtre.vy[fDev] = np.nan
               
         # ## FILTER 2 - COEFF. OF VARIATION
         # ##################################
         if transect:
             # Project velocity on vector normal to transect
             vp     = -b*vx+a*vy
             # Compute coefficient of variation on projected velocity 
             varVP  = np.nanstd(vp,axis = 1)/np.nanmean(vp,axis = 1)
             # Filter
             fCoV   = varVP > tCoV
             # Apply filter 
             if CoV:
                 self.vel_filtre.di[fCoV,:] = np.nan
                 self.vel_filtre.dj[fCoV,:] = np.nan
                 self.vel_filtre.r[fCoV,:]  = np.nan
                 self.vel_filtre.vx[fCoV,:] = np.nan
                 self.vel_filtre.vy[fCoV,:] = np.nan
         
         # ## FILTER 3 - ANGULAR VELOCITY
         # ##################################
         # Compute coefficient of variation on angle 
         aV     = np.arctan2(vy,vx)
         # Store in list to compute mean and std
         meanA  = scipy.stats.circmean(aV,high = np.pi,low=-np.pi,nan_policy="omit")
         R      = scipy.stats.circvar(aV,high = np.pi,low=-np.pi,nan_policy="omit",axis=1)
         # Filter 
         fAng   = R > tR
         # Apply 
         if Ang:
             self.vel_filtre.di[fAng,:] = np.nan
             self.vel_filtre.dj[fAng,:] = np.nan
             self.vel_filtre.r[fAng,:]  = np.nan
             self.vel_filtre.vx[fAng,:] = np.nan
             self.vel_filtre.vy[fAng,:] = np.nan
                   
         # dict with filter for output 
         out = {"fDev":fDev,
                "fCoV":fCoV,
                "fAng":fAng
             }
         return out
                   
     def applyOffsetGrid(self,x0,y0):
         
         if len(self.vel_raw) > 0:
             for el in self.vel_raw:
                 el.x = el.x + x0
                 el.y = el.y + y0
                 
         if len(self.vel_filtre) > 0:
             for el in self.vel_filtre:
                 el.x = el.x + x0
                 el.y = el.y + y0
                 
         if isinstance(self.vel_moy,velocities.vel) > 0:
            self.vel_moy.x = self.vel_moy.x + x0
            self.vel_moy.y = self.vel_moy.y + y0
            
     
            
class bathy():
    """ 
    Bathymetry info
    
    """
    def __init__(self): 
        self.X       = []  
        self.Y       = []
        self.Z       = []
        self.vx      = []
        self.vy      = []
        self.vn      = []
        self.xp      = []
        self.alpha   = [] 
        self.rx      = 0
        self.ry      = 0
        self.dxp     = 0
        self.nptMax  = 0
        self.a       = 0
        self.b       = 0
        self.x0      = 0
        self.y0      = 0
        self.Q       = 0
        self.A_wet   = 0
        self.vmean   = 0
        self.ratioQ  = 0
             
             
         
