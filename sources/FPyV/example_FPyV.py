# -*- coding: utf-8 -*-
"""
EXAMPLE FILE FOR F-PyV LIB

FPyV is an object-oriented library used to extract 
and play with Fudaa-LSPIV measurements 
"""
from FPyV import *

#####################
# LSPIV PROJECT CLASS
#####################

# Project initialisation
pivProj = FPyV.lspivProject()

# Open a project with .readfile() : 
pivProj.readfile() # This open a windows to select your file 

# you can also specify the file directory 
pivProj.readfile("/home/myFudaa-LSPIV.lsSpiv.zip")

# Access all the variables 
# -> PIV params 
print(pivProj.PIV_param.IA) # Check IA for PIV 
# -> Orthorectification params
print(pivProj.img_ref.res)  # Check orthoimage resolution

# Compute the mean velocity 
pivProj.aggreg(median=False)
# Compute the median velocity 
pivProj.aggreg(median=True)

# Show the results 10 first results with the ortho-image : 
pivProj.arrow2D("filtre",range(10))

# Show the mean results with the ortho-image : 
pivProj.arrow2D("moy")

# Apply temporal filtering (temporal distrib./streamwise dispersion/angular dispersion see https://doi.org/10.1029/2023WR034740 )
pivProj.temporalFiltering("filtre")

#####################
# VELOCITY CLASS
#####################
""" Velocities are stored in a specific class (see velocities.py) 
You can access each value as an object oriented attribute : .x , .vx, .n, .theta 
The results are stored in 2D arrays (m,n) 
    -> 1st dimension m represent the point number on the grid
    -> 2nd dimension n represent the result number 
    """
# To have all the results of the filtered x-velocity : 
xVel     = pivProj.vel_filtre.vx
# To have the raw norm of all the grid points on the first frame : 
velFr1   = pivProj.vel_raw.n[:,0]
# To have the raw norm of the 10th grid points on all the frames : 
velPt10  = pivProj.vel_raw.n[10,:]

# If you want to apply filters from the raw velocities, first initialise the vel_fitre
# with a copy of the raw velocities : 
pivProj.vel_filtre = pivProj.vel_raw.copy() 

# Then apply your favourite filters ! 
pivProj.vel_filtre.medianTest()              # median Test, see https://doi.org/10.1029/2023WR034740
pivProj.vel_filtre.filter("nij", 0.75, 1E30) # Classical filter with ("quantity I want to filter",MIN,MAX)
pivProj.vel_filtre.filter("r", 0.4, 1)       # Classical filter with ("quantity I want to filter",MIN,MAX)


