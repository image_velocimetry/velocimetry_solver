#!/usr/bin/env python3

# Image_Velocimetry_Solver is free software; you can redistribute it a
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# Alternatively, you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# Image_Velocimetry_Solver is distributed in the hope that it will be
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTA
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# or the GNU General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
# License and a copy of the GNU General Public License along with-
# Image_Velocimetry_Solver. If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

if __name__ == "__main__":
    main()
