!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

module util_solver_appli
   use iso_fortran_env, only: error_unit, output_unit
   use errors, only: ERR_BAD_COMMAND
   !use lib_LSPIV, only: dp
   !integer,parameter :: dp = kind(1.0d0)
   use lspiv_parameter, only: dp

   !derived types
   type point2D
      real(kind=dp) :: x
      real(kind=dp) :: y
   end type point2D

   procedure (compute_XY_2D), pointer :: compute_XY => null()

contains
   function duration(vtime1,vtime2)
   !time between vtime1 and vtime2
   !vtime1 & vtime2 are assumed to be resulting from date_and_time subroutine
      implicit none
      ! -- prototype --
      integer, intent(in) :: vtime1(8), vtime2(8)
      real(kind=dp) :: duration

      duration = (vtime2(5)*3600.+vtime2(6)*60.+vtime2(7)+vtime2(8)*0.001) &
                -(vtime1(5)*3600.+vtime1(6)*60.+vtime1(7)+vtime1(8)*0.001) &
                +(vtime2(3)-vtime1(3))*86400.
   end function duration


   logical function is_valid_email(user_email)
   ! check if the user_email is a valid email
   ! one word : no blank space
   ! an @ and a dot after the @
      implicit none
      ! -- prototype --
      character (len=*), intent(in) :: user_email
      !logical :: is_valid_email

      if (scan(trim(user_email),' ') > 0) then
         write(error_unit,'(2a)') '>>>> ERROR, invalid email, it contains a blank space : ',user_email
         is_valid_email = .false.
      elseif (scan(trim(user_email),'@') == 0) then
         write(error_unit,'(2a)') '>>>> ERROR, invalid email, it does not contain an @ : ',user_email
         is_valid_email = .false.
      elseif (scan(trim(user_email),'.') == 0) then
         write(error_unit,'(2a)') '>>>> ERROR, invalid email, it does not contain an dot : ',user_email
         is_valid_email = .false.
      elseif (scan(trim(user_email),'.',back=.true.) < scan(user_email,'@')) then
         write(error_unit,'(2a)') '>>>> ERROR, invalid email, no dot after the @ : ',user_email
         is_valid_email = .false.
      else
         is_valid_email = .true.
      endif
   end function is_valid_email


   logical function is_inside(xy,p,np)
   ! compute if point2D XY is inside the polygon P given as an array of point2D
   ! we assume the polygon is closed, that is p(np) = p(1)
      implicit none
      ! -- prototype --
      integer, intent(in) :: np
      type(point2D), intent(in) :: xy, p(np)
      ! -- local variables --
      real(kind=dp), parameter :: pi = 2._dp*asin(1._dp) !3.14159265359_dp
      integer :: i, j
      real(kind=dp) :: somme, det, nrm1, nrm2, sin_alpha, pscal, cos_alpha

      somme = 0._dp
      do i = 1, np-1
         j = i+1
         det = (p(i)%x-xy%x)*(p(j)%y-xy%y) - (p(i)%y-xy%y)*(p(j)%x-xy%x)
         pscal = (p(i)%x-xy%x)*(p(j)%x-xy%x) + (p(i)%y-xy%y)*(p(j)%y-xy%y)
         nrm1 = sqrt((p(i)%x-xy%x)**2 + (p(i)%y-xy%y)**2)
         nrm2 = sqrt((p(j)%x-xy%x)**2 + (p(j)%y-xy%y)**2)
         if (abs(nrm1*nrm2) < 1.0e-20_dp) then !xy = p(i) or xy = p(i+1)
            is_inside = .true.
            return
         else
            sin_alpha = det/(nrm1*nrm2)
            cos_alpha = pscal/(nrm1*nrm2)
            if (abs(cos_alpha) <= 1) then
               somme = somme + sign(1._dp,sin_alpha)*acos(cos_alpha)
            endif
         endif
      enddo
      somme = abs(somme) !important car on a pu tourner dans un sens ou l'autre
      is_inside = abs(somme-2._dp*Pi) <= Pi
   end function is_inside


   function ortho_rectif(p,a,resol) result(q)
   !transformation des coordonnées image (x,y) sur l'image brute en coordonnées image (pixels) sur l'image orthorectifiée
      implicit none
      ! -- protype --
      type (point2D), intent(in) :: p
      real(kind=dp), intent(in) :: a(*), resol
      type (point2D) :: q

      q%x = aint((a(1)*p%x+a(2)*p%y+a(3))/(a(7)*p%x+a(8)*p%y+1._dp) / resol)
      q%y = aint((a(4)*p%x+a(5)*p%y+a(6))/(a(7)*p%x+a(8)*p%y+1._dp) / resol)
   end function ortho_rectif


   function compute_XY_2D(ix,iy,a) result(q)
   !Compute XY metric coordinates (q) from pixels coordinates (ix,iy) on the source image
   !case 2D
      implicit none
      ! -- protype --
      integer, intent(in) :: ix, iy
      real(kind=dp), intent(in) :: a(*)
      type (point2D) :: q

      q%x = (a(1)*ix+a(2)*iy+a(3))/(a(7)*ix+a(8)*iy+1._dp)
      q%y = (a(4)*ix+a(5)*iy+a(6))/(a(7)*ix+a(8)*iy+1._dp)
   end function compute_XY_2D


   function compute_XY_3D(ix,iy,a) result(q)
   !Compute XY metric coordinates (q) from pixels coordinates (ix,iy) on the source image
   !case 2D
      implicit none
      ! -- protype --
      integer, intent(in) :: ix, iy
      real(kind=dp), intent(in) :: a(*)
      type (point2D) :: q

      !compute the orthorectified position Xt,Yt and position error dev for each GRP
      !q%x = (m1*ip%x+m2*ip%y+m3)/(p1*ip%x+p2*ip%y+p3)
      !q%y = (q1*ip%x+q2*ip%y+q3)/(p1*ip%x+p2*ip%y+p3)
      q%x = (a(1)*ix+a(2)*iy+a(3))/(a(4)*ix+a(5)*iy+a(6))
      q%y = (a(7)*ix+a(8)*iy+a(9))/(a(4)*ix+a(5)*iy+a(6))
   end function compute_XY_3D


   subroutine read_last_line(lw,last_line)
   !return the lat line of the file connected to unit lw
      implicit none
      ! -- prototype --
      integer, intent(in) :: lw
      character(len=80), intent(out) :: last_line
      ! -- local variables --
      integer :: ios
      character(len=80) :: ligne

      rewind(lw)
      do
         read(lw,'(a)',iostat=ios) ligne
         if (ios /= 0) exit
         last_line = ligne
      enddo
   end subroutine read_last_line


   subroutine gen_white_PNG(row, col, PNG_file)
   ! generate a white PNG image with size row rows x col columns
      implicit none
      ! -- prototype --
      character(len=*), intent(in) :: PNG_file
      integer, intent(in) :: row, col
      ! -- local variables
      character(len=:), allocatable :: command
      character(len=9) :: PGM_file = 'white.pgm'
      integer :: lw, color = 8, i, j, istop

      !generate a PGM image
      open(newunit=lw,file=PGM_file,form='formatted',status='unknown')
      write(lw,'(a2)') 'P2'
      write(lw,'(2i5)') col,row
      write(lw,'(a1)') '8'
      do i = 1, row
         write(lw,'(35(i1,1x))') (color,j=1,col)
      enddo
      close(lw)

      allocate(character(len=10+len_trim(PGM_file)+len_trim(PNG_file)) :: command)
      write(command,'(3(a,1x))') 'convert', PGM_file, PNG_file
      istop = 0 ; call run_external_program(command,.true.,istop)

      !removing temp file PGM_file
      open(newunit=lw,file=trim(PGM_file),status='old')
      close(lw,status='delete')
   end subroutine gen_white_PNG


   subroutine run_external_program(command,bwait,istop)
   ! launch command and manage errors
      implicit none
      ! -- prototype --
      character(len=*), intent(in) :: command
      logical, intent(in) :: bwait
      integer, intent(out) :: istop
      ! -- local variables --
      integer :: cstat
      character(len=120) :: err_message

      istop = 0
      if (bwait) then
         call execute_command_line(command,wait=.true.,exitstat=istop,cmdstat=cstat,cmdmsg=err_message)
      else
         call execute_command_line(command,wait=.false.,cmdstat=cstat,cmdmsg=err_message)
      endif
      if (cstat > 0) then
         write(output_unit,'(a,/,a)') 'Something went wrong with the command : ',trim(command)
         write(output_unit,'(a)') trim(err_message)
         write(error_unit,'(a,/,a)') 'Something went wrong with the command : ',trim(command)
         write(error_unit,'(a)') trim(err_message)
         !ignoring case "Termination status of the command-language interpreter cannot be obtained"
         if (cstat /= 1) stop ERR_BAD_COMMAND
      endif
   end subroutine run_external_program


   pure function f0p(x,p)
   ! hack tordu pour forcer l'écriture du zéro avant le séparateur décimal pour un format f0.p
      implicit none
      ! -- prototype --
      real(kind=dp), intent(in) :: x
      integer, intent(in) :: p
      character(len=20) :: f0p
      ! -- variable --
      character(len=18) :: tmp
      character(len=6) :: myfmt

      write(myfmt,'(a4,i1,a1)') '(f0.',p,')'
      write(tmp,fmt=myfmt) x
      if (tmp(1:1) == '.') then
         f0p = '0'//tmp(1:)
      elseif (tmp(1:2) == '-.') then
         f0p = '-0'//tmp(2:)
      else
         f0p = tmp(1:)
      endif
   end function f0p


   logical function is_on_right_side(P0,P1,P)
   !determine if point P is on the right of the segment [P0,P1]
   !is the case if the determinant of vectors P0P1 and P0P is negative
      implicit none
      ! -- prototype --
      type(point2D), intent(in) :: P0, P1, P
      ! -- local variables --
      real(kind=dp) :: v1x, v2x, v1y, v2y
      v1x = p1%x - p0%x  ;  v1y = p1%y - p0%y
      v2x = p%x - p0%x   ;  v2y = p%y - p0%y

      is_on_right_side = ((v1x * v2y - v1y * v2x) <= 0._dp)
   end function is_on_right_side


   logical function is_equal_p2d(P,Q)
   !return true if p%x == q%x and p%y == q%y
      implicit none
      ! -- prototype --
      type(point2D), intent(in) :: P, Q
      ! -- local variables --
      real(kind=dp), parameter :: eps = 0.001_dp

      is_equal_p2d = (abs(p%x-q%x) + abs(p%y-q%y)) < eps

   end function is_equal_p2d


   subroutine jarvis(p,np,lp,nv)
   !compute the convex envlop of {P(i), i=1,np}
   !the result is the list of index in p of the vertices {lp(i), i=1,nv}
   !the list lp starts with the vertex with minimal y-coordinate
      implicit none
      ! -- prototype --
      integer, intent(in) :: np
      type(point2D), intent(in) :: p(np)
      integer, intent(out) :: nv
      !type(point2D), intent(out) :: envlop(np)
      integer, intent(out) :: lp(np)  !indice dans la liste p(:)
      ! -- local variables --
      integer :: k, j, ka, kc
      real(kind=dp) :: ymin
      type(point2D) :: pc

      !step 1 : find the point p(i) with minimal y-coordinate
      !         this point is on the envlop and will be the starting point
      lp(1) = 1
      do k = 2, np
         if (p(k)%y < p(lp(1))%y) lp(1) = k
      enddo

      !step 2 : compute convex envlop
      kc = lp(1)  ;  nv = 1
      do j = 2, np
         !choose ka such that p(ka) =/= p(kc)
         do k = 1, np
            if (is_equal_p2d(p(k),p(kc))) then
               cycle
            else
               ka = k
               exit
            endif
         enddo
         !compute the rightmost point from pc
         do k = 1, np
            if (is_equal_p2d(p(k),p(kc)) .or. is_equal_p2d(p(k),p(ka))) cycle
            if (is_on_right_side(p(kc),p(ka),p(k))) ka = k
         enddo
         !print*,'jarvis : ',pa
         if (is_equal_p2d(p(ka),p(lp(1)))) then
            !print*,'retour au point n°1'
            exit
         else
            kc = ka
            nv  = nv + 1
            lp(nv) = ka
         endif
      enddo
   end subroutine jarvis


   function length(A,B)
   !compute the length of the segment [A,B]
      implicit none
      !--prototype --
      type(point2D), intent(in) :: A, B
      real(kind=dp) :: length

      length = sqrt( (A%x-B%x)**2 + (A%y-B%y)**2)
   end function length


   function heron(A,B,C)
   !compute the area of triangle (A,B,C) using Heron formula
      implicit none
      !--prototype --
      type(point2D), intent(in) :: A, B, C
      real(kind=dp) :: heron
      !--local variables --
      real(kind=dp) :: p, la, lb, lc
      !compute the length of each side
      la = length(B,C)  ;  lb = length(A,C)  ;  lc = length(A,B)

      p = 0.5_dp * (la + lb + lc)
      heron = sqrt(p*(p-la)*(p-lb)*(p-lc))
   end function heron


   function area_poly_convex(p,np)
   !compute the area of the convex polygon given by p(:)
      implicit none
      !-- prototype --
      integer, intent(in) :: np
      type(point2D), intent(in) :: p(np)
      real(kind=dp) :: area_poly_convex
      !-- local variables --
      real(kind=dp) :: area
      integer :: k

      area_poly_convex = 0._dp
      do k = 2, np-1
         area_poly_convex = area_poly_convex + heron(p(1),p(k),p(k+1))
      enddo
   end function area_poly_convex


   function number_interior_pixels(p,np,ni,nj,a)
   !compute the amount of pixels inside the convex polygon p
   !we assume that p is closed : p(1) = p(np)
   ! ni, nj = size of the source image
   ! a = orthorectification coefficients
      implicit none
      !-- prototype --
      integer, intent(in) :: np, ni, nj
      type(point2D), intent(in) :: p(np)
      real(kind=dp), intent(in) :: a(*)
      integer :: number_interior_pixels
      !-- local variables --
      integer :: i, j, num
      type(point2D) :: q, iq

      num = 0

      do j = 1, nj     !row index <-> y coordinate
         do i = 1, ni  !column index <-> x coordinate
            q = compute_XY(i,j,a)
            if (is_inside(q,p,np)) num = num + 1
         enddo
      enddo
      number_interior_pixels = num
   end function number_interior_pixels


   function estimated_resolution(GRP_dat,ni,nj,a)
   !compute the resolution from the GRP
      implicit none
      ! -- prototype
      character(len=*), intent(in) :: GRP_dat
      integer, intent(in) :: ni, nj
      real(kind=dp), intent(in) :: a(*)
      real(kind=dp) :: estimated_resolution
      !--local variables --
      type(point2D), allocatable :: xy(:), p(:)
      integer, allocatable :: lp(:)
      real(kind=dp) :: x, y, z, area
      integer :: lu, n, ix, jy, nv, nb_points, num

      !reading GRP.dat
      open(newunit=lu,file=trim(GRP_dat),form='formatted',status='old')
      read(lu,*)
      read(lu,*) nb_points
      read(lu,*)
      allocate (xy(nb_points),lp(nb_points))
      do n = 1, nb_points
         read(lu,*) x, y, z, ix, jy
!          xy(n) = point2D(x,y)
         xy(n) = compute_XY(ix,jy,a)
      enddo
      close (lu)

      !computing the convex hull using metric coordinate
      call jarvis(xy,nb_points,lp,nv)

      !computing area of convex hull enclosing the GRP
      allocate(p(nv+1))
      do n = 1, nv
         p(n) = xy(lp(n))
      enddo
      area = area_poly_convex(p,nv)

      !computing the amount of pixels inside de convex hull
      !closing the polygon
      nv = nv + 1
      p(nv) = p(1)
      num = number_interior_pixels(p,nv,ni,nj,a)

      estimated_resolution = sqrt(area/real(num,kind=dp))
   end function estimated_resolution


   subroutine compute_A_coeff(coeff_file, a, nbc)
   !compute the coefficients for functions compute_XY()
      use iso_fortran_env, only: error_unit, output_unit
      use lspiv_parameter, only: dp
      implicit none
      !-- prototype --
      character(len=*), intent(in) :: coeff_file
      real(kind=dp), intent(out) :: a(9)
      integer, intent(out) :: nbc
      !--local variables --
      logical :: bExist
      integer :: lu, ios, n
      real(kind=dp) :: a1, a2, a3, a4, c1, c2, c3, b1, b2, b3, b4
      real(kind=dp) :: m1, m2, m3, p1, p2 , p3, q1, q2, q3

      !read coeff.dat
      open(newunit=lu,file=trim(coeff_file),form='formatted',status='old')
      read(lu,*) nbc

      if (nbc == 8) then
         do n = 1, nbc
            read(lu,*) a(n)
         enddo
      elseif (nbc == 11) then
         read(lu,*) a1  ;  read(lu,*) a2  ;  read(lu,*) a3  ;  read(lu,*) a4
         read(lu,*) c1  ;  read(lu,*) c2  ;  read(lu,*) c3
         read(lu,*) b1  ;  read(lu,*) b2  ;  read(lu,*) b3  ;  read(lu,*) b4
   !     compute inverse transformation coefficients m, p, q assuming zp = 0 (water level)
         m1 =  b2-b4*c2
         m2 = -(a2-a4*c2)
         m3 =  a2*b4-a4*b2
         p1 =  b1*c2-b2*c1
         p2 = -(a1*c2-a2*c1)
         p3 =  a1*b2-a2*b1
         q1 = -(b1-b4*c1)
         q2 =  a1-a4*c1
         q3 = -(a1*b4-a4*b1)

         a(1) = m1  ;  a(2) = m2  ;  a(3) = m3
         a(4) = p1  ;  a(5) = p2  ;  a(6) = p3
         a(7) = q1  ;  a(8) = q2  ;  a(9) = q3
      endif
      close(lu)
   end subroutine compute_A_coeff

end module util_solver_appli

