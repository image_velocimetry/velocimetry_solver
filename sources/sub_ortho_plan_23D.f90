!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

subroutine sub_ortho(workdir)
!     compute the 3D ortho-rectification parameters from GRPs.
!     The file GRP.dat should be formated as:
!     - 3 lines of header:
!         - The title: "GRP"
!         - The number of GRPs used; for example: "7". At least 6 GRPs are required.
!         - The label of the columns: "X  Y  Z  i  j"
!     - The GRPs coordinates: Space coordinates and image coordinates

!    ajout MJ du 03-05-2011
!    calcul des coef transfo 2D si les z des GRP sont identiques
!    modified by JLC on 6 March 2018 to reduce the GRP coordinates (3D orthorectification only)

   implicit none
   ! protype
   character (len=*), intent(in) :: workdir !working directory

   integer :: Nb  !number of GRPs
   integer :: i, a
   integer,allocatable :: x(:), y(:)

   real(kind=dp),allocatable :: T(:,:), Z(:,:), Tt(:,:), TtTTt(:,:), TtT(:,:), TZ(:,:)
   real(kind=dp),allocatable :: xp(:), yp(:), zp(:), TZtemp(:,:)
   real(kind=dp) :: Dx, Dy, Dz, beta

   integer :: lw

   a = 1
   !open and read GRP file GRP.dat
   open(newunit=lw,file=trim(workdir)//'/'//GRP,status='old')
   read(lw,*)
   read(lw,*) Nb
   read(lw,*)

   allocate(x(Nb),y(Nb),xp(Nb),yp(Nb),zp(Nb))

   do i=1,Nb
      read(lw,*)xp(i),yp(i),zp(i),x(i),y(i)
   enddo
   close(lw)

   !condition pour ortho 2d ou 3d
   do i=2,Nb
      if ( abs(zp(1)-zp(i)) .le. 0.00001_dp) then
         a = a+1
      endif
   enddo

   ! a = Nb si tous les zGRP sont identiques -> transfo 2D -----------------------------------------------------------------
   if (a .eq. Nb) then
      allocate(T(2*Nb,8),Z(2*Nb,1),Tt(8,2*Nb),TtTTt(8,2*Nb),TtT(8,8),TZ(8,1),TZtemp(8,1))
      !format the data onto the matrix T
      do i = 1, Nb
         T(i,1) = real(x(i),kind=dp)
         T(i,2) = real(y(i),kind=dp)
         T(i,3) = 1._dp
         T(i,4) = real(-x(i),kind=dp)*xp(i)
         T(i,5) = real(-y(i),kind=dp)*xp(i)
         T(i,6) = 0._dp
         T(i,7) = 0._dp
         T(i,8) = 0._dp
      enddo
      do i = Nb+1, 2*Nb
         T(i,1) = 0._dp
         T(i,2) = 0._dp
         T(i,3) = 0._dp
         T(i,4) = real(-x(i-Nb),kind=dp)*yp(i-Nb)
         T(i,5) = real(-y(i-Nb),kind=dp)*yp(i-Nb)
         T(i,6) = real(x(i-Nb),kind=dp)
         T(i,7) = real(y(i-Nb),kind=dp)
         T(i,8) = 1._dp
      enddo
      !format the data onto the matrix Z
      do i = 1, Nb
         Z(i,1) = real(xp(i),kind=dp)
      enddo
      do i = Nb+1, 2*Nb
         Z(i,1) = real(yp(i-Nb),kind=dp)
      enddo

      !compute Tt, transposed matrix of T
      call trans(T,Tt,2*Nb,8)
      !multiply Tt with T
      call mmult(Tt,T,TtT,8,2*Nb,8)
      !compute the inverse matrix of TtT
      call gaussj(TtT,8,8)
      !multiply the inverse matrix with Tt
      call mmult(TtT,Tt,TtTTt,8,8,2*Nb)
      !multiply TtTTt with Z to get TZ, the vector of parameters
      call mmult(TtTTt,Z,TZ,8,2*Nb,1)

      !write the results in the output file coeff.dat
      open(newunit=lw,file=trim(workdir)//'/'//coeff_dat,status='unknown')
         write(lw,*)8
         do i = 1, 3
            write(lw,*) TZ(i,1)
         enddo
         write(lw,*) TZ(6,1)
         write(lw,*) TZ(7,1)
         write(lw,*) TZ(8,1)
         write(lw,*) TZ(4,1)
         write(lw,*) TZ(5,1)
      close(lw)

   else
! calcul coeff 3D -----------------------------------------------------------------
      allocate(T(2*Nb,11),Z(2*Nb,1),Tt(11,2*Nb),TtTTt(11,2*Nb),TtT(11,11),TZ(11,1),TZtemp(11,1))

      !reduce the GRP coordinates
      Dx = -minval(xp) !-sum(xp)/Nb
      Dy = -minval(yp) !-sum(yp)/Nb
      Dz = -minval(zp) !-sum(zp)/Nb
      do i = 1, Nb
      xp(i) = xp(i) + Dx
      yp(i) = yp(i) + Dy
      zp(i) = zp(i) + Dz
      !write(*,'(3f10.3)') xp(i),yp(i),zp(i)
      enddo

      !format the data onto the matrix T
      do i = 1, Nb
         T(i,1) = xp(i)
         T(i,2) = yp(i)
         T(i,3) = zp(i)
         T(i,4) = 1._dp
         T(i,5) = real(-x(i),kind=dp)*xp(i)
         T(i,6) = real(-x(i),kind=dp)*yp(i)
         T(i,7) = real(-x(i),kind=dp)*zp(i)
         T(i,8) = 0._dp
         T(i,9) = 0._dp
         T(i,10) = 0._dp
         T(i,11) = 0._dp
      enddo
      do i = Nb+1,2*Nb
         T(i,1) = 0._dp
         T(i,2) = 0._dp
         T(i,3) = 0._dp
         T(i,4) = 0._dp
         T(i,5) = real(-y(i-Nb),kind=dp)*xp(i-Nb)
         T(i,6) = real(-y(i-Nb),kind=dp)*yp(i-Nb)
         T(i,7) = real(-y(i-Nb),kind=dp)*zp(i-Nb)
         T(i,8) = xp(i-Nb)
         T(i,9) = yp(i-Nb)
         T(i,10) = zp(i-Nb)
         T(i,11) = 1._dp
      enddo

      !format the data onto the matrix Z
      do i = 1, Nb
         Z(i,1) = real(x(i),kind=dp)
      enddo
      do i = Nb+1, 2*Nb
         Z(i,1) = real(y(i-Nb),kind=dp)
      enddo

      !compute Tt, transposed matrix of T
      call trans(T,Tt,2*Nb,11)
      !multiply Tt with T
      call mmult(Tt,T,TtT,11,2*Nb,11)
      !compute the inverse matrix of TtT
      call gaussj(TtT,11,11)
      !multiply the inverse matrix with Tt
      call mmult(TtT,Tt,TtTTt,11,11,2*Nb)
      !multiply TtTTt with Z to get TZ, the vector of parameters
      call mmult(TtTTt,Z,TZ,11,2*Nb,1)

      !compute orthorectification coefficients for NON reduced coordinates
         ! store coefficients computed for reduced GRP coordinates in TZtemp
         do i = 1, 11
            TZtemp(i,1) = TZ(i,1)
         enddo

         beta = TZtemp(5,1)*Dx + TZtemp(6,1)*Dy + TZtemp(7,1)*Dz + 1
         do i = 1, 3
            TZ(i,1) = TZtemp(i,1) / beta
         enddo
         TZ(4,1) = (TZtemp(4,1)+TZtemp(1,1)*Dx+TZtemp(2,1)*Dy+TZtemp(3,1)*Dz)/beta
         do i = 5, 10
            TZ(i,1) = TZtemp(i,1) / beta
         enddo
         TZ(11,1) = (TZtemp(11,1)+TZtemp(8,1)*Dx+TZtemp(9,1)*Dy+TZtemp(10,1)*Dz)/beta

      !write the results in the output file coeff.dat
      open(newunit=lw,file=trim(workdir)//'/'//coeff_dat,status='unknown')
         write(lw,*) 11
         do i = 1, 11
            write(lw,*) TZ(i,1)
         enddo
      close(lw)
   endif
end subroutine sub_ortho


subroutine trans(A,At,m,n)
!Transpose a matrix or vector A(m,n) onto At(n,m)
   implicit none
   integer :: i, j, m, n
   real(kind=dp) :: A(m,n), At(n,m)

   do i = 1, n
      do j = 1, m
         At(i,j) = A(j,i)
      enddo
   enddo
end subroutine trans


subroutine mmult(A,B,C,m,n,o)
!Multiplication of 2 matrix or vectors
!   A(m,n)xB(n,o)=C(m,o)
   implicit none
   integer :: i, j, k, m, n, o
   real(kind=dp) :: A(m,n), B(n,o), C(m,o), part

   do i = 1, m
      do j = 1, o
         C(i,j) = 0._dp
         do k = 1,n
            part = A(i,k)*B(k,j)
            C(i,j) = C(i,j)+part
         enddo
      enddo
   enddo
end subroutine mmult


subroutine gaussj(a,n,np)
   !Linear equation solution by Gauss-Jordan elimination, equation (2.1.1) above. a(1:n,1:n)
   !is an input matrix stored in an array of physical dimensions np by np. b(1:n,1:m) is an input
   !matrix containing the m right-hand side vectors, stored in an array of physical dimensions
   !np by mp. On output, a(1:n,1:n) is replaced by its matrix inverse, and b(1:n,1:m) is
   !replaced by the corresponding set of solution vectors.
   implicit none

   integer, parameter :: nmax=50
   !parameter: nmax is the largest anticipated value of n.
   integer :: n,np
   real(kind=dp) :: a(np,np)
   integer :: i,icol,irow,j,k,l,ll,indxc(nmax),indxr(nmax),ipiv(nmax)
   !the integer arrays ipiv, indxr, and indxc are used
   !for bookkeeping on the pivoting.
   real(kind=dp) :: big,dum,pivinv

   ipiv(1:n) = 0
   irow = -1  ;  icol = -1  !initialisation
   do i = 1, n
   !This is the main loop over the columns to be reduced.
      big = 0._dp
      do j = 1, n
      !This is the outer loop of the search for a pivot element.
         if(ipiv(j).ne.1)then
            do k = 1,n
               if (ipiv(k).eq.0) then
                  if (abs(a(j,k)).ge.big) then
                     big = abs(a(j,k))
                     irow = j
                     icol = k
                  endif
               endif
            enddo
         endif
      enddo
      ipiv(icol) = ipiv(icol)+1
      !We now have the pivot element, so we interchange rows, if needed, to put the pivot
      !element on the diagonal. The columns are not physically interchanged, only relabeled:
      !indxc(i), the column of the ith pivot element, is the ith column that is reduced, while
      !indxr(i) is the row in which that pivot element was originally located. If indxr(i)  =
      !indxc(i) there is an implied column interchange. With this form of bookkeeping, the
      !solution will end up in the correct order, and the inverse matrix will be scrambled
      !by columns.
      if (irow.ne.icol) then
         do l = 1,n
            dum = a(irow,l)
            a(irow,l) = a(icol,l)
            a(icol,l) = dum
         enddo
      endif
      indxr(i) = irow
      !We are now ready to divide the pivot row by the pivot
      !element, located at irow and icol.
      indxc(i) = icol
      if (abs(a(icol,icol)) < 1.e-20_dp) print *, 'singular matrix in gaussj'
      pivinv = 1./a(icol,icol)
      a(icol,icol) = 1._dp
      do l = 1,n
         a(icol,l) = a(icol,l)*pivinv
      enddo

      do ll = 1,n
         !Next, we reduce the rows...
         if(ll.ne.icol)then
            !...except for the pivot one, of course.
            dum = a(ll,icol)
            a(ll,icol) = 0._dp
            do l = 1,n
               a(ll,l) = a(ll,l)-a(icol,l)*dum
            enddo
         endif
      enddo
   enddo

   !This is the end of the main loop over columns of the reduction.
   do l = n,1,-1
      !It only remains to unscramble the solution in view
      !of the column interchanges. We do this by interchanging
      !pairs of columns in the reverse order
      !that the permutation was built up.
      if (indxr(l).ne.indxc(l)) then
         do k = 1, n
            dum = a(k,indxr(l))
            a(k,indxr(l)) = a(k,indxc(l))
            a(k,indxc(l)) = dum
         enddo
      endif
   enddo
end subroutine gaussj
