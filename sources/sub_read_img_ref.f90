!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

subroutine read_img_ref(workdir,xmin, ymin, xmax, ymax, r, width, height)
!Read the image information file img_ref.dat
!X -> abscissa
!Y -> ordinate
!origin = left bottom corner
!r = resolution (m/pixel)
!width & height = image size
   implicit none
   ! -- prototype --
   character (len=*), intent(in) :: workdir
   real(kind=dp), intent(out) :: xmin, ymin, xmax, ymax, r
   integer, intent(out) :: width, height
   ! -- local variables --
   integer :: lw

   open(newunit=lw,file=trim(workdir)//'/'//img_ref, status='old')
   read(lw,*) ; read(lw,*) xmin, ymin
   read(lw,*) ; read(lw,*) xmax, ymax
   read(lw,*) ; read(lw,*) r
   read(lw,*) ; read(lw,*) height, width
   close(lw)
end subroutine read_img_ref
