!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

subroutine read_coeff_ortho(workdir,n,a)
!read the file coeff.dat for sub_transf_img
   implicit none
   ! -- prototype --
   character (len=*), intent(in) :: workdir
   integer, intent(out) :: n
   real(kind=dp), intent(out) :: a(11)
   ! -- local variables --
   integer :: lw, i

   open(newunit=lw,file=trim(workdir)//'/'//coeff_dat,form='formatted',status='old')
   read(lw,*) n
   if (n == 8) then
      do i = 1, 8
         read(lw,*) a(i)
      enddo
   elseif (n == 11) then
      do i = 1, 4
         read(lw,*) a(i)
      enddo
      do i = 1, 3
         read(lw,*) a(i+8)
      enddo
      do i = 1, 4
         read(lw,*) a(i+4)
      enddo
   elseif (n == 0) then
      read(lw,*) a(1)
      read(lw,*) a(2)
   endif
   close (lw)
end subroutine read_coeff_ortho
