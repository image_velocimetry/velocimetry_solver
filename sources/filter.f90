!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
!
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works
! on LSPIV by Ichiro Fujita and Marian Muste.
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

include  'sub_lspiv_parameter.f90'

program filter

   use lspiv_parameter
   implicit none
   character (len=160) :: param1, param2, param3
   real(kind=dp) :: dt

   param3 = ''
#ifdef version_station
   call get_command_argument(1,param1)
   call get_command_argument(2,param2)
   call get_command_argument(3,param3)
#endif /* version_station */

#ifndef version_station
   param1 = './'//outputs_dir//'/'
   param2 = './'
   call get_command_argument(1,param3)
#endif /* version_station */
   if (len_trim(param3) > 0) then
     read (param3,*) dt
     call sub_filter(param1,param2,dt)
   else
     call sub_filter(param1,param2)
   endif


contains

include 'sub_filter.f90'

end program filter
