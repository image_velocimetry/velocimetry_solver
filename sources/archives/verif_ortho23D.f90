! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

program verif_ortho

!     check 3D ortho-rectification parameters by computing the orthorectified space coordinates (Xt,Yt) of the GRPs from their image coordinates (i,j) and vertical space coordinate (Z)
!    modif du 1-06-2011 MJ pour prise en compte des orthorectification 2d ou 3d

 
!     The input file GRP.dat should be formated as:
!     - 3 lines of header:
!         - The title: "GRP"
!         - The number of GRPs used; for example: "7". At least 6 GRPs are required. 
!         - The label of the columns: "X  Y  Z  i  j"
!     - The GRPs coordinates: Space coordinates and image coordinates

!      The input file coeff.dat is produced by solver "ortho" it should be formated as :
!     - nb of coefficients si nb=8 transfo 2d (8 coeff) si nb=11 transfo 3d (11 coeff)
!    - values of coefficients

!      The output file GRP_test_ortho.dat will be formated as:
!     - 3 lines of header:
!         - The title: "GRP"
!         - The number of GRPs used; for example: "7". At least 6 GRPs are required. 
!         - The label of the columns: "X  Y  Z  i  j  Xt  Yt  Dev"
!     - The GRPs coordinates: Space coordinates and image coordinates

   implicit none

   integer, parameter :: pr=8
   
   integer :: Nb  !number of GRPs
   integer :: nbc  ! number of coefficients
   integer :: i, j, m, n, o
   integer,allocatable :: x(:), y(:)
   real(kind=pr),allocatable :: xp(:), yp(:), zp(:)
   real(kind=pr),allocatable :: xt(:), yt(:), dev(:)      
   real(kind=pr),allocatable :: m1(:), m2(:), m3(:)
   real(kind=pr),allocatable :: p1(:), p2(:), p3(:)      
   real(kind=pr),allocatable :: q1(:), q2(:), q3(:)      
   real(kind=pr) :: a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3

   !open and read GRP file GRP.dat !!! REPRENDRE MODIF ORTHO JLC !!!
   open(10,file='outputs.dir/GRP.dat',status='old')
   read(10,*)
   read(10,*) Nb
   read(10,*)
   
   allocate(x(Nb),y(Nb))  
   allocate(xp(Nb),yp(Nb),zp(Nb))   
   allocate(xt(Nb),yt(Nb),dev(Nb))   
   allocate(m1(Nb),m2(Nb),m3(Nb))
   allocate(p1(Nb),p2(Nb),p3(Nb))      
   allocate(q1(Nb),q2(Nb),q3(Nb))      

   do i = 1, Nb
      read(10,*) xp(i), yp(i), zp(i), x(i), y(i)
   enddo
   close(10)
 
   !open and read 3D or 2D ortho-rectification parameters in file coeff.dat
   !si nbc=8 transfo 2d (8 coeff) si nb=11 transfo 3d (11 coeff)
   open(12,file='outputs.dir/coeff.dat',status='old')
   
   read(12,*) nbc
   
   if (nb.eq.11) then
      read(12,*) a1
      read(12,*) a2
      read(12,*) a3
      read(12,*) a4
      read(12,*) c1
      read(12,*) c2
      read(12,*) c3
      read(12,*) b1
      read(12,*) b2
      read(12,*) b3
      read(12,*) b4
      close(12)
!     compute inverse transformation coefficients m, p, q for each GRP
      do i = 1,Nb
         m1(i) =  zp(i)*(b2*c3-b3*c2)+(b2-b4*c2)
         m2(i) = -(zp(i)*(a2*c3-a3*c2)+(a2-a4*c2))
         m3(i) =  zp(i)*(a2*b3-a3*b2)+(a2*b4-a4*b2)
         p1(i) =  b1*c2-b2*c1
         p2(i) = -(a1*c2-a2*c1)
         p3(i) =  a1*b2-a2*b1
         q1(i) = -(zp(i)*(b1*c3-b3*c1)+(b1-b4*c1))
         q2(i) = zp(i)*(a1*c3-a3*c1)+(a1-a4*c1)
         q3(i) = -(zp(i)*(a1*b3-a3*b1)+(a1*b4-a4*b1))
      enddo
      !compute the orthorectified position Xt,Yt and position error dev for each GRP
      do i = 1,Nb
         xt(i) = (m1(i)*x(i)+m2(i)*y(i)+m3(i)) / (p1(i)*x(i)+p2(i)*y(i)+p3(i))
         yt(i) = (q1(i)*x(i)+q2(i)*y(i)+q3(i)) / (p1(i)*x(i)+p2(i)*y(i)+p3(i))
         dev(i) = sqrt( (xt(i)-xp(i))*(xt(i)-xp(i)) + (yt(i)-yp(i))*(yt(i)-yp(i)) )
      enddo
      !write the results in the output file GRP_test_ortho.dat
      open(11,file='outputs.dir/GRP_test_ortho.dat',status='unknown')
         write(11,*) 'GRP verification of 3D ortho-rectification'
         write(11,*) Nb
         write(11,*) 'X  Y  Z  i  j  Xt  Yt  Dev'

      do i = 1, Nb
         write(11,*) xp(i),yp(i),zp(i),x(i),y(i),xt(i),yt(i),dev(i)
      enddo
   
      close(11)

      end

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!cc Subroutine library | m�mes que transf23D
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine space2CRT(xp,yp,a,ii,jj,D3)
      
!     Compute image coordinates from space coordinates using a(11) ie 3D
      real a(11),xp,yp,ii,jj,D3

      xp=(a(1)*ii+a(2)*jj+(a(3)*D3+a(4)))
     &     /(a(9)*ii+a(10)*jj+(a(11)*D3+1))
      yp=(a(5)*ii+a(6)*jj+(a(7)*D3+a(8)))
     &     /(a(9)*ii+a(10)*jj+(a(11)*D3+1))

      end                  

	subroutine space2CRT2(xp,yp,b,ii,jj,D3)
      
!     Compute image coordinates from space coordinates using b(9) ie 2D

      real b(9),xp,yp,ii,jj,D3

      xp=(b(1)*ii+b(2)*jj+b(3))/(b(7)*ii+b(8)*jj+b(9));
      yp=(b(4)*ii+b(5)*jj+b(6))/(b(7)*ii+b(8)*jj+b(9));

      end

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

