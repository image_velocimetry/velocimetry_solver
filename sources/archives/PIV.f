      program PIV_alex

c     PIV analysis of a sequence of images, using CDWS with sub-pixel
c     gaussian fit.

      implicit none

      integer i,j,k,l,m,n,compt,compteur,kk
      integer ni,nj,x,y
c      parameter (nj=900,ni=1100)   !taille images transf
      integer,dimension(:,:),allocatable :: imga,imgb
      character*60 imagea,imageb,out,station
      integer Bi,Bj            ! IA size
c      parameter (Bi=30,Bj=30)
      integer Sim,Sjm,Sip,Sjp   ! SA size
c      parameter (Sim=10,Sip=10,Sjm=10,Sjp=50)
c            ----------------------  
c            |         |          |
c            |         |Sim       |
c            |   Sjm   |    Sjp   |
c            ----------------------
c            |         |          |
c            |         |Sip       |
c            |         |          |
c            ----------------------
      real R_min,R_max       ! 
c      parameter (R_min=0.05,R_max=0.95) ! threshold on the correlation coeff

      integer,dimension(:,:),allocatable :: ia,ib
      real sum_ia,sum_ib,t1,t2,t3,sum_t1,sum_t2,sum_t3
      
c modif par JLC le 16/08/08      
c      real c(Sim+Sip,Sjm+Sjp),cmax,imax,jmax,per,dm,dn
      real cmax,per,dm,dn
      real,dimension(:,:),allocatable :: c
      integer imax,jmax

      character*60 BaseName(5000)

c      read(*,*) station
   
c     Read the PIV parameters     
c      open(14,file='config/'//trim(station)//'/PIV_param.dat'  ,status='old')   
      open(14,file='./outputs.dir/PIV_param.dat'  ,status='old')   
	read(14,*)
      read(14,*) Bi
      Bj=Bi
      read(14,*)
      read(14,*) Sim
      read(14,*) Sip
      read(14,*) Sjm
      read(14,*) Sjp
      do i=1,3
         read(14,*)
      enddo
      read(14,*) R_min
      read(14,*)
      read(14,*) R_max
      read(14,*)
      read(14,*) nj
      read(14,*) ni

      allocate(ia(Bi,Bj))
      allocate(ib(Bi,Bj))
      allocate(c(Sim+Sip,Sjm+Sjp))
      allocate(imga(ni,nj))
      allocate(imgb(ni,nj))

	!initialisation
	do m=1,Sim+Sip
	  do n=1,Sjm+Sjp
	     c(m,n)=0.0001
	enddo
	enddo


c     Read the 2 images used
      open(10,file='img_transf/image1_transf.pgm',status='old')
      open(11,file='img_transf/image2_transf.pgm',status='old')
      do i=1,3
         read(10,*)
         read(11,*)
      enddo
      do i=1,ni      
         read(10,*)(imga(i,j),j=1,nj)
         read(11,*)(imgb(i,j),j=1,nj)
      enddo
      close(10)
      close(11)

      
c     Read the grid file
c      open(12,file='config/'//trim(station)// '/grid.dat',status='old')
      open(12,file='./outputs.dir/grid.dat',status='old')
      do i=1,100000
         read(12,*,end=100)x,y
      enddo
 100  continue
      rewind(12)

  
c     PIV analysis
      compt=i-1
      do i=1,compt
         read(12,*,end=100)x,y

         sum_ia=0
         if (((x-Sjm-Bj/2).gt.0).and.((y-Sim-Bi/2).gt.0).and.
     &        ((x+Sjp+Bj/2).lt.nj).and.((y+Sip+Bi/2).lt.ni)) then
         do k=1,Bi
            do l=1,Bj
               ia(k,l)=imga(y+(k-Bi/2),x+(l-Bj/2))
               sum_ia=sum_ia+float(ia(k,l))
            enddo
         enddo
         sum_ia=sum_ia/(Bi*Bj)


         cmax=-99
         do m=1,Sim+Sip 
            do n=1,Sjm+Sjp
               sum_ib=0
               do k=1,Bi
                  do l=1,Bj 
                     ib(k,l)=imgb(y+(m-Sim)+(k-Bi/2),x+(n-Sjm)+(l-Bj/2))
                     sum_ib=sum_ib+float(ib(k,l))                  
                  enddo
               enddo
               sum_ib=sum_ib/(float(Bi*Bj))
               sum_t1=0
               sum_t2=0
               sum_t3=0             
               do k=1,Bi
                  do l=1,Bj                
                     t1=(ia(k,l)-sum_ia)*(ib(k,l)-sum_ib)
                     sum_t1=sum_t1+t1
                     t2=(ia(k,l)-sum_ia)*(ia(k,l)-sum_ia)
                     sum_t2=sum_t2+t2
                     t3=(ib(k,l)-sum_ib)*(ib(k,l)-sum_ib)
                     sum_t3=sum_t3+t3
                  enddo
               enddo
               c(m,n)=abs(sum_t1/(sqrt(sum_t2*sum_t3)))
               ! avoid c=0
               if (c(m,n).eq.0) then
                  c(m,n)=0.0001               
               endif
               if (c(m,n).gt.cmax) then
                  cmax=c(m,n)
                  imax=m-(Sim)
                  jmax=n-(Sjm)
               endif
            enddo
         enddo

c     Gaussian sub-pixel fit
         if ((imax+Sim.gt.1).and.(jmax+Sjm.gt.1).and.   
     &       (imax+Sim.lt.Sim+Sip-1).and.(jmax+Sjm.lt.Sjm+Sjp-1)) then 

         dm=0.5*(alog(c(imax+Sim-1,jmax+Sjm))
     &        -alog(c(imax+Sim+1,jmax+Sjm)))
     &        /(alog(c(imax+Sim-1,jmax+Sjm))
     &        +alog(c(imax+Sim+1,jmax+Sjm))
     &        -2*alog(c(imax+Sim,jmax+Sjm)))
         dn=0.5*(alog(c(imax+Sim,jmax+Sjm-1))
     &        -alog(c(imax+Sim,jmax+Sjm+1)))
     &        /(alog(c(imax+Sim,jmax+Sjm-1))
     &        +alog(c(imax+Sim,jmax+Sjm+1))
     &        -2*alog(c(imax+Sim,jmax+Sjm)))

         else
            dm=0
            dn=0
         endif


        
c     Write results
         open(13,file='./vel_raw/piv.dat',status='unknown')
         
         if ((cmax.ge.R_min).and.(cmax.le.R_max)) then
            if ((dm.gt.-1).and.(dm.lt.1).and.(dn.gt.-1).and.(dn.lt.1))
     &           then 
               write(13,*)y,x,imax+dm,jmax+dn,cmax
            else
               write(13,*)y,x,imax,jmax,cmax
            endif
         else
            write(13,*)y,x,0,0,0
         endif
  
      endif
      enddo
      close(12)
      close(13)
      end

