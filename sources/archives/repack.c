#include <stdio.h>


#define MAXIMAGE (2048*2048)

unsigned char data[MAXIMAGE];

main(int argc, char **argv)
{
  FILE *fp1;
  int i,j,k,nx,ny,depth;
  char filename[] = "in.pgm";
  char buffer[2048]; //1024

  //  if ((fp1=fopen(filename,"r"))==NULL) {
  //  fprintf(stderr,"ERROR: could not open: %s\n",filename);
  //  exit(0);
  //}

  fp1 = stdin;

  // Read the PGM data into "data".
  
  fscanf(fp1,"%s\n",buffer);      // Magic number
  fscanf(fp1,"%d %d\n",&nx,&ny);  // Width (X) and Height (Y) - dimensions
  fscanf(fp1,"%d\n",&depth);      // Color depth
  if (nx*ny >= MAXIMAGE){
    fprintf(stderr,"ERROR: image too big - enlarge image buffer & recompile...\n",filename);
    //    fclose(fp1);
    exit(0);
  }
  k = 0;
  for (i=0;i<=ny-1;i++){
    for (j=0;j<=nx-1;j++){
      fscanf(fp1,"%d",&data[k++]);
    }
  }
  //  fclose(fp1);

  // Write it out in desired format.

  printf("%s\n",buffer);
  printf("%d %d\n",nx,ny);
  printf("%d\n",depth);
  k = 0;
  for (i=0;i<=ny-1;i++){
    for (j=0;j<=nx-1;j++){
      printf(" %3d",data[k++]);
    }
    printf("\n");

  }
  fclose(fp1); 


}




