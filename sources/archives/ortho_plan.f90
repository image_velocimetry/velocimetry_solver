! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

program ortho

!     compute the 3D ortho-rectification parameters from GRPs. 
!     The file GRP.dat should be formated as:
!     - 3 lines of header:
!         - The title: "GRP"
!         - The number of GRPs used; for example: "7". At least 6 GRPs are required. 
!         - The label of the columns: "X  Y  Z  i  j"
!     - The GRPs coordinates: Space coordinates and image coordinates

   implicit none
   
   integer Nb  !number of GRPs
   integer i,j
   integer,dimension(:),allocatable :: x,y
    
   double precision,dimension(:,:),allocatable :: T,Z,Tt,TtTTt
   double precision,dimension(:),allocatable :: xp,yp,zp
   double precision TtT(11,11),TZ(11,1),diff
   
   integer m, n,o

   !open and read GRP file GRP.dat
   open(10,file='outputs.dir/GRP.dat',status='old')
   read(10,*)
   read(10,*) Nb
   read(10,*)

   allocate(x(Nb))
   allocate(y(Nb))  
   allocate(xp(Nb))
   allocate(yp(Nb))  
   allocate(zp(Nb))        
   allocate(T(2*Nb,11))          
   allocate(Z(2*Nb,1))          
   allocate(Tt(11,2*Nb))             
   allocate(TtTTt(11,2*Nb))      
      
   
   do i=1,Nb
      read(10,*) xp(i),yp(i),zp(i),x(i),y(i)
      !write(*,*) xp(i),yp(i),zp(i),x(i),y(i)
   enddo
   close(10)
   
   
   !format the data onto the matrix T
   do i=1,Nb
      T(i,1)=xp(i)
      T(i,2)=yp(i)
      T(i,3)=zp(i)
      T(i,4)=1
      T(i,5)=-x(i)*xp(i)
      T(i,6)=-x(i)*yp(i)
      T(i,7)=-x(i)*zp(i)
      T(i,8)=0
      T(i,9)=0
      T(i,10)=0
      T(i,11)=0
   enddo
   do i=Nb+1,2*Nb
      T(i,1)=0
      T(i,2)=0
      T(i,3)=0
      T(i,4)=0
      T(i,5)=-y(i-Nb)*xp(i-Nb)
      T(i,6)=-y(i-Nb)*yp(i-Nb)
      T(i,7)=-y(i-Nb)*zp(i-Nb)
      T(i,8)=xp(i-Nb)
      T(i,9)=yp(i-Nb)
      T(i,10)=zp(i-Nb)
      T(i,11)=1
   enddo   
   
   !format the data onto the matrix Z
   do i=1,Nb
      Z(i,1)=x(i)
   enddo
   do i=Nb+1,2*Nb
      Z(i,1)=y(i-Nb)
   enddo
   
   !compute Tt, transposed matrix of T
   call trans(T,Tt,2*Nb,11)      
   
   !multiply Tt with T
   call mmult(Tt,T,TtT,11,2*Nb,11)
      
   !compute the inverse matrix of TtT
   call gaussj(TtT,11,11)
   
   !multiply the inverse matrix with Tt
   call mmult(TtT,Tt,TtTTt,11,11,2*Nb)
   
   !multiply TtTTt with Z to get TZ, the vector of parameters
   call mmult(TtTTt,Z,TZ,11,2*Nb,1)
   
   !write the results in the output file coeff.dat
   open(11,file='outputs.dir/coeff.dat',status='unknown')
   do i=1,11
      write(11,*)TZ(i,1)
   enddo
   
   close(11)

end program ortho

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!ccc  Subroutine library
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine trans(A,At,m,n)

! Transpose a matrix or vector A(m,n) onto At(n,m)

   integer i,j,m,n
   double precision A(m,n),At(n,m)

   do i=1,n
      do j=1,m
         At(i,j)=A(j,i)
       enddo
   enddo
end subroutine trans

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine mmult(A,B,C,m,n,o)

!     Multiplication of 2 matrix or vectors
!     A(m,n)xB(n,o)=C(m,o)

   integer i,j,k,m,n,o
   double precision A(m,n),B(n,o),C(m,o),part
   
   do i=1,m
      do j=1,o
         C(i,j)=0
         do k=1,n
            part=A(i,k)*B(k,j)
            C(i,j)=C(i,j)+part
         enddo
      enddo
   enddo
end subroutine mmult

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

subroutine gaussj(a,n,np)

!     Linear equation solution by Gauss-Jordan elimination, equation (2.1.1) above. a(1:n,1:n)
!     is an input matrix stored in an array of physical dimensions np by np. b(1:n,1:m) is an input
!     matrix containing the m right-hand side vectors, stored in an array of physical dimensions
!     np by mp. On output, a(1:n,1:n) is replaced by its matrix inverse, and b(1:n,1:m) is
!     replaced by the corresponding set of solution vectors. 

   integer n,np,nmax
   double precision a(np,np)
   parameter (nmax=50)
!     parameter: nmax is the largest anticipated value of n.
   integer i,icol,irow,j,k,l,ll,indxc(nmax),indxr(nmax),ipiv(nmax)
!     The integer arrays ipiv, indxr, and indxc are used
!     for bookkeeping on the pivoting.
   double precision big,dum,pivinv

   do j=1,n
      ipiv(j)=0
   enddo
   do i=1,n
!     This is the main loop over the columns to be reduced.
      big=0.
      do j=1,n
!     This is the outer loop of the search for a pivot element.
         if(ipiv(j).ne.1)then
            do k=1,n
               if (ipiv(k).eq.0) then
                  if (abs(a(j,k)).ge.big)then
                     big=abs(a(j,k))
                     irow=j
                     icol=k
                  endif
               endif
            enddo
         endif
      enddo
      ipiv(icol)=ipiv(icol)+1
!     We now have the pivot element, so we interchange rows, if needed, to put the pivot
!     element on the diagonal. The columns are not physically interchanged, only relabeled:
!     indxc(i), the column of the ith pivot element, is the ith column that is reduced, while
!     indxr(i) is the row in which that pivot element was originally located. If indxr(i) =
!     indxc(i) there is an implied column interchange. With this form of bookkeeping, the
!     solution will end up in the correct order, and the inverse matrix will be scrambled
!     by columns.
      if (irow.ne.icol) then
         do l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
         enddo
      endif
      indxr(i)=irow
!     We are now ready to divide the pivot row by the pivot
!     element, located at irow and icol.
      indxc(i)=icol
      if (a(icol,icol).eq.0.) stop 'singular matrix in gaussj'
      pivinv=1./a(icol,icol)
      a(icol,icol)=1.
      do l=1,n
         a(icol,l)=a(icol,l)*pivinv
      enddo

      do ll=1,n
!     Next, we reduce the rows...
         if(ll.ne.icol)then
!     ...except for the pivot one, of course.
            dum=a(ll,icol)
            a(ll,icol)=0.
            do l=1,n
               a(ll,l)=a(ll,l)-a(icol,l)*dum
            enddo
         endif
      enddo
   enddo

!     This is the end of the main loop over columns of the reduction.
   do l=n,1,-1
!     It only remains to unscramble the solution in view
!     of the column interchanges. We do this by interchanging
!     pairs of columns in the reverse order
!     that the permutation was built up.
      if(indxr(l).ne.indxc(l))then
         do k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
         enddo
      endif
   enddo
!     And we are done.
end subroutine gaussj

