      program Q_compute

c     Modifications JLC decembre 2008      
c     Moyenne IDW des 3 (au plus) plus proches vitesses
      
      implicit none

      integer i,j,compt,k,compt2,jd,jd2,jd3
      real coeff_surface,dmax,Dxp
c      parameter (coeff_surface=0.79) ! Coefficient de surface
c      parameter (dmax=5.) !rayon de recherche des vitesses autour du point bathy
c      parameter (trunc=160.0) !distance au-dela de laquelle on extrapole les vitesses      
      
      real stage,x,y,z,hh,h_cnr,x0,y0,aa,bb,cc,xpp,pds,fr,a1,a2
      real xpb(1000),xb(1000),yb(1000),h(1000),d,dmin
      real xp(10000),yp(10000),u(10000),v(10000),ub(1000),vb(1000)
      real dist(1000),Q(1000),vv(1000),sum_Q,vmoy(1000),sum_V,ext(1000)
      real d2,ub2(1000),vb2(1000),dmin2,vv2(1000)
      real d3,ub3(1000),vb3(1000),dmin3,vv3(1000)
      real ubmoy(1000),vbmoy(1000)
      character*12 a,b,c
      integer Intensity,stop_value
      real sum_A


      open(14,file='./outputs.dir/PIV_param.dat'
     & ,status='old')
      do i=1,23
         read(14,*)
      enddo
      read(14,*)coeff_surface
      read(14,*)
      read(14,*)dmax
      read(14,*)
      read(14,*)Dxp

      open(10,file='./outputs.dir/h.dat'
     & ,status='old')
      read(10,*)h_cnr
      close(10)

      open(10,file='./outputs.dir/bathy_p.dat'
     & ,status='old')
      read(10,*,end=100) 
      read(10,*,end=100)
      read(10,*,end=100) x0,y0
      read(10,*,end=100)
      read(10,*,end=100) aa,bb
      read(10,*,end=100)
      
c   First dry point     (k=1) 
      k=1
      read(10,*,end=100)xpp,x,y,z
      hh=h_cnr-z
      xpb(k)=xpp-.000001      
      xb(k)=x       
      yb(k)=y       
      h(k)=hh 
      write(*,*)  xpb(1),xb(1),yb(1),h(1)    
      do while(hh.le.0)
        xpb(k)=xpp      
        xb(k)=x       
        yb(k)=y       
        h(k)=hh 
        read(10,*,end=100)xpp,x,y,z
        hh=h_cnr-z
      enddo      
            
c   Wetted points            
      do while (hh.ge.0)
            k=k+1
            xpb(k)=xpp
            xb(k)=x
            yb(k)=y
            h(k)=hh
            read(10,*,end=101)xpp,x,y,z
            hh=h_cnr-z         
      enddo
 101  continue
            
c   Second dry point  (k=compt)    
      k=k+1
      xpb(k)=xpp+.000001
      xb(k)=x
      yb(k)=y
      h(k)=hh      
      
 100  continue
      close(10)

      compt=k     !nombre de points de bathy
      
c   Computation of edge point coordinates

      if ((h(2)-h(1)).eq.0) then
      cc=0
      else
      cc=-h(1)/(h(2)-h(1))
      endif
      xpb(1) =  xpb(1)+cc*(xpb(2)-xpb(1))
      xb(1)  =  xb(1)+cc*(xb(2)-xb(1))
      yb(1)  =  yb(1)+cc*(yb(2)-yb(1))
      h(1)   =  0 
      
       
      if ((h(compt)-h(compt-1)).eq.0) then
      cc=0
      else
      cc=-h(compt)/(h(compt-1)-h(compt))
      endif      
      xpb(compt) =  xpb(compt)+cc*(xpb(compt-1)-xpb(compt))
      xb(compt)  =  xb(compt)+cc*(xb(compt-1)-xb(compt))
      yb(compt)  =  yb(compt)+cc*(yb(compt-1)-yb(compt))
      h(compt)   =  0  


c      write(*,*) xpb(1),xpb(compt) 
      
      open(10,file='./outputs.dir/average_vel.out'
     & ,status='old')
      do i=1,10000
         read(10,*,end=200)xp(i),yp(i),u(i),v(i)
      enddo
 200  continue
      compt2=i-1  !nombre de points de vitesse
      close(10)

      do i=2,compt-1
         dmin=dmax
         dmin2=dmax
         dmin3=dmax
         jd=0
         jd2=0         
         jd3=0
                  
         do j=1,compt2 
            d=sqrt((xp(j)-xb(i))**2 +(yp(j)-yb(i))**2)
               
c            if (d.lt.dmax) write(*,*) i,d
               
            if (d.lt.dmin3) then
               if (d.lt.dmin2) then
                  if (d.lt.dmin) then
                  dmin3=dmin2
                  jd3=jd2
                  dmin2=dmin
                  jd2=jd               
                  dmin=d
                  jd=j              
                  else
                  dmin3=dmin2
                  jd3=jd2               
                  dmin2=d
                  jd2=j                   
                  endif
               else   
               dmin3=d
               jd3=j             
               endif
            endif
         enddo
         
         if (jd.ne.0) then
         ub(i)=u(jd)
         vb(i)=v(jd)
         vv(i)=-bb*ub(i)+aa*vb(i) ! normal velocity
c         vv(i)=coeff_surface*sqrt(ub(i)**2+vb(i)**2)
         else
         vv(i)=0
         endif

         if (jd2.ne.0) then         
         ub2(i)=u(jd2)
         vb2(i)=v(jd2)
         vv2(i)=-bb*ub2(i)+aa*vb2(i) ! normal velocity
c         vv2(i)=coeff_surface*sqrt(ub2(i)**2+vb2(i)**2)
         else
         vv2(i)=0
         endif
         
         if (jd3.ne.0) then         
         ub3(i)=u(jd3)
         vb3(i)=v(jd3)
         vv3(i)=-bb*ub3(i)+aa*vb3(i) ! normal velocity
c         vv3(i)=coeff_surface*sqrt(ub3(i)**2+vb3(i)**2)
         else
         vv3(i)=0
         endif
                 
         enddo

      do i=2,compt-1
         pds=0
c mid-section integration method
      dist(i)=abs(xpb(i+1)-xpb(i-1))/2
c mean-section integration method            
c      dist(i)=abs(xpb(i+1)-xpb(i))         

          
c         dist(i)=sqrt(((xb(i+1)-xb(i-1))/2)*((xb(i+1)-xb(i-1))/2)
c     &        +((yb(i+1)-yb(i-1))/2)*((yb(i+1)-yb(i-1))/2))

     
         if (dmin.eq.0) then
            dmin=0.001
         endif 
         if (dmin2.eq.0) then
            dmin2=0.001
         endif 
         if (dmin3.eq.0) then
            dmin3=0.001
         endif         
         if (vv(i).ne.0) then
            pds=pds+1/dmin
         endif
         if (vv2(i).ne.0) then
            pds=pds+1/dmin2
         endif
         if (vv3(i).ne.0) then
            pds=pds+1/dmin3
         endif 
         
      
c      write(*,*) i, vv(i),vv2(i),vv3(i)           
         if (pds.ne.0) then
            vmoy(i)=coeff_surface*(vv(i)/dmin+vv2(i)/
     & dmin2+vv3(i)/dmin3)/pds
            ubmoy(i)=coeff_surface*(ub(i)/dmin+ub2(i)/
     & dmin2+ub3(i)/dmin3)/pds
            vbmoy(i)=coeff_surface*(vb(i)/dmin+vb2(i)/
     & dmin2+vb3(i)/dmin3)/pds
         else
            vmoy(i)=0
            ubmoy(i)=0
            vbmoy(i)=0
         endif
      enddo
      
      vmoy(1)=0
      ubmoy(1)=0
      vbmoy(1)=0

      vmoy(compt)=0
      ubmoy(compt)=0
      vbmoy(compt)=0

      h(1)=h(2)/2
      h(compt)=h(compt-1)/2
      dist(1)=abs(xpb(1)-xpb(2))/2
      dist(compt)=abs(xpb(compt-1)-xpb(compt))/2

c     truncate velocity measurements      
c      do i=1,compt
c      if (xpb(i).gt.trunc) then
c      vmoy(i)=0 
c      ubmoy(i)=0
c      vbmoy(i)=0     
c      endif
c      enddo
      
      
            
c     interpolation and extrapolation (constant Froude assumption)      
      do i=1,compt
         ext(i)=0   
         if (vmoy(i).eq.0) then
            k=i+1
            do while((k.le.compt).and.(vmoy(k).eq.0))
            k=k+1
            enddo
            j=i-1
            do while((j.ge.1).and.(vmoy(j).eq.0))
            j=j-1
            enddo         
            if (k.le.compt) then
                if (j.ge.1) then
                a1=1/abs(xpb(k)-xpb(i))
                a2=1/abs(xpb(j)-xpb(i))
                fr=(a1*vmoy(k)/sqrt(h(k))+a2*vmoy(j)/sqrt(h(j)))/(a1+a2)
                else
                fr=vmoy(k)/sqrt(h(k))
                endif
            else
                if (j.ge.1) then
                fr=vmoy(j)/sqrt(h(j))
                else
                fr=0
                endif                        
            endif
 
         vmoy(i)=sqrt(h(i))*fr
         ubmoy(i)=-vmoy(i)*bb
         vbmoy(i)=vmoy(i)*aa
         ext(i)=1

         endif
c        aux bords: moyenne entre vitesse a mi-chemin et vitesse nulle au bord 
         if ((i.eq.1).or.(i.eq.compt)) then
             vmoy(i)=vmoy(i)/2
         endif
         Q(i)=dist(i)*h(i)*vmoy(i)
      enddo
         

cc   Edge discharge extrapolation    (Mid-section!!!)
c      vmoy(1)=vmoy(2)*0.67
c      Q(1)=h(2)/2*vmoy(1)*abs(xpb(2)-xpb(1))/2
c      vmoy(compt)=vmoy(compt-1)*0.67
c      Q(compt)=h(compt-1)/2*vmoy(compt)*abs(xpb(compt)-xpb(compt-1))/2

      
      
c   Discharge sum      
      sum_Q=0
      sum_A=0
      do i=1,compt !5,24       
c        write(*,*) xpb(i),h(i),vmoy(i)
         sum_Q=sum_Q+Q(i)
         sum_A=sum_A+h(i)*dist(i)
      enddo


      write(*,*)'#######################################'
      write(*,400)sum_Q
 400  format(' ### Total Discharge:',F10.3,' m3/s ###') 
      write(*,401)sum_A
 401  format(' ### Wetted Area:      ',F10.3,' m2 ###') 
      write(*,402) sum_Q/sum_A
 402  format(' ### Mean Velocity:   ',F10.3,' m/s ###') 
      write(*,403) h_cnr
 403  format(' ### Water stage:       ',F10.3,' m ###') 
      write(*,*)'#######################################'    
      
c      read(*,*)date     
c      date=''
      
cc    sauvegarde des vitesses interpolees pour le calcul du debit  
      open(10,file='./outputs.dir/Discharge.dat'
     & ,status='unknown')

      write(10,1001) h_cnr,sum_Q,sum_A,sum_Q/sum_A
1001  format(4F10.3)     
     
      do i=1,compt      
        write(10,1100) i,xpb(i),xb(i),yb(i),h(i),
     & ubmoy(i),vbmoy(i),vmoy(i),ext(i)
 1100 format(I3,8F10.3)
       enddo 
      close(10)




cc    sauvegarde des debits calcules
      open(10,file='./outputs.dir/Discharges.dat'
     & ,status='unknown')
      do i=1,99999
         read(10,*,end=300)
      enddo
 300  continue
      write(10,1000)'hQAU',h_cnr,sum_Q,sum_A,sum_Q/sum_A
1000  format(A12,4F10.3)
      close(10)

      end
