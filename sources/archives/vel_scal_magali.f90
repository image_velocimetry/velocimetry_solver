! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

program vel_scal

!     calcule les scalaires associ�s au champ de vitesse
!     M Jodeau le 13-07-2012
!     modif par JLC: format '(A)' au lieu de * pour les strings 
!     modif par JLC: ajout du calcul du fichier average_scal.out
!     modif par JBF: passage en format libre et double pr�cision
!     modif par MJ 11-02-2015 : 1. traite les fichiers de vitesse filtr�s (et non plus
!                               les vitesses brutes)
!                               2. calcule la norme des vitesses moy.

   implicit none
   
   integer, parameter :: pr = 8 !choose pr=4 for single precision

   integer :: i,itot,io_status
   real(kind=pr) :: x,y,u,v
   real(kind=pr) :: r,n,omega,div
   real(kind=pr) :: sum,nmoy(10000),rmoy(10000)
   real(kind=pr) :: omegamoy(10000),divmoy(10000)
   real(kind=pr) :: xmoy(10000),ymoy(10000)
   real(kind=pr) :: sum_u(10000),sum_v(10000)
   real(kind=pr) :: mean_u(10000),mean_v(10000),moy
   real(kind=pr) :: uu(10000,5000),vv(10000,5000)
   real(kind=pr) :: xx(10000,5000),yy(10000,5000)
   integer :: j,compteur,compt,nb(10000),nn, ios
   character(len=160) :: BaseName,NomIn,NomOut
   character(len=60) :: NomFich
   character(len=20) :: BaseName2(1000)

! ecriture des fichiers vel_scal_piv00x    
   open(13,file='./outputs.dir/list_avg.dat',status='old')
   sum = 0
   
   do
      read(13,*,iostat=io_status) BaseName
      if (io_status > 0) exit
      sum = sum+1
      !NomIn = './vel_real/real_'//trim(BaseName)
      NomIn = './vel_filter/filter_'//trim(BaseName)
      
      open(10,file=trim(NomIn),status='old') ! ouverture du fichier de vitesse

      ! ecriture du fichier de scalaires
      NomOut = './vel_scal/scal_'//trim(BaseName)
      open(11,file=trim(NomOut),status='unknown')
      itot = 0
      do
         read(10,*,iostat=io_status) x,y,u,v,r
         if (io_status > 0) exit

         n = sqrt(u*u+v*v)
                
         omega = 4._pr             ! calcul du rotationnel (a faire)
         div = 5._pr               ! calcul de la divergence (a faire)   
                          
         write(11,'(f8.4,f8.4,f8.4,f8.4,f8.4,f8.4)') x,y,n,r,omega,div 
         
         itot = itot+1
         !nmoy(itot) = nmoy(itot)+n
         rmoy(itot) = rmoy(itot)+r
         omegamoy(itot) = omegamoy(itot)+omega
         divmoy(itot) = divmoy(itot)+div            
         xmoy(itot) = x
         ymoy(itot) = y
      enddo
      close(10)
      close(11)
   enddo

   close(13)
   
! calcul des moyennes en coherence avec moy_ec.f90
   open(13,file='./outputs.dir/list_avg.dat',status='old')
   do i=1,10000
      read(13,*,iostat=ios) BaseName2(i)
      if (ios /= 0) exit
   enddo
   close (13)
   compteur=i-1 ! Number of velocity fields to average

   do i=1,compteur
      NomFich = 'vel_filter/filter_'//trim(BaseName2(i))
!      write(*,*) NomFich
      call readfile(NomFich,i,compt,xx,yy,uu,vv)
   enddo

   nn=0
   moy=0
   do i=1,compt ! Number of data points in each velocity field
      sum_u(i)=0
      sum_v(i)=0
      nb(i)=0
      do j=1,compteur ! number of files 
         if ((uu(i,j).ne.0._pr).or.(vv(i,j).ne.0._pr)) then
            nb(i)=nb(i)+1
         endif
         sum_u(i)=sum_u(i)+uu(i,j)
         sum_v(i)=sum_v(i)+vv(i,j)
      enddo

    !   write(*,*) nb(i)
      if (nb(i).ne.0) then
         mean_u(i)=sum_u(i)/nb(i)
         mean_v(i)=sum_v(i)/nb(i)
      else
         mean_u(i)=0._pr
         mean_v(i)=0._pr
      endif
      nmoy(i)=sqrt(mean_u(i)*mean_u(i)+mean_v(i)*mean_v(i))
    enddo  
! ----------------------------------------------------------------      
   
! ecriture du fichier des parametres moyens  
   open(14,file='./outputs.dir/average_scal.out',status='unknown')
   do i = 1,itot
      !nmoy(i) = nmoy(i)/sum
      rmoy(i) = rmoy(i)/sum
      omegamoy(i) = omegamoy(i)/sum
      divmoy(i) = divmoy(i)/sum
      write(14,'(f8.4,f8.4,f8.4,f8.4,f8.4,f8.4)') xmoy(i),ymoy(i),nmoy(i),rmoy(i),omegamoy(i),divmoy(i)
      
   enddo
   close(14)
   
   contains
   
   subroutine readfile(NomFich,k,compt,x,y,u,v)

      integer i,k,compt, ios
      character*60 NomFich
      real(kind=pr) :: x(10000,5000),y(10000,5000)
      real(kind=pr) :: u(10000,5000),v(10000,5000)
      
      open(15,file=NomFich,status='old')
      do i=1,1000000
         read(15,*,iostat=ios) x(i,k), y(i,k), u(i,k), v(i,k)
         if (ios /= 0) exit
      enddo
      compt=i-1
      close(15)
   end
   
 end program vel_scal  
 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


