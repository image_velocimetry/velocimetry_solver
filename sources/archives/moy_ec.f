      program moy_ec

      implicit none

      integer i,j,compteur,compt,nb(10000),nn
      character*20 BaseName(10000)
      character*60 NomFich
      real sum_u(10000),sum_v(10000)
      real mean_u(10000),mean_v(10000),moy
      real u(10000,5000),v(10000,5000)
      real x(10000,5000),y(10000,5000)
      real sum_dif_u(10000),sum_dif_v(10000)
      real dif_u(10000),dif_v(10000)

      open(12,file='outputs.dir/average_vel.out',status='unknown')
      open(10,file='outputs.dir/list_avg.dat',status='old')

      do i=1,10000
         read(10,*,end=100)BaseName(i)
      enddo
 100  continue
      compteur=i-1 ! Number of velocity fields to average

      do i=1,compteur
         write(NomFich,1000)BaseName(i)      
 1000    format('vel_filter/filter_',A20)  
c         write(*,*) NomFich    
         call readfile(NomFich,i,compt,x,y,u,v)
      enddo 

      nn=0
      moy=0
      do i=1,compt ! Number of data points in each velocity field
         sum_u(i)=0
         sum_v(i)=0
         nb(i)=0
         do j=1,compteur
            if ((u(i,j).ne.0).or.(v(i,j).ne.0)) then
               nb(i)=nb(i)+1
            endif
            sum_u(i)=sum_u(i)+u(i,j)
            sum_v(i)=sum_v(i)+v(i,j)            
         enddo

c       write(*,*) nb(i)
         if (nb(i).ne.0) then
            mean_u(i)=sum_u(i)/nb(i)
            mean_v(i)=sum_v(i)/nb(i)
         else
            mean_u(i)=0
            mean_v(i)=0
         endif

c         sum_dif_u(i)=0
c         sum_dif_v(i)=0
c         do j=1,compteur
c            if ((u(i,j).ne.0).and.(v(i,j).ne.0)) then
c               dif_u(i)=(u(i,j)-mean_u(i))**2
c               dif_v(i)=(v(i,j)-mean_v(i))**2
c            
c               sum_dif_u(i)=sum_dif_u(i)+dif_u(i)
c               sum_dif_v(i)=sum_dif_v(i)+dif_v(i)     
c            endif
c         enddo
         
         write(12,8500)x(i,1),y(i,1),mean_u(i),mean_v(i)
 8500    format(4F10.4)
      enddo
      close(10)
      close(12)
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccc   subroutines

      subroutine readfile(NomFich,k,compt,x,y,u,v)

      integer i,k,compt
      character*60 NomFich
      real x(10000,5000),y(10000,5000)
      real u(10000,5000),v(10000,5000)

      open(11,file=NomFich,status='old')
      do i=1,1000000
         read(11,*,end=200)x(i,k),y(i,k),u(i,k),v(i,k)
      enddo
 200  continue
      compt=i-1
      close(11)
      end
