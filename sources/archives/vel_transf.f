! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

      program vel_transf

c     cr�� � partir de filter.f, transforme les vecteurs vitesses de l'espace image transform�e � l'espace r�el
c     M Jodeau le 14-11-2011

      implicit none

      integer i,k,kk
      real x,y,u,v,xmin,ymin,xmax,ymax,resol
      real r,size,smin,smax,Dt,vmin,vmax
c      parameter (Dt=0.2)   !intervalle de temps [s] entre deux images
c      parameter (smin=0.1,smax=10.)   !seuils limites de norme de vitesse
c      parameter (vmin=-5.,vmax=5.)   !seuils limites de vitesse v transversale
      character*60 station
      character*160 BaseName,NomIn,NomOut
  

      open(14,file='./outputs.dir/PIV_param.dat',status='old')
      do i=1,8
         read(14,*)
      enddo
      read(14,*)Dt
      do i=1,8
         read(14,*)
      enddo

      open(15,file='./outputs.dir/img_ref.dat',status='old')
      read(15,*)
      read(15,*)xmin,ymin
      read(15,*)
      read(15,*)xmax,ymax
      read(15,*)
      read(15,*)resol

      open(13,file='./outputs.dir/list_avg.dat',status='old')
      do k=1,10000
         read(13,*,end=100)BaseName
         write(NomIn,1000) BaseName
 1000    format('./vel_raw/',A40) 

c      write(*,*)NomIn


         open(10,file=NomIn,status='old')

         write(NomOut,2000) BaseName
 2000    format('./vel_real/real_',A40) 
         open(11,file=NomOut,status='unknown')

         do i=1,100000
            read(10,*,end=200)x,y,u,v,r
            y=y*resol+xmin
            x=x*resol+ymin                        
            u=(u*resol)/Dt
            v=(v*resol)/Dt           
            size=sqrt(u*u+v*v)
                     
            write(11,*)y,x,v,u,r         
         enddo


 200     continue
         close(10)
         close(11)
      enddo
 100  continue
      close(13)
      end
