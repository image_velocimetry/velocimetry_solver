      program filter

      implicit none

      integer i,k,kk
      real x,y,u,v,xmin,ymin,xmax,ymax,resol
      real r,size,smin,smax,Dt,vmin,vmax
      parameter (Dt=0.2)   !intervalle de temps [s] entre deux images
      parameter (smin=0.1,smax=10.)   !seuils limites de norme de vitesse
      parameter (vmin=-5.,vmax=5.)   !seuils limites de vitesse v transversale
      character*20 BaseName
      character*100 NomIn,NomOut
    
      
      open(15,file='outputs.dir/img_ref.dat',status='old')
      read(15,*)
      read(15,*)xmin,ymin
      read(15,*)
      read(15,*)xmax,ymax
      read(15,*)
      read(15,*)resol

      open(13,file='outputs.dir/list_avg.dat',status='old')
      do k=1,10000
         read(13,*,end=100)BaseName
         write(NomIn,1000)BaseName
 1000    format('vel_raw/',A20) 
         open(10,file=NomIn,status='old')

         write(NomOut,2000)BaseName
 2000    format('vel_filter/filter_',A20) 
         open(11,file=NomOut,status='unknown')

         do i=1,100000
            read(10,*,end=200)x,y,u,v,r
            y=y*resol+xmin
            x=x*resol+ymin                        
            u=(u*resol)/Dt
            v=(v*resol)/Dt           
            size=sqrt(u*u+v*v)
            
            
            if ((size.ge.smax).or.(size.le.smin)) then
c             write(*,*) 'size',size
               u=0
               v=0
            endif
            if ((v.ge.vmax).or.(v.lt.vmin)) then
c            write(*,*) 'v',v
               u=0
               v=0
            endif

                     
            write(11,*)y,x,v,u,r         
         enddo
 200     continue
         close(10)
         close(11)
      enddo
 100  continue
      close(13)
      end
