      program transf_img

c     From a raw pgm image, compute the ortho-rectified image using
c     the parameters of coeff.dat.

      implicit none

      integer i,j,k,compteur
      integer ni,nj
c      parameter (ni=576,nj=720) !taille des images de base
      integer iinf,isup,jinf,jsup
c      parameter(iinf=1,isup=575,jinf=1,jsup=719) !zone a traiter (images transf)
      real a(11),xmin,ymin,xmax,ymax,r,h
      integer gg(10000,10000),maxx,maxy
      integer,dimension(:,:),allocatable :: img


c      real x,y,ii,jj,diff,D1,D2,D3
      real x,y,ii,jj,diff,D3
      character*60 BaseName(2000)
      character*60 NomFich,NomOut
      character*60 toto,station
      integer i2,j2,i2max,j2max

c      read(*,*) station
c      write(*,*) 'config/'//trim(station)//'/coeff_plan.dat'

c     Read the parameters a(8) of coeff.dat
c      open(10,file='config/'//trim(station)//'/coeff_plan.dat', status='old')
      open(10,file='./outputs.dir/coeff.dat', status='old')
      do i=1,4
         read(10,*)a(i)
      enddo
      do i=1,3
         read(10,*)a(i+8)
      enddo
      do i=1,4
         read(10,*)a(i+4)
      enddo   
      close(10)

c      open(13,file='config/'//trim(station)//'/h.dat', status='old')
      open(13,file='./outputs.dir/h.dat', status='old')
      read(13,*)h
      close(13)

c     Definition of the river plan equation: Z=D1X+D2Y+D3
c      D1=0
c      D2=0
      D3=h
 

c     Read the image information file img_ref.dat
c      open(14,file='config/'//trim(station)//'/img_ref.dat', status='old')
      open(14,file='./outputs.dir/img_ref.dat', status='old')
      read(14,*)
      read(14,*)xmin,ymin
      read(14,*)
      read(14,*)xmax,ymax
      read(14,*)
      read(14,*)r 
      read(14,*)
      read(14,*)ni,nj    
      close(14)
      iinf=2
      isup=ni-1
      jinf=2
      jsup=nj-1   
      allocate(img(ni,nj))
      
c     Read the raw image
      open(11,file='img_pgm/image1.pgm',
     & status='old')
      read(11,*)
      read(11,*)
      read(11,*)
      do i=1,ni
         read(11,*)(img(i,j),j=1,nj)
      enddo
      close(11)
  


c     Initialize the output matrix gg(i,j)
cc modification par JLC le 30/08/2008         
         i2max=int((xmax-xmin)/r)+1
         j2max=int((ymax-ymin)/r)+1
         
         do i=1,i2max
            do j=1,j2max
               gg(i,j)=0    
            enddo
         enddo

c     Compute the ortho-image                
         do i=1,i2max
            ii=xmin + i*r
            do j=1,j2max
            jj=ymin + j*r               
c     Compute the image coordinates [x,y] from space coordinates [ii,jj]
c     using the parameters a(11)
c               call space2CRT(x,y,a,ii,jj,D1,D2,D3)
               call space2CRT(x,y,a,ii,jj,D3)
c     Only compute the area of interest defined by iinf,isup,jinf,jsup
               if (((x.ge.jinf).and.(x.le.jsup)).and.
     &              ((y.ge.iinf).and.(y.le.isup))) then
c     Compute the intensity of the pixel using cubic convolution
                  call cub_conv(gg,i,j,x,y,ni,nj,img)
               endif
            enddo
         enddo
cc  fin modif JLC




c     Write the data in the output file NomOut
         maxx=i-1
         maxy=j-1
         
         write(toto,'(A,I5.5,A)') '(',maxx,'I5)'
             
         open(12,file='img_transf/image1_transf.pgm',
     &   status='unknown',form='formatted') 
         write(12,1000)
         write(12,1100)maxx,maxy
         write(12,1200)         
 1000    format('P2')
 1100    format(2I8)
 1200    format('255')              
         do i=1,maxy
            write(12,toto)(gg(j,i),j=1,maxx)
c            write(12,*)(gg(j,i),j=1,maxx)
         enddo
         close(12)
            
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccc Subroutine library
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c      subroutine space2CRT(xp,yp,a,ii,jj,D1,D2,D3)
      subroutine space2CRT(xp,yp,a,ii,jj,D3)
      
c     Compute image coordinates from space coordinates using a(11)
c           real a(11),xp,yp,ii,jj,D1,D2,D3
      real a(11),xp,yp,ii,jj,D3

      xp=(a(1)*ii+a(2)*jj+(a(3)*D3+a(4)))
     &     /(a(9)*ii+a(10)*jj+(a(11)*D3+1))
      yp=(a(5)*ii+a(6)*jj+(a(7)*D3+a(8)))
     &     /(a(9)*ii+a(10)*jj+(a(11)*D3+1))

c      xp=((a(1)+a(3)*D1)*ii+(a(2)+a(3)*D2)*jj+(a(3)*D3+a(4)))
c     &     /((a(9)+a(11)*D1)*ii+(a(10)+a(11)*D2)*jj+(a(11)*D3+1))
c      yp=((a(5)+a(7)*D1)*ii+(a(6)+a(7)*D2)*jj+(a(7)*D3+a(8)))
c     &     /((a(9)+a(11)*D1)*ii+(a(10)+a(11)*D2)*jj+(a(11)*D3+1))

      end

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cub_conv(gg,i,j,xp,yp,ni,nj,img)

c     Compute the intensity of a pixel using cubic convolution

      integer ni,nj,nxp,nyp,i,j,gg(10000,10000),img(ni,nj)
      real xp,yp,sx,sy,Cx,Cy,fx,sum_fx

c      write(*,*) xp,yp

      nxp=int(xp)
      nyp=int(yp)

      sum_fx=0
      do k=1,4
         do l=1,4
            sx=(nxp+k-2)-xp
            sy=(nyp+l-2)-yp
            if ((abs(sx).ge.0).and.(abs(sx).lt.1)) then
               Cx=1-2*(abs(sx)*abs(sx))+(abs(sx)*abs(sx)*abs(sx))
            elseif ((abs(sx).ge.1).and.(abs(sx).lt.2)) then
               Cx=4-8*abs(sx)+5*(abs(sx)*abs(sx))
     $              -(abs(sx)*abs(sx)*abs(sx))
            elseif (abs(sx).gt.2) then
               Cx=0
            endif

            if ((abs(sy).ge.0).and.(abs(sy).lt.1)) then
               Cy=1-2*(abs(sy)*abs(sy))+(abs(sy)*abs(sy)*abs(sy))
            elseif ((abs(sy).ge.1).and.(abs(sy).lt.2)) then
               Cy=4-8*abs(sy)+5*(abs(sy)*abs(sy))
     &              -(abs(sy)*abs(sy)*abs(sy))
            elseif (abs(sy).gt.2) then
               Cy=0
            endif

cc   toto            
c        if ((nyp+l-2.gt.600).or.(nxp+k-2.gt.800)) then
c        write(*,*) nyp+l-2,nxp+k-2
cc        pause
c        endif        
c        if ((nyp+l-2.lt.0).or.(nxp+k-2.lt.0)) then
c        write(*,*) nyp+l-2,nxp+k-2
cc        pause
c        endif     
            
            fx=img(nyp+l-2,nxp+k-2)*Cx*Cy
            sum_fx=sum_fx+fx
         enddo
      enddo
      gg(i,j)=sum_fx
      if (gg(i,j).gt.255) gg(i,j)=255
      if (gg(i,j).lt.0) gg(i,j)=0   
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccc   END
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
