      program bathy_compute

      implicit none
      integer i,j,k,m,compt,nxp
      real x(1000),y(1000),z(1000),a,b,n
      real temp
      real xm(1000),ym(1000),zm(1000)
      real xp(1000),xpm(1000),Dxp,intxp
      real xpa,xa,ya,za
      character*180 ligne

      open(14,file='./outputs.dir/PIV_param.dat',status='old')
      do i=1,27
         read(14,*)
      enddo
      read(14,*)Dxp
      close(14)

      open(10,file='./outputs.dir/bathy.dat',status='old')
      k=1
      do i=1,1000
      read(10,*,end=100) x(k),y(k),z(k)
      k=k+1
      enddo

 100  continue
      close(10)

      compt=k-1    !nombre de points de bathy



      open(10,file='./outputs.dir/bathy_p.dat',status='unknown')
      write(10,*) ' projected bathymetry profile'
      write(10,*) ' x0  y0 (start point coordinates)'
      write(10,*) x(1),y(1)
      write(10,*) 'a   b (unit vector coordinates)'

      n=sqrt((x(compt)-x(1))**2+(y(compt)-y(1))**2)
      a=(x(compt)-x(1))/n
      b=(y(compt)-y(1))/n
      write(10,*) a,b
      write(10,*) 'xp  x   y   zb (position, bed elevation)'


c bathymetry projection
      do k=1,compt
         temp=x(k)
         x(k)=a*a*x(k)+a*b*y(k)+b*b*x(1)-a*b*y(1)
         y(k)=a*b*temp+b*b*y(k)-a*b*x(1)+a*a*y(1)
         xp(k)=sqrt((x(k)-x(1))**2+(y(k)-y(1))**2)
      enddo

c sort bathy points
      do k=2,compt
        xpa=xp(k)
        xa=x(k)
        ya=y(k)
        za=z(k)
      do j=k-1,1,-1
         if (xp(j).le.xpa) goto 10
         xp(j+1)=xp(j)
         x(j+1)=x(j)
         y(j+1)=y(j)
         z(j+1)=z(j)
      enddo
      j=0
10    xp(j+1)=xpa
      x(j+1)=xa
      y(j+1)=ya
      z(j+1)=za
      enddo

      do k=1,compt
c      write(*,*) xp(k),x(k),y(k),z(k)
      enddo

      m=1
      xpm(m)=0
      xm(m)=x(m)
      ym(m)=y(m)
      zm(m)=z(m)


      do k=2,compt

c        bathymetry interpolation
         if ((xp(k)-xp(k-1)).gt.Dxp) then

         nxp=floor((xp(k)-xp(k-1))/Dxp)

         intxp=(xp(k)-xp(k-1))/(nxp+1)
         do j=1,nxp
         m=m+1
         xpm(m)=xp(k-1)+j*intxp
         xm(m)=x(k-1)+a*j*intxp
         ym(m)=y(k-1)+b*j*intxp
         zm(m)=z(k-1)+(z(k)-z(k-1))*j/nxp

         enddo

         endif

         m=m+1
         xpm(m)=xp(k)
         xm(m)=x(k)
         ym(m)=y(k)
         zm(m)=z(k)

      enddo




c write file bathy_p
      do k=1,m
         write(10,80) xpm(k),xm(k),ym(k),zm(k)
80    format (4F20.3)
      enddo





      close(10)



      end
