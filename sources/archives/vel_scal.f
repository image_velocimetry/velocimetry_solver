! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

      program vel_scal

c     calcule les scalaires associ�s au champ de vitesse
c     M Jodeau le 13-07-2012
c     modif par JLC: format '(A)' au lieu de * pour les strings 
c     modif par JLC: ajout du calcul du fichier average_scal.out

      implicit none

      integer i,k,itot
      real x,y,u,v
      real r,n,omega,div
      real sum,nmoy(10000),rmoy(10000)
      real omegamoy(10000),divmoy(10000)
      real xmoy(10000),ymoy(10000)
      character*60 station
      character*160 BaseName,NomIn,NomOut
  
      open(13,file='./outputs.dir/list_avg.dat',status='old')
      sum=0
      
      do k=1,10000
         read(13,*,end=100)BaseName
         sum=sum+1
         write(NomIn,'(A)')trim('./vel_real/real_'//trim(BaseName))
         
c 1000    format(A160) 


         open(10,file=NomIn,status='old') ! ouverture du fichier de vitesse

c ecriture du fichier de scalaires
         write(NomOut,'(A)')trim('./vel_scal/scal_'//trim(BaseName))
c 2000    format(A160) 
         open(11,file=NomOut,status='unknown')
         itot=0
         do i=1,100000
            read(10,*,end=200)x,y,u,v,r

			n=sqrt(u*u+v*v)
			omega=4             ! calcul du rotationnel
			div=5               ! calcul de la divergence   
                             
            write(11,'(f8.4,f8.4,f8.4,f8.4,f8.4,f8.4)')x,y,n,r,omega,div 
            
            itot=itot+1
            nmoy(i)=nmoy(i)+n
            rmoy(i)=rmoy(i)+r
            omegamoy(i)=omegamoy(i)+omega
            divmoy(i)=divmoy(i)+div            
            xmoy(i)=x
            ymoy(i)=y
                   
         enddo
 200     continue
         close(10)
         close(11)
      enddo
 100  continue

      close(13)

c ecriture du fichier des parametres moyens  
      open(14,file='./outputs.dir/average_scal.out',status='unknown')    
      do i=1,itot
            nmoy(i)=nmoy(i)/sum
            rmoy(i)=rmoy(i)/sum
            omegamoy(i)=omegamoy(i)/sum
            divmoy(i)=divmoy(i)/sum
            write(14,'(f8.4,f8.4,f8.4,f8.4,f8.4,f8.4)')xmoy(i),
     &      ymoy(i),nmoy(i),rmoy(i),omegamoy(i),divmoy(i)                           
      enddo

      end
