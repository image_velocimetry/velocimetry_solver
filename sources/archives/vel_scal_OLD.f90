! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

program vel_scal

!     calcule les scalaires associ�s au champ de vitesse
!     M Jodeau le 13-07-2012
!     modif par JLC: format '(A)' au lieu de * pour les strings 
!     modif par JLC: ajout du calcul du fichier average_scal.out
!     modif par JBF: passage en format libre et double pr�cision

   implicit none
   
   integer, parameter :: pr = 8 !choose pr=4 for single precision

   integer :: i,itot,io_status
   real(kind=pr) :: x,y,u,v
   real(kind=pr) :: r,n,omega,div
   real(kind=pr) :: sum,nmoy(10000),rmoy(10000)
   real(kind=pr) :: omegamoy(10000),divmoy(10000)
   real(kind=pr) :: xmoy(10000),ymoy(10000)
   character(len=160) :: BaseName,NomIn,NomOut

   open(13,file='./outputs.dir/list_avg.dat',status='old')
   sum = 0
   
   do
      read(13,*,iostat=io_status) BaseName
      if (io_status > 0) exit
      sum = sum+1
      NomIn = './vel_real/real_'//trim(BaseName)
      
      open(10,file=trim(NomIn),status='old') ! ouverture du fichier de vitesse

      ! ecriture du fichier de scalaires
      NomOut = './vel_scal/scal_'//trim(BaseName)
      open(11,file=trim(NomOut),status='unknown')
      itot = 0
      do
         read(10,*,iostat=io_status) x,y,u,v,r
         if (io_status > 0) exit

         n = sqrt(u*u+v*v)
         omega = 4._pr             ! calcul du rotationnel (� faire)
         div = 5._pr               ! calcul de la divergence (� faire)   
                          
         write(11,'(f8.4,f8.4,f8.4,f8.4,f8.4,f8.4)') x,y,n,r,omega,div 
         
         itot = itot+1
         nmoy(itot) = nmoy(itot)+n
         rmoy(itot) = rmoy(itot)+r
         omegamoy(itot) = omegamoy(itot)+omega
         divmoy(itot) = divmoy(itot)+div            
         xmoy(itot) = x
         ymoy(itot) = y
      enddo
      close(10)
      close(11)
   enddo

   close(13)

! ecriture du fichier des parametres moyens  
   open(14,file='./outputs.dir/average_scal.out',status='unknown')
   do i = 1,itot
      nmoy(i) = nmoy(i)/sum
      rmoy(i) = rmoy(i)/sum
      omegamoy(i) = omegamoy(i)/sum
      divmoy(i) = divmoy(i)/sum
      write(14,'(f8.4,f8.4,f8.4,f8.4,f8.4,f8.4)') xmoy(i),ymoy(i),nmoy(i),rmoy(i),omegamoy(i),divmoy(i)                           
   enddo
   close(14)

end program vel_scal
