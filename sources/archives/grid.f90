! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
! 
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of 
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works 
! on LSPIV by Ichiro Fujita and Marian Muste. 
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

program mk_grid

!     Create the computational grid for PIV analysis.

   implicit none

   integer i,j
   integer xp1,xp2,xp3,xp4,yp1,yp2,yp3,yp4,Nb1,Nb2
   real a1(100),b1(100),a2(100),b2(100),x(10,100),y(100,100)
   real x1,y1,x2,y2
#ifdef version_station
   character (len=160) :: param1, param2
#endif /* version_station */

!     Read the grid data file grid_param.dat
   open(10,file='inputs.dir/grid_param.dat',status='old')
   read(10,*)
   read(10,*)xp1,yp1
   read(10,*)xp2,yp2
   read(10,*)xp3,yp3
   read(10,*)xp4,yp4
   read(10,*)
   read(10,*)Nb1
   read(10,*)
   read(10,*)Nb2
   close(10)

!     Compute the equations of the segments
   do i=1,Nb1
      x1=xp1+((i-1)*(xp2-xp1)/(Nb1-1))
      y1=yp1+((i-1)*(yp2-yp1)/(Nb1-1))
      x2=xp4+((i-1)*(xp3-xp4)/(Nb1-1))
      y2=yp4+((i-1)*(yp3-yp4)/(Nb1-1))
      call equ(x1,y1,x2,y2,a1(i),b1(i))
   enddo
   do i=1,Nb2
      x1=xp2+((i-1)*(xp3-xp2)/(Nb2-1))
      y1=yp2+((i-1)*(yp3-yp2)/(Nb2-1))
      x2=xp1+((i-1)*(xp4-xp1)/(Nb2-1))
      y2=yp1+((i-1)*(yp4-yp1)/(Nb2-1))
      call equ(x1,y1,x2,y2,a2(i),b2(i))
   enddo

!     Compute the intersection of the segments and write the nodes
!     location on grid.dat
   open(15,file='outputs.dir/grid.dat',status='unknown')
   do i=1,Nb1
      do j=1,Nb2
         x(i,j)=(b2(j)-b1(i))/(a1(i)-a2(j))
         y(i,j)=a1(i)*x(i,j)+b1(i)
         write(15,*)nint(x(i,j)),nint(y(i,j))
      enddo
   enddo
end


subroutine equ(x1,y1,x2,y2,a,b)
   real x1,x2,y1,y2
   real a,b

   if (y1.eq.y2) then
      a=0
      b=y1
   else if (x1.eq.x2) then
      x2=x1+0.1
      a=(y1-y2)/(x1-x2)
      b=(y1)-(a*(x1))
   else
      a=(y1-y2)/(x1-x2)
      b=(y1)-(a*(x1))
   endif
end

