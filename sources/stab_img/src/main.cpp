#include <iostream>
#include <ctime>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <stdio.h>

#include "../include/ImageNdg.h"
#include "../include/ImageDouble.h"
#include "../include/keypoint.h"
#include "../include/image.h"
#include "../include/lib.h"
#include "../include/match_surf.h"
#include "../include/lib_match_surf.h"
#include "libNumerics/homography.h"
#include "libNumerics/numerics.h"
#include "libNumerics/matrix.cpp"

#define MAGIC_NUMBER_BMP ('B'+('M'<<8)) // Bitmap signature windows
#define PI 3.14159265358979323846


// IMAGE REGISTRATION -----------------------------------------
//        Cancel the movement from one frame to another
//            SURF algorithm is used to detect keypoints on frames
//            ORSA algorithm is used to clean matches

#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;

float CubicHermite(float A, float B, float C, float D, float t)
{
    float a = -A / 2.0f + (3.0f*B) / 2.0f - (3.0f*C) / 2.0f + D / 2.0f;
    float b = A - (5.0f*B) / 2.0f + 2.0f*C - D / 2.0f;
    float c = -A / 2.0f + C / 2.0f;
    float d = B;

    return a * t*t*t + b * t*t + c * t + d;
}


int InterpolateBicubic(CImageNdg* imgsrc, double* _i, double* _j)
{
    // x refers to i coordinate
    float x = (float)*_j;
    int xint = int(x); // use to get 4 control points from data
    float xfract = x - floor(x); // xfract is between 0-1 --> t in Hermitte Function
                                 // y refers to j coordinate
    float y = (float)*_i;
    int yint = int(y); // use to get 4 control points from data
    float yfract = y - floor(y);// yfract is between 0-1 --> t in Hermitte Function

                                // row -1
    double p00 = imgsrc->operator()(yint - 1, xint - 1);
    double p10 = imgsrc->operator()(yint - 1, xint + 0);
    double p20 = imgsrc->operator()(yint - 1, xint + 1);
    double p30 = imgsrc->operator()(yint - 1, xint + 2);

    // row 0
    double p01 = imgsrc->operator()(yint + 0, xint - 1);
    double p11 = imgsrc->operator()(yint + 0, xint + 0);
    double p21 = imgsrc->operator()(yint + 0, xint + 1);
    double p31 = imgsrc->operator()(yint + 0, xint + 2);

    // row +1
    double p02 = imgsrc->operator()(yint + 1, xint - 1);
    double p12 = imgsrc->operator()(yint + 1, xint + 0);
    double p22 = imgsrc->operator()(yint + 1, xint + 1);
    double p32 = imgsrc->operator()(yint + 1, xint + 2);

    // row +2
    double p03 = imgsrc->operator()(yint + 2, xint - 1);
    double p13 = imgsrc->operator()(yint + 2, xint + 0);
    double p23 = imgsrc->operator()(yint + 2, xint + 1);
    double p33 = imgsrc->operator()(yint + 2, xint + 2);

    // interpolate bi-cubically

    float col0 = CubicHermite(p00, p10, p20, p30, xfract);
    float col1 = CubicHermite(p01, p11, p21, p31, xfract);
    float col2 = CubicHermite(p02, p12, p22, p32, xfract);
    float col3 = CubicHermite(p03, p13, p23, p33, xfract);

    // Clamp the values since the curve can put the value below 0 or above 255
    int res = round(CubicHermite(col0, col1, col2, col3, yfract));

    //int res = CubicHermite(p01, p11, p21, p31, xfract);

    if (res>255)
        return 255;
    if (res < 0)
        return 0;

    return res;
}


int main(int argc, char* argv[]) {

#ifdef WITH_TIMERS
    time_t start_tmp;
    time_t end_tmp;
    time_t start_tmp2;
    time_t end_tmp2;
    time_t start;
    time_t end;
    time_t time_init;
    time(&start);
    time(&start_tmp);
    double seconds;
    double time_ini_img = 0;
    double time_ini_img_pyramid = 0;
    double time_ini_img_switch = 0;
    double time_ini_img_normalize = 0;
    double time_ini_img_open = 0;
    double time_keypoint_detection = 0;
    double time_match_keypoint = 0;
    double time_clean_match_keypoint = 0;
    double time_estim_transf = 0;
    double time_estim_transf_homography = 0;
    double time_estim_transf_correction = 0;
    double time_estim_transf_save = 0;
#endif // WITH_TIMERS

    try {
        // PARAMETERS

        string pathRaw; // Path raw images (pgm)
        if (argc > 1)
        {
            pathRaw = argv[1];
            char last = pathRaw[pathRaw.length()];
            if (last != '/')
            {
                pathRaw = pathRaw + '/';
            }
        }
        else
        {
            pathRaw = "./img_pgm/";
        }

        int KeyDens;
        float threshold_max = 10000.f; // Threshold for keypoints detection
        int similarity = 0;         // Transformation model : 0 -> projective ; 1 -> similarity
        bool isOrsa = true;            // Clean match with ORSA
        int divisor = 0;            // Divide image resolution (computational time)
        float prog = 0;                // Counter for progression

        //Read param files
            //stab_param.dat
        int bufInt;
        fstream stabP("./outputs.dir/stab_param.dat", ios::in);

        if (stabP.is_open())
        {
            stabP.ignore(127, '\n');
            stabP >> bufInt;
            if (bufInt == 2)
            {
                KeyDens = 2;
            }
            else if (bufInt == 1)
            {
                KeyDens = 1;
            }
            else if (bufInt == 0)
            {
                KeyDens = 0;
            }
            else
                throw std::string("ERROR READING PARAMETERS : stab_param.dat contents");
            stabP.ignore(127, '\n');
            stabP.ignore(127, '\n');
            stabP >> bufInt;
            if (bufInt == 1)
            {
                similarity = 1;
            }
            else if (bufInt == 0)
            {
                similarity = 0;
            }
            else
                throw std::string("ERROR READING PARAMETERS : stab_param.dat contents");
            stabP.close();
        }
        else
            throw std::string("ERROR READING PARAMETERS : couldn't find stab_param.dat");


        // Open first image (reference)
        string nomtst = "image0001.pgm";
        int indBegin = 1, indEnd = -1;
        fstream imgtest(pathRaw + nomtst);
        // Look for first image
        while (!imgtest.is_open() && indBegin < 100)
        {
            indBegin++;
            if (indBegin % 10 == 0)
            {
                nomtst[7] ++;
                nomtst[8] -= 9;
            }
            else
            {
                nomtst[8] ++;
            }

            imgtest.close();
            imgtest.open(pathRaw + nomtst);

        }
        // Look for last image
        indEnd = indBegin;
        while (imgtest.is_open() && indBegin < 1000)
        {
            imgtest.close();
            indEnd++;
            // Change image name
            if (indEnd % 100 == 0)
            {
                nomtst[6] ++;
                nomtst[7] -= 9;
                nomtst[8] -= 9;
            }
            else if (indEnd % 10 == 0)
            {
                nomtst[7] ++;
                nomtst[8] -= 9;
            }
            else
            {
                nomtst[8] ++;
            }
            imgtest.open(pathRaw + nomtst);
        }
        indEnd--;
        // Use to measure progression of the algorithm within the image stack
        float* progStep = new float();
//        *progStep = floor((1/ (float)(indEnd - indBegin +1))*100);
        *progStep = 0.01*floor((1/ (float)(indEnd - indBegin +1))*10000);
        // Re-initialization & open first image
        ostringstream nomImg1;
        if (indBegin < 10)
        {
            nomImg1 << "image000" << indBegin << ".pgm";
        }
        else if (indBegin < 100)
        {
            nomImg1 << "image00" << indBegin << ".pgm";
        }
        CImageNdg imgExtC1(pathRaw + nomImg1.str(), "pgm");

        //Read param files
            //mask.dat
        fstream maskF("./outputs.dir/mask.dat", ios::in);
        stringstream buffer;
        CImageNdg imgCalc;
        if (maskF.is_open()) // Ensure file mask.dat is open
        {
            // Time optimisation with class stringstream : reading all buffer at once
            buffer << maskF.rdbuf();
            maskF.close(); // Closing the stream file
            // Initialisation of mask image and maskBo, boolean used to ensure that mask.dat contains data
            bool maskBo = false;
            imgCalc = *new CImageNdg(imgExtC1.lireHauteur(), imgExtC1.lireLargeur(), 127); // creation of stamp image for mask
            while (buffer)
            {
                maskBo = true;
                int _i, _j;
                buffer >> _i >> _j;
                imgCalc(_i, _j) = 0;
            }
            if (!maskBo)throw string("ERROR READING PARAMETERS : mask.dat is empty");
        }
        else
            throw string("ERROR READING PARAMETERS : couldn't find mask.dat");


        // INITIALISATIONS ----------------------------------------------------------

        // Determine the 'divisor' factor used to lower the resolution to 1k - save computational time
            //4K
        if (imgExtC1.lireLargeur() > 3200)divisor = 2;
        //Full HD
        if (imgExtC1.lireLargeur() > 1600 && imgExtC1.lireLargeur() < 3201)divisor = 1;

        // Gaussian pyramid to play with image resolution
        CImageDouble tmp2(imgExtC1, "cast");
        CImageDouble tmpCalc(imgCalc, "cast");
        vector<CImageDouble> pyr = tmp2.pyramide(divisor+1,3,0.7);
        vector<CImageDouble> pyrCalc = tmpCalc.pyramide(divisor+1,3,0.7);
        CImageNdg imgSrc1 = pyr[divisor].toNdg(); // Divisor determine the "floor" of the pyramid - each floor divide by 2 the resolution
        CImageNdg imgCalq = pyrCalc[divisor].toNdg();

        // Switch to "image" class (compatibility with SURF library)
        image img1(imgSrc1.lireLargeur(), imgSrc1.lireHauteur(), imgSrc1.lireData());
        img1.normalizeImage();
        // Keypoints detection frame 1 (reference)
        listKeyPoints* lst_keypoint1 = new listKeyPoints();
        listDescriptor* lst_descriptor1;
        lst_descriptor1 = getKeyPoints(&img1, lst_keypoint1, threshold_max, imgCalq);

        // Set divisor and threshold for keypoints detection depending on "keypoint density" parameter
        // Loop while the amount of keypoint isn't include in the given range
        // We assume a linear relationship between amount of keypoints and threshold
        int max_ITER = 0;
        switch (KeyDens)
        {
        case 0: // Low density of point [300 ; 500]
            while ((lst_descriptor1->size() < 300 || lst_descriptor1->size() > 500) && max_ITER < 50)
            {
                float step_div = threshold_max;
                if (lst_descriptor1->size() < 300)
                {    // Difference between amount of keypoint and target (in %)
                    float diff_size = float(300 - lst_descriptor1->size()) / 300.f;
                    threshold_max -= (step_div*diff_size); // Adjusting threshold depending on difference
                }
                else if (lst_descriptor1->size() > 500)
                {   // Difference between amount of keypoint and target (in %)
                    float diff_size = float(lst_descriptor1->size() - 500) / 500.f;
                    threshold_max += (step_div / diff_size); // Adjusting threshold depending on difference
                }

                lst_descriptor1->clear();
                lst_keypoint1->clear();
                lst_descriptor1 = getKeyPoints(&img1, lst_keypoint1, threshold_max, imgCalq);
                max_ITER++;
            }
            break;
        case 1: // Medium density of point[1000 ; 2000]
            while ((lst_descriptor1->size() < 1000 || lst_descriptor1->size() > 2000) && max_ITER < 50)
            {
                float step_div = threshold_max;
                //last_threshold = threshold_max;
                if (lst_descriptor1->size() < 1000)
                {   // Difference between amount of keypoint and target (in %)
                    float diff_size = float(1000 - lst_descriptor1->size()) / 1000.f;
                    threshold_max -= (step_div*diff_size); // Adjusting threshold depending on difference
                }
                else if (lst_descriptor1->size() > 2000)
                {   // Difference between amount of keypoint and target (in %)
                    float diff_size = float(lst_descriptor1->size() - 2000) / 2000.f;
                    threshold_max += (step_div / diff_size); // Adjusting threshold depending on difference
                }

                lst_descriptor1->clear();
                lst_keypoint1->clear();
                lst_descriptor1 = getKeyPoints(&img1, lst_keypoint1, threshold_max, imgCalq);
                max_ITER++;

            }
            break;
        case 2:// High density of point[3000 ; 5000]
            while ((lst_descriptor1->size() < 3000 || lst_descriptor1->size() > 5000) && max_ITER < 50)
            {
                float step_div = threshold_max;
                if (lst_descriptor1->size() < 3000)
                {   // Difference between amount of keypoint and target (in %)
                    float diff_size = float(3000 - lst_descriptor1->size()) / 3000.f;
                    threshold_max -= (step_div*diff_size); // Adjusting threshold depending on difference
                }
                else if (lst_descriptor1->size() > 5000)
                {   // Difference between amount of keypoint and target (in %)
                    float diff_size = float(lst_descriptor1->size() - 5000) / 5000.f;
                    threshold_max += (step_div / diff_size); // Adjusting threshold depending on difference
                }

                lst_descriptor1->clear();
                lst_keypoint1->clear();
                lst_descriptor1 = getKeyPoints(&img1, lst_keypoint1, threshold_max, imgCalq);
                max_ITER++;
            }
            break;
        }
#ifdef WITH_TIMERS
        time(&end_tmp);
        cout << "Init time : " << difftime(end_tmp, start_tmp) << "sec" << endl;
        time(&start_tmp);
#endif // WITH_TIMERS

        // Save first frame
        ostringstream fileImg1;
        if (indBegin < 10)
        {
            fileImg1 << "image000" << indBegin << "_stab";
        }
        else if (indBegin < 100)
        {
            fileImg1 << "image00" << indBegin << "_stab";
        }
        imgExtC1.ecrireNom(fileImg1.str());
        imgExtC1.sauvegardePGM("P2", "./img_stab/");
        prog += *progStep;
#ifdef WITH_TIMERS
        time(&end_tmp);
        time_ini_img = difftime(end_tmp, start_tmp);
#endif // WITH_TIMERS

        cout  << setprecision(3) << prog << "% " << nomImg1.str() << " OK" << endl;

        // LOOP ON ALL FRAMES  --------------------------------------------------------------------------
        CImageNdg* imgExtC2;
        CImageNdg* imgTransf;
        CImageNdg* imgSrc2;
        CImageDouble* tmp3;
        vector<CImageDouble>* pyr2;
        image* img2;
        listKeyPoints* lst_keypoint2;;
        listDescriptor* lst_descriptor2;
        vector<Match>* match_coor;
        libNumerics::ComputeH::Type modeleTransf; // Define transform. model

#ifdef _OPENMP
        int num_procs = omp_get_num_procs();
        //int max_thread = min(4,num_procs - 1);
        char* pNumMaxThreads;
        int max_thread;
        pNumMaxThreads = getenv("OMP_NUM_THREADS");
        if (pNumMaxThreads != NULL)
        {
           max_thread = omp_get_max_threads();
        }
        else
        {
           max_thread = (num_procs/2)-1;
       }
       fprintf(stdout,"Nombre de threads : %d\n",max_thread);

#ifdef WITH_TIMERS
#pragma omp parallel num_threads(max_thread) shared(imgCalq, lst_descriptor1, prog,progStep,time_ini_img,time_keypoint_detection,time_match_keypoint,time_clean_match_keypoint,time_estim_transf,time_ini_img_open,time_ini_img_pyramid,time_ini_img_switch,time_ini_img_normalize,time_estim_transf_homography,time_estim_transf_correction,time_estim_transf_save) private(imgExtC2,imgTransf,imgSrc2,tmp3,pyr2,img2,lst_keypoint2,lst_descriptor2,match_coor,modeleTransf,start_tmp,end_tmp,start_tmp2,end_tmp2)
#else // WITH_TIMERS
#pragma omp parallel num_threads(max_thread) shared(imgCalq, lst_descriptor1, prog,progStep) private(imgExtC2,imgTransf,imgSrc2,tmp3,pyr2,img2,lst_keypoint2,lst_descriptor2,match_coor,modeleTransf)
#endif // WITH_TIMERS
{
#pragma omp for schedule(static,(indEnd-indBegin)/max_thread)

#endif
        for (int k = indBegin + 1; k < indEnd + 1; k++)
        {
        #ifdef _OPENMP
            int n = omp_get_thread_num();
        #endif

            //fprintf(stdout, "Je suis le thread %d, traite l'image %d\n", n, k);

#ifdef WITH_TIMERS
            time(&start_tmp);
#endif // WITH_TIMERS
#ifdef WITH_TIMERS
            time(&start_tmp2);
#endif // WITH_TIMERS

            // Set file names
            ostringstream nomImg2, fileImg2;
            if (k < 10)
            {
                nomImg2 << "image000" << k << ".pgm";
                fileImg2 << "image000" << k;
            }
            else if (k < 100)
            {
                nomImg2 << "image00" << k << ".pgm";
                fileImg2 << "image00" << k;
            }
            else if (k < 1000)
            {
                nomImg2 << "image0" << k << ".pgm";
                fileImg2 << "image0" << k;
            }
            // Open file
            imgExtC2 = new CImageNdg(pathRaw + nomImg2.str(), "pgm");
            // Gaussian pyramid to play with image resolution - save computational time
#ifdef WITH_TIMERS
            time(&end_tmp2);
            time_ini_img_open += difftime(end_tmp2, start_tmp2);
            time(&start_tmp2);
#endif // WITH_TIMERS
            tmp3 = new CImageDouble(*imgExtC2, "cast");
            pyr2 = new vector<CImageDouble>(3);
            *pyr2 = tmp3->pyramide(divisor+1,3,0.7);
            delete tmp3;
            imgSrc2 = new CImageNdg(pyr2->operator[](divisor).lireHauteur(), pyr2->operator[](divisor).lireLargeur());
            *imgSrc2 = pyr2->operator[](divisor).toNdg(); // Divisor determine the "floor" of the pyramid - each floor divide by 2 the resolution
            delete pyr2;
#ifdef WITH_TIMERS
            time(&end_tmp2);
            time_ini_img_pyramid += difftime(end_tmp2, start_tmp2);
            time(&start_tmp2);
#endif // WITH_TIMERS

            // Switch to "image" class (compatibility with SURF library)
            img2 = new image(imgSrc2->lireLargeur(), imgSrc2->lireHauteur(), imgSrc2->lireData());
#ifdef WITH_TIMERS
            time(&end_tmp2);
            time_ini_img_switch += difftime(end_tmp2, start_tmp2);
            time(&start_tmp2);
#endif // WITH_TIMERS
            img2->normalizeImage();
#ifdef WITH_TIMERS
            time(&end_tmp2);
            time_ini_img_normalize += difftime(end_tmp2, start_tmp2);
#endif // WITH_TIMERS
#ifdef WITH_TIMERS
            time(&end_tmp);
            time_ini_img += difftime(end_tmp, start_tmp);
            time(&start_tmp);
#endif // WITH_TIMERS

            // Keypoint detection frame X  -----------------------------------------------------
            lst_keypoint2 = new listKeyPoints();
            lst_descriptor2 = getKeyPoints(img2, lst_keypoint2, threshold_max, imgCalq);
#ifdef WITH_TIMERS
            time(&end_tmp);
            time_keypoint_detection += difftime(end_tmp, start_tmp);
            time(&start_tmp);
#endif // WITH_TIMERS

            // Match keypoints     --------------------------------------------------------------
            match_coor = matchDescriptorP(lst_descriptor1, lst_descriptor2);
#ifdef WITH_TIMERS
            time(&end_tmp);
            time_match_keypoint += difftime(end_tmp, start_tmp);
            time(&start_tmp);
#endif // WITH_TIMERS

            // Delete similar points
            //cleanMatch(match_coor); // Correction (04.2019) --> No need to delete similar points

            // Clean match with ORSA method
            if (match_coor->size() > 10 && isOrsa)
                *match_coor = cleanMatchORSA(*match_coor);
#ifdef WITH_TIMERS
            time(&end_tmp);
            time_clean_match_keypoint += difftime(end_tmp, start_tmp);
            time(&start_tmp);
#endif // WITH_TIMERS

            // Estimation of the transformation    -----------------------------------------------
            modeleTransf = libNumerics::ComputeH::ComputeH::Similarity;
            if (similarity == false)modeleTransf = libNumerics::ComputeH::ComputeH::Projective;

            libNumerics::ComputeH matPt(modeleTransf);
            libNumerics::Homography matTransf;
            libNumerics::Homography TransfInv;
            // Run only if we have 10 points.
            if (match_coor->size() > 10)
            {
#ifdef WITH_TIMERS
                time(&start_tmp2);
#endif // WITH_TIMERS
                for (int i = 0; i < match_coor->size() - 1; i++)
                    if (divisor != 0)
                    {
                        matPt.add(match_coor->operator[](i).x2*divisor * 2, match_coor->operator[](i).y2 * 2 * divisor, match_coor->operator[](i).x1*divisor * 2, match_coor->operator[](i).y1*divisor * 2);
                    }
                    else
                    {
                        matPt.add(match_coor->operator[](i).x2, match_coor->operator[](i).y2, match_coor->operator[](i).x1, match_coor->operator[](i).y1);
                    }


                matPt.compute(matTransf);  // Compute homography matrix
                TransfInv = matTransf.inverse();
#ifdef WITH_TIMERS
                time(&end_tmp2);
                time_estim_transf_homography += difftime(end_tmp2, start_tmp2);
                time(&start_tmp2);
#endif // WITH_TIMERS

                // Initialisation of output image
                imgTransf = new CImageNdg(imgExtC2->lireHauteur(), imgExtC2->lireLargeur(), 0);
                // Using inverse transform to apply correction
                for (int j = 0; j < imgExtC2->lireHauteur(); j++)
                    for (int i = 0; i < imgExtC2->lireLargeur(); i++)
                    {
                        // Initialisation of coord.
                        double coord_i = i;
                        double coord_j = j;
                        // Applly inverse transf.
                        TransfInv(coord_i, coord_j);
                        // Bicubic interpolation --> Correction (04.2019)
                        if (coord_j > 1 && coord_j < imgExtC2->lireHauteur() - 2 && coord_i > 1 && coord_i < imgExtC2->lireLargeur() - 2)
                            imgTransf->operator()(j, i) = InterpolateBicubic(imgExtC2, &coord_j, &coord_i);
                    }

                imgTransf->ecrireNom(fileImg2.str() + "_stab");
#ifdef WITH_TIMERS
                time(&end_tmp2);
                time_estim_transf_correction += difftime(end_tmp2, start_tmp2);
                time(&start_tmp2);
#endif // WITH_TIMERS
                imgTransf->sauvegardePGM("P2", "./img_stab/");
#ifdef WITH_TIMERS
                time(&end_tmp2);
                time_estim_transf_save += difftime(end_tmp2, start_tmp2);
#endif // WITH_TIMERS
            }
            else
            {
                //cout << "Not enough matching points..." << endl;

                imgTransf = new CImageNdg(*imgExtC2);

                imgTransf->ecrireNom(fileImg2.str() + "_stab");
                imgTransf->sauvegardePGM("P2", "./img_stab/");
            }
            prog += *progStep;
            cout << setprecision(3) << prog << "% " << nomImg2.str() << " OK" << endl;
#ifdef WITH_TIMERS
            time(&end_tmp);
            time_estim_transf += difftime(end_tmp, start_tmp);
#endif // WITH_TIMERS

            // MemCheck
            delete lst_descriptor2;
            delete lst_keypoint2;
            delete imgSrc2;
            delete imgTransf;
            delete img2;
            delete match_coor;
            delete imgExtC2;
            }
#ifdef _OPENMP
}
// #ifdef WITH_TIMERS
// #pragma omp reduction(+:time_ini_img,time_keypoint_detection,time_match_keypoint,time_clean_match_keypoint,time_estim_transf,time_ini_img_open,time_ini_img_pyramid,time_ini_img_switch,time_ini_img_normalize,time_estim_transf_homography,time_estim_transf_correction,time_estim_transf_save)
// #endif // WITH_TIMERS
#endif

#ifdef WITH_TIMERS
        cout << "  Init image time :         " << time_ini_img << "sec" << endl;
        cout << "    Init open time :        " << time_ini_img_open << "sec" << endl;
        cout << "    Init pyramid time :     " << time_ini_img_pyramid << "sec" << endl;
        cout << "    Init switch time :      " << time_ini_img_switch << "sec" << endl;
        cout << "    Init normalize time :   " << time_ini_img_normalize << "sec" << endl;
        cout << "  Keypoint detection time : " << time_keypoint_detection << "sec" << endl;
        cout << "  Match keypoint time :     " << time_match_keypoint << "sec" << endl;
        cout << "  Clean keypoint time :     " << time_clean_match_keypoint << "sec" << endl;
        cout << "  Transformation time :     " << time_estim_transf << "sec" << endl;
        cout << "     homography time :      " << time_estim_transf_homography << "sec" << endl;
        cout << "     correction time :      " << time_estim_transf_correction << "sec" << endl;
        cout << "     save time :            " << time_estim_transf_save << "sec" << endl;
        time(&end);

        seconds = difftime(end, start);
        int minutes = floor(seconds / 60);
        seconds = (int)seconds % 60;

        cout << "Total time : " << minutes << "min " << seconds << "sec" << endl;
#endif // WITH_TIMERS

    }
    catch (const string& chaine) {
        cerr << chaine << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
