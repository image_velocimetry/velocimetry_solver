#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <limits>
#include <stack>
#include <stdio.h>
#include <algorithm>
#include <cstring>

#include "ImageNdg.h"

#define MAGIC_NUMBER_BMP ('B'+('M'<<8)) // signature bitmap windows


// constructeurs et destructeur
CImageNdg::CImageNdg() {

	this->m_iHauteur  = 0;
	this->m_iLargeur  = 0;
	this->m_bBinaire  = false;
	this->m_sNom      = "vide";
	
	this->m_pucPixel  = NULL;
	this->m_pucPalette = NULL; 
}

CImageNdg::CImageNdg(int hauteur, int largeur, int valeur) {

	this->m_iHauteur = hauteur;
	this->m_iLargeur = largeur;
	this->m_bBinaire	= false; // Image Ndg par d�faut, binaire apr�s seuillage
	this->m_sNom      = "inconnu";

	this->m_pucPixel = new unsigned char[hauteur*largeur];
	this->m_pucPalette = new unsigned char[256*4];	
	choixPalette("grise"); // palette grise par d�faut, choix utilisateur 
	if (valeur != -1) 
		for (int i=0;i<this->lireNbPixels();i++)
			this->m_pucPixel[i] = valeur;
}


CImageNdg::CImageNdg(const std::string& name, const std::string& format) 
{
	std::ifstream f(name.c_str(), std::ios::in | std::ios::binary);
	if (format == "pgm") {
		if (f.is_open())
		{
			std::string extract;
			int maxVal,valPix;
			// Ecriture nom
			std::string nom(name);
			nom.pop_back();
			nom.pop_back();
			nom.pop_back();
			nom.pop_back();
			this->m_sNom = nom;
			// Allocation m�moire palette (compatibilit� BMP)
			this->m_pucPalette = new unsigned char[256 * 4];

			// Extraction Header
			f >> extract; 
			if (extract.compare("P5") == 0) // cas pgm P5 -> ASCII 
			{
				f >> this->m_iLargeur >> this->m_iHauteur;
				f >> maxVal;
				if (maxVal > 255)
				{
					throw std::string("ERREUR FORMAT : image differente 8bit");
				}
				else
				{
					this->m_pucPixel = new unsigned char[m_iLargeur * m_iHauteur];
					for (int i = 0; i < m_iHauteur; i++)
						for(int j = 0; j < m_iLargeur; j++)
					{
						f.read((char*)&this->m_pucPixel[i*this->m_iLargeur + j], sizeof(char));
					}
				}
			}
			else // cas P2 -> donn�es brutes (int)
			{
				f >> this->m_iLargeur >> this->m_iHauteur;
				f >> maxVal;
				if (maxVal > 255)
				{
					throw std::string("ERREUR FORMAT : image differente 8bit");
				}
				else
				{
					this->m_pucPixel = new unsigned char[m_iLargeur * m_iHauteur];
					for (int i = 0; i < m_iHauteur; i++)
						for (int j = 0; j < m_iLargeur; j++)
						{
							f >> valPix; 
							m_pucPixel[i*this->m_iLargeur + j] = valPix;
						}
				}
			}

			f.close();
			this->choixPalette("grise");
		}
		else
			throw std::string("ERREUR : Image absente");
	}
	else
		throw std::string("ERREUR : Format non pris en compte");

}

CImageNdg::CImageNdg(const CImageNdg& im) {

	this->m_iHauteur = im.lireHauteur();
	this->m_iLargeur = im.lireLargeur();
	this->m_bBinaire = im.lireBinaire(); 
	this->m_sNom     = im.lireNom();
	this->m_pucPixel = NULL; 
	this->m_pucPalette = NULL;

	if (im.m_pucPalette != NULL) {
		this->m_pucPalette = new unsigned char[256*4];
		memcpy(this->m_pucPalette,im.m_pucPalette,4*256);
	}
	if (im.m_pucPixel != NULL) {
		this->m_pucPixel = new unsigned char[im.lireHauteur() * im.lireLargeur()];
		memcpy(this->m_pucPixel,im.m_pucPixel,im.lireNbPixels());
	}
}

CImageNdg::~CImageNdg() {
	if (this->m_pucPixel) {
		delete[] this->m_pucPixel;
		this->m_pucPixel = NULL;
	}

	if (this->m_pucPalette) {
		delete[] this->m_pucPalette;
		this->m_pucPalette = NULL;
	}
}

void CImageNdg::sauvegardePGM(const std::string& format)
{
	if (this->m_pucPixel) {
		std::string nomFichier = "../Res/" + this->lireNom() + ".pgm"; // force sauvegarde dans r�pertoire Res (doit exister)
		std::ofstream f(nomFichier.c_str(), std::ios::out | std::ios::trunc);
		if (format.compare("P5") == 0)
		{ // ASCII -> P5 
			if (f.is_open()) {
				f << "P5" << std::endl;
				f << this->m_iLargeur << " " << this->m_iHauteur << std::endl;
				f << (int)255 << std::endl;
				// Writting NDG values
				for (int i = 0; i < this->lireHauteur(); i++)
				{
					for (int j = 0; j < this->m_iLargeur; j++)
						f.write((char*)&this->m_pucPixel[i*m_iLargeur + j], sizeof(char));
				}
			}
			else
				throw std::string("Impossible de creer le fichier de sauvegarde !");
			
		}
		else
		{ // Raw data -> P2 (int)
			if (f.is_open()) {
				f << "P2" << std::endl;
				f << this->m_iLargeur << " " << this->m_iHauteur << std::endl;
				f << (int)255 << std::endl;
				// Writting NDG values
				for (int i = 0; i < this->lireHauteur(); i++)
					for (int j = 0; j < this->m_iLargeur; j++)
						f << (int)m_pucPixel[i*m_iLargeur + j] << " ";
			}
			else
				throw std::string("Impossible de creer le fichier de sauvegarde !");

		}
		
		f.close();
	}
	else
		throw std::string("Pas de donnee a sauvegarder !");
}

void CImageNdg::sauvegardePGM(const std::string& format, const std::string& dir)
{
	if (this->m_pucPixel) {
		std::string nomFichier = dir + this->lireNom() + ".pgm"; // force sauvegarde dans le r�pertoire dir
		std::ofstream f(nomFichier.c_str(), std::ios::out | std::ios::trunc);
		if (format.compare("P5") == 0)
		{ // ASCII -> P5 
			if (f.is_open()) {
				f << "P5" << std::endl;
				f << this->m_iLargeur << " " << this->m_iHauteur << std::endl;
				f << (int)255 << std::endl;
				// Writting NDG values
				for (int i = 0; i < this->lireHauteur(); i++)
				{
					for (int j = 0; j < this->m_iLargeur; j++)
						f.write((char*)&this->m_pucPixel[i*m_iLargeur + j], sizeof(char));
				}
			}
			else
				throw std::string("Impossible de creer le fichier de sauvegarde !");

		}
		else if(format.compare("P2") == 0)
		{ // Raw data -> P2 (int)
			if (f.is_open()) {
				f << "P2" << std::endl;
				f << this->m_iLargeur << " " << this->m_iHauteur << std::endl;
				f << (int)255 << std::endl;
				// Writting NDG values
				for (int i = 0; i < this->lireHauteur(); i++)
					for (int j = 0; j < this->m_iLargeur; j++)
						f << (int)m_pucPixel[i*m_iLargeur + j] << " ";
			}
			else
				throw std::string("Impossible de creer le fichier de sauvegarde !");

		}else
			throw std::string("ERREUR FORMAT : format PGM non pris en compte");

		f.close();
	}
	else
		throw std::string("Pas de donnee a sauvegarder !");
}

CImageNdg& CImageNdg::operator=(const CImageNdg& im) {

	if (&im == this)
		return *this;

	this->m_iHauteur = im.lireHauteur();
	this->m_iLargeur = im.lireLargeur();
	this->m_bBinaire = im.lireBinaire(); 
	this->m_sNom     = im.lireNom();

	if (this->m_pucPixel) 
		delete[] this->m_pucPixel;
	this->m_pucPixel = new unsigned char[this->m_iHauteur * this->m_iLargeur];

	if (this->m_pucPalette)
		delete[] this->m_pucPalette;
	this->m_pucPalette = new unsigned char[256*4];

	if (im.m_pucPalette != NULL)
		memcpy(this->m_pucPalette,im.m_pucPalette,4*256);
	if (im.m_pucPixel != NULL)
		memcpy(this->m_pucPixel,im.m_pucPixel,im.lireNbPixels());

return *this;
}

// fonctionnalit�s histogramme 
std::vector<unsigned long> CImageNdg::histogramme(bool enregistrementCSV, int pas) {

	std::vector<unsigned long> h;

	h.resize(256/pas,0);
	for (int i=0;i<this->lireNbPixels();i++) 
		h[this->operator()(i)/pas] += 1L;

	if (enregistrementCSV) {
	 std::string fichier = "../Res/" + this->lireNom() + ".csv";
		std::ofstream f (fichier.c_str());

		if (!f.is_open())
			std::cout << "Impossible d'ouvrir le fichier en ecriture !" << std::endl;
		else {
			for (int i=0;i<(int)h.size();i++)
				f << h[i] << std::endl;
		}
		f.close();
	}

	return h;
}

// signatures globales
MOMENTS CImageNdg::signatures(const std::vector<unsigned long>& h) {

	MOMENTS globales;
	
	// min
	int i=0;
	while ((i < (int)h.size()) && (h[i] == 0))
		i++;
	globales.minNdg = i;
		
	// max
	i=h.size()-1;
	while ((i > 0) && (h[i] == 0))
		i--;
	globales.maxNdg = i;

	// mediane

	int moitPop = this->lireNbPixels()/2;

	i=globales.minNdg;
	int somme = h[i];
	while (somme < moitPop) {
		i += 1;
		if (i < (int)h.size())
			somme += h[i];
	}
	globales.medianeNdg = i;

	// moyenne et �cart-type
	float moy=0,sigma=0;
	for (i=globales.minNdg;i<=globales.maxNdg;i++) {
		moy += ((float)h[i])*i;
		sigma += ((float)h[i])*i*i;
	}
	moy /= (float)this->lireNbPixels();
	sigma = sqrt(sigma/(float)this->lireNbPixels() - (moy*moy));
	globales.moyenneNdg = moy;
	globales.ecartTypeNdg = sigma;

	return globales;
}

MOMENTS CImageNdg::signatures() {
	
	MOMENTS globales;
	std::vector<unsigned long> hist;
	hist=this->histogramme();

	globales = this->signatures(hist);
	return globales;
}

// op�rations ensemblistes images binaires
CImageNdg& CImageNdg::operation(const CImageNdg& im, const std::string& methode) {

	if ((&im == this) || !(this->lireBinaire() && im.lireBinaire())) {
		std::cout << "operation logique uniquement possible entre 2 images binaires" << std::endl;
		return *this;
	}

	this->m_iHauteur = im.lireHauteur();
	this->m_iLargeur = im.lireLargeur();
	this->m_bBinaire = im.lireBinaire(); 
	this->choixPalette("binaire"); // images binaires -> palettes binaires
	this->m_sNom     = im.lireNom()+"L";

	if (methode.compare("et") == 0) {
		for (int i=0;i<this->lireNbPixels();i++)
			this->operator()(i) = this->operator()(i) && im(i);
	}
	else
		if (methode.compare("ou") == 0) {
			for (int i=0;i<this->lireNbPixels();i++)
				this->operator()(i) = this->operator()(i) || im(i);
		}

return *this;
}

// seuillage
CImageNdg CImageNdg::seuillage(const std::string& methode, int seuilBas, int seuilHaut) {
	
	if (!this->m_bBinaire) {
		CImageNdg out(this->lireHauteur(),this->lireLargeur());
		out.m_sNom     = this->lireNom()+"S";
		out.choixPalette("binaire"); // palette binaire par d�faut
		out.m_bBinaire = true;

		// cr�ation lut pour optimisation calcul
		std::vector<int> lut;
		lut.resize(256);

		// recherche valeur seuil
		// cas "manuel" -> seuil reste celui pass� en param�tre

		if (methode.compare("auto") == 0) 
		{
			std::vector<unsigned long> hist = this->histogramme();
			std::vector<unsigned long> histC; // histogramme cumul�
			histC.resize(256,0);
			histC[0] = hist[0];
			for (int i=1;i<(int)hist.size();i++) 
				histC[i] = histC[i-1]+hist[i];

			MOMENTS globales = this->signatures(hist);
			int min = globales.minNdg,
				max = globales.maxNdg;

			// f(s)
			std::vector<double> tab;
			tab.resize(256,0);
		
			double M1,M2,w1;
		
			// initialisation
			M1 = min;
			seuilBas = min;
			seuilHaut = 255;

			w1 = (double)histC[min] / (double)(this->lireNbPixels());
			M2 = 0;
			for (int i = min + 1; i <= max; i++)
				M2 += (double)hist[i] * i;
			M2 /= (double)(histC[max] - hist[min]);
			tab[min] = w1*(1 - w1)*(M1 - M2)*(M1 - M2);

			// parcours seuillage automatique Otsu
			for (int i = min + 1; i < max; i++) {
				M1 = ((double)histC[i - 1] * M1 + (double)hist[i] * i) / histC[i];
				M2 = ((double)(histC[255] - histC[i - 1])*M2 - hist[i] * i) / (double)(histC[255] - histC[i]);
				w1 = (double)histC[i] / (double)(this->lireNbPixels());
				tab[i] = w1*(1 - w1)*(M1 - M2)*(M1 - M2);
				if (tab[i] > tab[seuilBas])
					seuilBas = i;
			}
		}
		else {
			// gestion des seuils valant "moyenne" et "mediane"
		}

		// fin recherche valeur seuil 

		// g�n�ration lut
		for (int i = 0; i < seuilBas; i++)
			lut[i] =  0; 
		for (int i = seuilBas; i <= seuilHaut; i++)
			lut[i] = 1;
		for (int i = seuilHaut+1; i <= 255; i++)
			lut[i] = 0;

		// cr�ation image seuill�e
		std::cout << "Seuillage des pixels entre " << seuilBas << " et " << seuilHaut << std::endl;
		for (int i=0; i < out.lireNbPixels(); i++) 
			out(i) = lut[this->operator ()(i)]; 

		return out;
		}
	else {
		std::cout << "Seuillage image binaire impossible" << std::endl;
		return (*this);
	}
}

// renvoie une image avec la ligne de pixel les plus haut dans une image seuill�
CImageNdg CImageNdg::extractLigne() {

	if (this->m_bBinaire)
	{
		CImageNdg out(this->lireHauteur(), this->lireLargeur(), 0); // cr�ation de l'image de sortie, pour l'instant compl�tement noire
		out.m_sNom = this->lireNom() + "Line";
		out.m_bBinaire = this->lireBinaire();

		int i;
		// parcours de toutes les colonnes
		for (int j = 0; j < this->lireLargeur(); j++)
		{
			i = 0;
			while (this->operator()(i, j) == 0) // si le pixel est noir on descend
			{
				i++;
			}
			out(i, j) = 1; // d�s que l'on trouve le pixel blanc le plus haut de la colonne on le met � blanc
		}

		return out;
	}
	else {
		std::cout << "Extraction de ligne sur une image non binaire impossible" << std::endl;

		return *this;
	}

}

