// ajouter  devant tout public pour permettre � la dll d'exporter ces m�thodes 
// pour qu'elles puissent �tre utilis�es par d'autres applications ou programmes

#pragma once

#ifndef _IMAGE_DOUBLE_
#define _IMAGE_DOUBLE_

// d�finition classe Image Double pour stocker informations autres que ndg
#include <vector>
#include <string.h>

#include "ImageNdg.h"


class CImageDouble {

	///////////////////////////////////////
	private : 
	///////////////////////////////////////

		int              m_iHauteur;
		int              m_iLargeur; 
		std::string      m_sNom;
		double			 m_vMin; // valeur plus petite possible
		double           m_vMax; // valeur plus grande possible
		double*          m_pucPixel;

	///////////////////////////////////////
	public : 
	///////////////////////////////////////

		// constructeurs
		CImageDouble(); // par d�faut
		CImageDouble(int hauteur, int largeur); // initialisation � 0
		 CImageDouble(const CImageNdg& im, const std::string& methode = "normalise");  // choix "normalise"/"cast"/"integrale1"/"integrale2"
		// image Ndg en entr�e
		 CImageDouble(const CImageDouble& im); // image en entr�e
		// image double format autre en entr�e 
		 CImageDouble(int hauteur, int largeur, double* data); // image en entr�e

		// destructeur
		 ~CImageDouble();

		// pouvoir acc�der � un pixel par image(i)
		 double& operator() (int i) const { 
		return m_pucPixel[i];
		}

		// pouvoir acc�der � un pixel par image(i,j)
		 double& operator() (int i, int j) const { 
		return m_pucPixel[i*m_iLargeur+j];
		}

		// op�rateur copie image par imOut = imIn
		 CImageDouble& operator=(const CImageDouble& im);

		// get et set 

		 int lireHauteur() const {  
		return m_iHauteur;
		}

		 int lireLargeur() const {
		return m_iLargeur;
		}

		 double lireMin() const {
			return m_vMin;
		}

		 double lireMax() const {
			return m_vMax;
		}

		 std::string lireNom() const {
		return m_sNom;
		}

		 void ecrireHauteur(int hauteur) {
		m_iHauteur = hauteur;
		}

		 void ecrireLargeur(int largeur) {
		m_iLargeur = largeur;
		}

		 void ecrireMin(double min) {
			m_vMin = min;
		}

		 void ecrireMax(double max) {
			m_vMax = max;
		}

		 void ecrireNom(std::string nom) {
		m_sNom = nom;
		}

		 int lireNbPixels() const { 
		return m_iHauteur*m_iLargeur;
		}

		// seuillage manuel uniquement
		 CImageDouble seuillage(double seuilBas = 0, double seuilHaut = 1);

		// distance au fond
		 CImageDouble distance(std::string eltStructurant = "V8", double valBord = 0);

		// conversions !!!
		 CImageNdg toNdg(const std::string& methode = "defaut"); // choix "defaut"/"expansion"

		// Hough Transform
		 CImageDouble planHough();
		 CImageDouble maxiLocaux(int N = 9, int M = 9) const;
		 CImageDouble extractionLignes(int N=9, int M=9, double dimLigne=50, bool affichage="true");
		 CImageNdg    houghInverse(const CImageNdg& img); 

		// Vecteur gradient
		 CImageDouble vecteurGradient(const std::string& axe = "norme"); // choix "norme"/"angle"

		// Filtrage : moyen/gaussien
		 CImageDouble filtrage(const std::string& methode = "moyen", int N = 3, double sigma = 0.5);

		// d�tecteur de Harris
		// CImageClasse harris(double k=0.01, double seuil=0.1, int taille=7, double sigma=2);

		// pyramides
		 std::vector<CImageDouble> pyramide(int hauteur = 5, int tailleFiltre = 5, double sigma = 1);

};

#endif 