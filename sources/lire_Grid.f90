!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.


subroutine lire_Grid(grid_file, compt, x, y)
!  Read the grid file
   implicit none
   ! -- prototype --
   character (len=*), intent(in) :: grid_file
   integer, intent(out) :: compt
   integer, intent(out), allocatable :: x(:), y(:)
   ! -- local variables --
   integer :: lu, ios, xx, yy

   open(newunit=lu,file=trim(grid_file),status='old')
   !first read to get the size of the grid
   compt = 0
   do
      read(lu,*,iostat=ios) xx, yy
      if (ios /= 0) then
         exit
      else
         compt = compt+1
      endif
   enddo
   rewind(lu)
   !memory allocation
   allocate(x(compt),y(compt))
   !second read to read the grid
   compt = 0
   do
      read(lu,*,iostat=ios) xx, yy
      if (ios /= 0) then
         exit
      else
         compt = compt+1  ;  x(compt) = xx  ;  y(compt) = yy
      endif
   enddo
   close(lu)
end subroutine lire_Grid
