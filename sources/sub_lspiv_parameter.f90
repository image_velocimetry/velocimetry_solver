!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

module lspiv_parameter
integer,parameter :: dp = kind(1.0d0)
!standard subfolders in working directory
character (len=7),  parameter :: img_pgm = 'img_pgm'
character (len=8),  parameter :: img_stab = 'img_stab'
character (len=10), parameter :: img_transf = 'img_transf'
character (len=11), parameter :: outputs_dir = 'outputs.dir'
character (len=9),  parameter :: transects = 'transects'
character (len=10), parameter :: vel_filter = 'vel_filter'
character (len=7),  parameter :: vel_raw = 'vel_raw'
character (len=8),  parameter :: vel_real = 'vel_real'
character (len=8),  parameter :: vel_scal = 'vel_scal'

!standard datafiles
character (len=11), parameter :: img_ref = 'img_ref.dat'
character (len=13), parameter :: PIV_param = 'PIV_param.dat'
character (len=12), parameter :: list_avg = 'list_avg.dat'
character (len=09), parameter :: bathy = 'bathy.dat'
character (len=11), parameter :: bathy_p = 'bathy_p.dat'
character (len=15), parameter :: average_vel = 'average_vel.out'
character (len=05), parameter :: h_dat = 'h.dat'
character (len=13), parameter :: discharge = 'Discharge.dat'
character (len=08), parameter :: grid = 'grid.dat'
character (len=16), parameter :: average_scal = 'average_scal.out'
character (len=07), parameter :: GRP = 'GRP.dat'
character (len=09), parameter :: coeff_dat = 'coeff.dat'
character (len=19), parameter :: manual_tracking = 'manual_tracking.dat'
character (len=14), parameter :: manual_vel = 'manual_vel.out'
character (len=11), parameter :: filters = 'filters.dat'
end module lspiv_parameter
