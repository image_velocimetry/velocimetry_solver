!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

! ---------------------------
! LSPIV-solvers, V1.0, 2014
!
! Jerome Le Coz & Jean-Baptiste Faure, Irstea
!
! Contributions:
! -------------
! LSPIV-solvers was built on original developments from the PhD work of
! Alex Hauet (LTHE Grenoble, University of Iowa), following the seminal works
! on LSPIV by Ichiro Fujita and Marian Muste.
! Recent contributions by Alex Hauet (EDF) and Magali Jodeau (EDF) are also acknowledged.

program transf_img

!     From a raw pgm image, compute the ortho-rectified image using
!     the parameters of coeff.dat.
!     mj 4-05-2011 modifications for 2D or 3D ortho rectification
!     jlc 3-01-2017 transformation without ortho rectification and simplification of the code structure
!     jlc 10-03-2018 read and add remnant errors (3D ortho with BiM)

#ifdef openmp
   use omp_lib
#endif /* openmp */

   implicit none
   integer,parameter :: dp = kind(1.0d0)

   integer :: i, j, k, compteur, ni, nj, nb, nb_arg=0, nx, ny
   integer :: iinf, isup, jinf, jsup, i2, j2, i2max, j2max, maxx, maxy
   integer, allocatable :: img(:,:), gg(:,:)
   real(kind=dp), allocatable :: ierror(:,:), jerror(:,:)
   real(kind=dp) :: a(11), xmin, ymin, xmax, ymax, r, h, b(9)
   real(kind=dp) :: x, y, ii, jj, diff, D3, x0, y0
   character (len=60) ::  ligne
#ifdef openmp
   integer :: nb_taches, ncore
   character (len=2) :: param3
#endif /* openmp */

#ifdef version_station
   character (len=160) :: param1, param2

   nb_arg = nb_arg+1 ; call get_command_argument(nb_arg,param1)
   nb_arg = nb_arg+1 ; call get_command_argument(nb_arg,param2)
#endif /* version_station */

#ifdef openmp
   nb_arg = nb_arg+1 ; call get_command_argument(nb_arg,param3)
   read(param3,'(i2)') ncore
#endif /* openmp */



!Read the image information file img_ref.dat
#ifdef version_station
   open(14,file=trim(param1)//'/img_ref.dat', status='old')
#endif /* version_station */
#ifndef version_station
   open(14,file='outputs.dir/img_ref.dat', status='old')
#endif /* version_station */
   read(14,*) ; read(14,*) xmin, ymin
   read(14,*) ; read(14,*) xmax, ymax
   read(14,*) ; read(14,*) r
   read(14,*) ; read(14,*) ni, nj
   close(14)
   iinf = 2 ; isup = ni-1 ; jinf = 2 ; jsup = nj-1
   allocate(img(ni,nj))

!Read the raw image
#ifdef version_station
   open(11,file=trim(param2)//'/img_pgm/image1.pgm', status='old')
#endif /* version_station */
#ifndef version_station
   open(11,file='img_pgm/image1.pgm', status='old')
#endif /* version_station */
   read(11,*) ; read(11,*) ; read(11,*)
   do i = 1, ni
      read(11,*) (img(i,j),j=1,nj)
   enddo
   close(11)

!Initialize the output matrix gg(i,j)
!! debut modification par JLC le 30/08/2008
   i2max = int((xmax-xmin)/r)+1  ;  j2max = int((ymax-ymin)/r)+1
   allocate (gg(i2max,j2max),ierror(i2max,j2max),jerror(i2max,j2max))
   gg(:,:) = 0



!!! Image transformation : 3D or 2D or no orthorectification

!Read the parameters a(11) of coeff.dat
#ifdef version_station
   open(10,file=trim(param1)//'/coeff_plan.dat', status='old')
#endif /* version_station */
#ifndef version_station
   open(10,file='outputs.dir/coeff.dat', status='old')
#endif /* version_station */

   read(10,*) nb
! if nb=8 then 2D ortho (8 coefficients)
! if nb=11 then 3D ortho (11 coeff)
! if nb=0 then no ortho (0 coeff)


!!!!!!!!!!!!!!!!!!! 3D !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if (nb == 11) then   !3d orthorectif
      do i = 1, 4
         read(10,*) a(i)
      enddo
      do i = 1, 3
         read(10,*) a(i+8)
      enddo
      do i = 1, 4
         read(10,*) a(i+4)
      enddo

#ifdef version_station
      open(13,file=trim(param1)//'/h.dat', status='old')
#endif /* version_station */
#ifndef version_station
      open(13,file='outputs.dir/h.dat', status='old')
#endif /* version_station */
      read(13,*) h
      close(13)

! Only if remnant error files are available !!!
      open(20,file='outputs.dir/remnant_error_c.dat', status='old')
      read(20,*) ((ierror(i,j), j = 1, j2max), i = 1, i2max)
      close(20)

      open(21,file='outputs.dir/remnant_error_l.dat', status='old')
      read(21,*) ((jerror(i,j), j = 1, j2max), i = 1, i2max)
      close(21)


!     Compute the ortho-image
      !Definition of the river plan equation: Z=D1X+D2Y+D3
      !   D1=0
      !   D2=0
      D3=h
#ifdef openmp
      !$omp parallel default(shared) private(ii,jj,x,y,i,j) &
      !$omp num_threads(ncore)
      nb_taches = omp_get_num_threads()
      !$omp do schedule(static,i2max/nb_taches)
#endif /* openmp */
      do i = 1, i2max
         ii = xmin + i*r
         do j= 1, j2max
            jj=ymin + j*r
            !Compute the image coordinates [x,y] from space coordinates [ii,jj]
            !using the parameters a(11)
            call space2CRT(x,y,a,ii,jj,D3,ierror(i,j),jerror(i,j))
            !Only compute the area of interest defined by iinf,isup,jinf,jsup
            if (((x.ge.jinf).and.(x.le.jsup)).and.((y.ge.iinf).and.(y.le.isup))) then
               !Compute the intensity of the pixel using cubic convolution
               gg(i,j) = cub_conv(i,j,x,y,ni,nj,img)
            endif
         enddo
      enddo
#ifdef openmp
      !$omp end do
      !$omp end parallel
#endif /* openmp */


!!!!!!!!!!!!!!!!!!! 2D !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   elseif (nb == 8) then   !2d orthorectif
      do i = 1, 8
         read(10,*) a(i)
      enddo
      b(1) = a(5)-a(8)*a(6);
      b(2) = a(3)*a(8)-a(2);
      b(3) = a(2)*a(6)-a(3)*a(5);
      b(4) = a(7)*a(6)-a(4);
      b(5) = a(1)-a(3)*a(7);
      b(6) = a(3)*a(4)-a(1)*a(6);
      b(7) = a(8)*a(4)-a(7)*a(5);
      b(8) = a(2)*a(7)-a(1)*a(8);
      b(9) = a(1)*a(5)-a(2)*a(4);

!     Compute the ortho-image
#ifdef openmp
      !$omp parallel default(shared) private(ii,jj,x,y,i,j) &
      !$omp num_threads(ncore)
      nb_taches = omp_get_num_threads()
      !$omp do schedule(static,i2max/nb_taches)
#endif /* openmp */
      do i = 1, i2max
         ii = xmin + i*r
         do j= 1, j2max
            jj=ymin + j*r
            !Compute the image coordinates [x,y] from space coordinates [ii,jj]
            !using the parameters b(9)
            call space2CRT2(x,y,b,ii,jj)
            !Only compute the area of interest defined by iinf,isup,jinf,jsup
            if (((x.ge.jinf).and.(x.le.jsup)).and.((y.ge.iinf).and.(y.le.isup))) then
               !Compute the intensity of the pixel using cubic convolution
               gg(i,j) = cub_conv(i,j,x,y,ni,nj,img)
            endif
         enddo
      enddo
#ifdef openmp
   !$omp end do
   !$omp end parallel
#endif /* openmp */



!!!!!!!!!!!!!!!!!!! NO ortho !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   elseif (nb == 0) then   !no orthorectif
      read(10,*) x0
      read(10,*) y0

!     Compute the ortho-image
#ifdef openmp
      !$omp parallel default(shared) private(nx,ny,i,j) &
      !$omp num_threads(ncore)
      nb_taches = omp_get_num_threads()
      !$omp do schedule(static,i2max/nb_taches)
#endif /* openmp */
      do i = 1, i2max
         do j = 1, j2max
            nx = int((xmin - x0)/r) + i
            ny = int(nj - (ymax-y0)/r) + j
            gg(i,j) = img(ny,nx)  !img(nx,ny)
         enddo
      enddo
#ifdef openmp
      !$omp end do
      !$omp end parallel
#endif /* openmp */

   endif
   close(10)



!Write the data in the output file NomOut
   maxx = i2max-1
   maxy = j2max-1

   write(ligne,'(A,I5.5,A)') '(',maxx,'I5)'

#ifdef version_station
   open(12,file=trim(param2)//'/img_transf/image1_transf.pgm',status='unknown',form='formatted')
#endif /* version_station */
#ifndef version_station
   open(12,file='img_transf/image1_transf.pgm',status='unknown',form='formatted')
#endif /* version_station */
   write(12,'(a2)') 'P2'
   write(12,*) maxx,maxy
   write(12,'(a3)') '255'
   do i = 1, maxy
      write(12,ligne) (gg(j,i),j=1,maxx)
   enddo
   close(12)

contains
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!cc Subroutine library
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

   subroutine space2CRT(xp,yp,a,ii,jj,D3,ierr,jerr)
   !Compute image coordinates from space coordinates using a(11)
      real(kind=dp), intent(in)  :: a(11) , ii, jj, D3, ierr, jerr
      real(kind=dp), intent(out) :: xp, yp

      xp = (a(1)*ii+a(2)*jj+(a(3)*D3+a(4))) / (a(9)*ii+a(10)*jj+(a(11)*D3+1)) + ierr
      yp = (a(5)*ii+a(6)*jj+(a(7)*D3+a(8))) / (a(9)*ii+a(10)*jj+(a(11)*D3+1)) + jerr
   end subroutine space2CRT

   subroutine space2CRT2(xp,yp,b,ii,jj)
   !Compute image coordinates from space coordinates using b(9)
      real(kind=dp), intent(in)  :: b(9), ii, jj
      real(kind=dp), intent(out) :: xp, yp

      xp = (b(1)*ii+b(2)*jj+b(3)) / (b(7)*ii+b(8)*jj+b(9));
      yp = (b(4)*ii+b(5)*jj+b(6)) / (b(7)*ii+b(8)*jj+b(9));
   end subroutine space2CRT2


!   subroutine cub_conv(gg,i,j,xp,yp,ni,nj,img)
   function cub_conv(i,j,xp,yp,ni,nj,img)
   !Compute the intensity of a pixel using cubic convolution
      real(kind=dp), parameter :: zero=0._dp, un=1._dp, deux=2._dp, four=4._dp
      real(kind=dp), parameter :: cinq = 5._dp, huit=8._dp

!      integer, intent(out) :: gg(:,:)
      integer :: cub_conv, ggij
      integer, intent(in) :: ni, nj, i, j, img(:,:)
      real(kind=dp), intent(in) :: xp, yp
      integer :: nxp, nyp, k, l
      real(kind=dp) :: sx, sy, Cx, Cy, fx, sum_fx

      nxp = int(xp)
      nyp = int(yp)

      sum_fx = 0._dp
      do k = 1, 4
         do l = 1, 4
            sx = abs(nxp+k-2-xp)
            sy = abs(nyp+l-2-yp)
            if ((sx >= zero) .and. (sx < un)) then
               !Cx = un - deux*(sx*sx) + (sx*sx*sx)
               Cx = un + (sx-deux)*sx*sx
            elseif ((sx >= un) .and. (sx < deux)) then
               !Cx = four - huit*sx + cinq*(sx*sx) - (sx*sx*sx)
               Cx = four + ((cinq-sx)*sx-huit)*sx
            elseif (sx > deux) then
               Cx = zero
            endif

            if ((sy >= zero) .and. (sy < un)) then
               !Cy = un - deux*(sy*sy) + (sy*sy*sy)
               Cy = un + (sy-deux)*sy*sy
            elseif ((sy >= un) .and. (sy < deux)) then
               !Cy = four - huit*sy + cinq*(sy*sy) - (sy*sy*sy)
               Cy = four + ((cinq-sy)*sy-huit)*sy
            elseif (sy > deux) then
               Cy = zero
            endif

            fx = img(nyp+l-2,nxp+k-2)*Cx*Cy
            sum_fx = sum_fx+fx
         enddo
      enddo
      ggij = sum_fx
      if (ggij > 255) ggij = 255
      if (ggij < 0) ggij = 0
      cub_conv = ggij
   end function cub_conv

end program transf_img
