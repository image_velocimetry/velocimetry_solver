!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.

subroutine sub_filter(dir1, dir2, dt_in)
! Corrections by JLC on June 24, 2014: filter thresholds read in PIV_param.dat file;
!  add screening with correlation thresholds; all conditions now are strict (gt and lt)

! Modification by JLC on Oct 31, 2016:
! Read min/max thresholds on Vx and Vy for Fudaa-LSPIV v1.5

! Modification by JLC on July 30, 2019:
! Exit conditions of DO loops
!
!  dir1 = './outputs.dir' if used by Fudaa-LSPIV
!  dir1 = param1 if station
!  dir2 = './' if used by Fudaa-LSPIV
!  dir2 = param2 if station

   use iso_fortran_env, only: error_unit
   use errors, only: ERR_READ_LIST_AVG
   implicit none
   ! protype
   character (len=*), intent(in) :: dir1, dir2
   real(kind=dp), intent(in), optional :: dt_in
   ! local variables
   real(kind=dp) :: xmin,ymin,xmax,ymax,resol

   real(kind=dp) :: Dt, smin, smax, vxmin, vxmax, vymin, vymax, epsi, r0min, dist_max, rcut, rhomax, covmax, circvarmax
   integer :: n_neighbor, nstdvel, aggregType
!   Dt = time interval [s] between two images
!   smin & smax = velocity magnitude thresholds
!   vxmin & vxmax = Vx velocity component thresholds
!   vymin & vymax = Vy velocity component thresholds
!   epsi & r0min & n_neighbor & dist_max = median test parameters & kd-tree query parameters
!   rcut & rhomax = correlation pic width filter
!   covmax = streamwise velocity dispersion filter
!   circvarmax = angular dispersion filter
!   aggregType = aggregation at point, 0:median / 1:mean

   logical :: bCor, bNorm, bVx, bVy, bMedTest, bCorPeak, bVelDisp, bStreamDisp, bAngDisp,bOldFile
   real(kind=dp) :: x, y, u, v, r, u2v2, R_min, R_max, stdi, stdj, rhoC, npx, tmp
   character(len=160) :: BaseName, line
   character(len=:), allocatable :: NomIn, NomOut, list_avg_file
   integer :: lRef, lAvg, lPiv, lFil, lIn, lOut
   integer :: i, ios, bsize, usp, nimg, npoints, k, l
   real(kind=dp),dimension(:,:), allocatable :: lstx, lsty, lstu, lstv, lstr, lstAr, lstnpx

   open(newunit=lRef,file=trim(dir1)//'/'//img_ref,status='old')
   read(lRef,*)
   read(lRef,*) xmin,ymin
   read(lRef,*)
   read(lRef,*) xmax,ymax
   read(lRef,*)
   read(lRef,*) resol
   close (lRef)


! Read the PIV parameters
   open(newunit=lPiv,file=trim(dir1)//'/'//PIV_param,status='old')
   do i = 1, 8
      read(lPiv,*)
   enddo
   read(lPiv,'(a)',iostat=ios) line ; read(lPiv,*)
   if (present(dt_in)) then
     Dt = dt_in
   else
     read(line, *,iostat=ios)Dt, usp
     if (ios .ne. 0 .or. usp <= 0) usp = 1
     Dt = Dt * usp
   endif
   close (lPiv)
! END Read the PIV parameters

! Read the filters parameters
   open(newunit=lFil,file=trim(dir1)//'/'//filters,status='old')
   read(lFil,*)
   read(lFil,*)
   read(lFil,*) bCor, R_min, R_max
   read(lFil,*)
   read(lFil,*) bNorm, smin, smax
   read(lFil,*)
   read(lFil,*) bVx, vxmin, vxmax
   read(lFil,*)
   read(lFil,*) bVy, vymin, vymax
   read(lFil,*)
   read(lFil,*) bMedTest, epsi, r0min, n_neighbor, dist_max
   read(lFil,*)
   read(lFil,*) bCorPeak, rcut, rhomax
   read(lFil,*)
   read(lFil,*) bVelDisp, nstdvel
   read(lFil,*)
   read(lFil,*) bStreamDisp, covmax
   read(lFil,*)
   read(lFil,*) bAngDisp, circvarmax
   read(lFil,*)
   read(lFil,*) aggregType
   close (lFil)
! END Read the filters parameters

   ! Read+count all data files listed in list_avg_files
   allocate(character(len=len_trim(dir1)+13) :: list_avg_file)
   list_avg_file = trim(dir1)//'/'//list_avg
   open(newunit=lAvg,file=trim(list_avg_file),status='old')
   bsize = 0
   nimg = 0
   do
      read(lAvg,*,iostat=ios) BaseName
      if (is_iostat_end(ios)) exit
      if (ios /= 0) then
          write(error_unit,'(2a)') 'Erreur de lecture du fichier ',trim(list_avg_file)
          stop ERR_READ_LIST_AVG
      endif
      nimg = nimg + 1
      bsize = max(bsize,len_trim(BaseName))
   enddo
   rewind(lAvg)
   allocate(character(len=len_trim(dir2)+bsize+20) :: NomIn, NomOut)

   ! Open last data file to get the number of velocity points
    NomIn = trim(dir2)//'/'//vel_raw//'/'//trim(BaseName)
    open(newunit=lIn,file=trim(NomIn),status='old')
    npoints = 0
    do
        read(lIn,*,iostat=ios)
        if (is_iostat_end(ios)) exit
        if (ios /= 0) then
            write(error_unit,'(2a)') 'Erreur de lecture du fichier ', trim(NomIn)
            stop ERR_READ_LIST_AVG
        endif
        npoints = npoints + 1
    enddo
    rewind(lIn)
    ! Check if the file contains 8 columns, otherwise it's an older piv.dat file
    do k = 1,npoints
        read(lIn,*,iostat=ios) tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp
        if (is_iostat_end(ios)) then
            bOldFile = .true.
            exit
        endif
    enddo
    close(lIn)

    ! Allocate data tables
    allocate(lstx(npoints,nimg),lsty(npoints,nimg),lstu(npoints,nimg),lstv(npoints,nimg),lstr(npoints,nimg)&
    ,lstAr(npoints,nimg),lstnpx(npoints,nimg))

    ! Fill tables with data
    do k = 1, nimg
      read(lAvg,*,iostat=ios) BaseName
      if (ios /= 0) then
          write(error_unit,'(2a)') 'Erreur de lecture du fichier ',trim(list_avg_file)
          stop ERR_READ_LIST_AVG
      endif
      ! Open data file
      NomIn = trim(dir2)//'/'//vel_raw//'/'//trim(BaseName)
      open(newunit=lIn,file=trim(NomIn),status='old')
      ! Run over data file and add data in tables
      do l = 1,npoints

        if (bOldFile)  then
            read(lIn,*,iostat=ios) x, y, u, v, lstr(l,k)
        else
            read(lIn,*,iostat=ios) x, y, u, v, lstr(l,k), stdi, stdj, lstAr(l,k)
        end if
        if (ios /= 0) then
            write(error_unit,'(2a)') 'Erreur de lecture du fichier ', trim(NomIn)
            stop ERR_READ_LIST_AVG
        endif
        lsty(l,k) = y*resol+xmin
        lstx(l,k) = x*resol+ymin
        lstu(l,k) = (u*resol)/Dt
        lstv(l,k) = (v*resol)/Dt
        lstnpx(l,k) = sqrt(u**2+v**2) ! Pixel norm of disp.
      enddo
      close(lIn)
    enddo
    rewind(lAvg)

    ! Apply filters and write results
    do k = 1, nimg
      read(lAvg,*,iostat=ios) BaseName
      if (ios /= 0) then
          write(error_unit,'(2a)') 'Erreur de lecture du fichier ',trim(list_avg_file)
          stop ERR_READ_LIST_AVG
      endif
      NomOut = trim(dir2)//'/'//vel_filter//'/'//'filter_'//trim(BaseName)
      open(newunit=lOut,file=trim(NomOut),status='unknown')

      ! Median Test (apply on raw vectors)
      if (bMedTest)then
       ! Run over files

      end if

      ! Run over vel points
       do l = 1,npoints
        x   = lstx(l,k)
        y   = lsty(l,k)
        u   = lstu(l,k)
        v   = lstv(l,k)
        r   = lstr(l,k)
        npx = lstnpx(l,k)
        u2v2 = sqrt(u**2+v**2)
        if (bOldFile) then
        rhoC = -1 ! Force rhoC to -1 to cancel filter (temporary solution) GB
        else
        rhoC = sqrt(lstAr(l,k)/3.141516) ! Radius of correlation pic at 0.8*max, expressed in m
        end if

           ! Correlation filter
           if (bCor .and.((r.gt.R_max).or.(r.lt.R_min))) then
!            write(*,*) 'r',r
              lstu(l,k) = 0._dp  ;  lstv(l,k) = 0._dp
           endif
           ! Norm filter
           if (bNorm .and.((u2v2.gt.smax).or.(u2v2.lt.smin))) then
!            write(*,*) 'size',u2v2
              lstu(l,k) = 0._dp  ;  lstv(l,k) = 0._dp
           endif
           ! Vx filter
           if (bVx .and.((v.gt.vxmax).or.(v.lt.vxmin))) then
!            write(*,*) 'Vx',u
              lstu(l,k) = 0._dp  ;  lstv(l,k) = 0._dp
           endif
         ! Vy filter
           if (bVy .and.((u.gt.vymax).or.(u.lt.vymin))) then
!            write(*,*) 'Vy',v
              lstu(l,k) = 0._dp  ;  lstv(l,k) = 0._dp
           endif
           ! Correlation peak width filter
           if (bCorPeak .and. (rhoC.gt.(rhomax*npx))) then
!            write(*,*) 'Vy',v
              lstu(l,k) = 0._dp  ;  lstv(l,k) = 0._dp
           endif

       enddo

       ! Write results
       do l = 1,npoints
          write(lOut,*) lsty(l,k), lstx(l,k), lstv(l,k), lstu(l,k), lstr(l,k)
       enddo
       close(lOut)
   enddo
   close(lAvg)
end subroutine sub_filter
