!! This file is part of Image_Velocimetry_Solver, a fortran solver for image velocimetry.
!!
!! Copyright (C) 2022 INRAE
!!
!! Image_Velocimetry_Solver is free software; you can redistribute it and/or
!! modify it under the terms of the GNU Lesser General Public
!! License as published by the Free Software Foundation; either
!! version 3 of the License, or (at your option) any later version.
!!
!! Alternatively, you can redistribute it and/or
!! modify it under the terms of the GNU General Public License as
!! published by the Free Software Foundation; either version 2 of
!! the License, or (at your option) any later version.
!!
!! Image_Velocimetry_Solver is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
!! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
!! or the GNU General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public
!! License and a copy of the GNU General Public License along with
!! Image_Velocimetry_Solver If not, see <http://www.gnu.org/licenses/>.


subroutine lire_PIV_param(param_file, Bi, Bj, Sim, Sjm, Sip, Sjp, R_min, R_max, ni, nj, silent)
!  Read the PIV parameters
   use iso_fortran_env, only: error_unit
   implicit none
   ! -- prototype --
   character (len=*), intent(in) :: param_file
   integer, intent(out) :: Bi, Bj  ! IA size
   integer, intent(out) :: Sim, Sjm, Sip, Sjp   ! SA size
!            ----------------------
!            |         |          |
!            |         |Sim       |
!            |   Sjm   |    Sjp   |
!            ----------------------
!            |         |          |
!            |         |Sip       |
!            |         |          |
!            ----------------------
   real(kind=dp), intent(out) :: R_min, R_max
   !ni -> image height ; nj -> image width  (matrix view)
   integer, intent(out) :: ni, nj
   ! NOTE: set silent to .true. to skip write(error_unit,...)
   ! NOTE: if silent == .false. or not present, then lire_PIV_param() can write on error_unit
   logical, intent(in), optional :: silent
   ! -- local variables --
   integer :: lw, i
   logical :: verbose

   open(newunit=lw,file=trim(param_file),status='old')
   read(lw,*)
   read(lw,*) Bi
   if (mod(Bi,2) == 0) then
      Bi = Bi + 1
      call corrige_PIV_param(lw, Bi)
      verbose = .true. ; if (present(silent)) verbose = .not.silent
      if (verbose) then
         write(error_unit,'(2(a,i0))') '>>>> WARNING : Bi must be odd ; read value : ', Bi-1,' changed to ',Bi
         write(error_unit,'(3a)')      '              ',trim(param_file),' has been updated'
      endif
   endif
   Bj = Bi
   read(lw,*)
   read(lw,*) Sim ; read(lw,*) Sip ; read(lw,*) Sjm ; read(lw,*) Sjp
   do i = 1, 3
      read(lw,*)
   enddo
   R_min = 0
   R_max = 1
   !nj -> image width ; ni -> image height (matrix view)
   read(lw,*) nj ; read(lw,*) ni
   close (lw)
end subroutine lire_PIV_param


subroutine corrige_PIV_param(lw, Bi)
   use errors, only: ERR_READ_PIV_PARAMETER
   implicit none
   ! -- prototype --
   integer, intent(in) :: lw, Bi
   ! -- variables locales --
   character(len=90) :: ligne(35)
   integer :: ios, n

   rewind (lw)
   read(lw,'(a)') ligne(1)
   read(lw,'(a)') ligne(2)
   write(ligne(2),'(i0)') Bi

   do n = 3, 22
      read(lw,'(a)',iostat=ios) ligne(n)
      if (ios /= 0) stop ERR_READ_PIV_PARAMETER
   enddo
   rewind(lw)
   do n = 1, 22
      write(lw,'(a)') trim(ligne(n))
   enddo
   rewind (lw)
   read(lw,*)
   read(lw,*)
end subroutine corrige_PIV_param
