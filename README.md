# solveurs FlowPic and Fudaa-LSPIV

Compilation (on a Linux system):

Requirements : CMake, C++ compiler, Fortran compiler

First one must compile the image stabilization module :
$ cd sources/stab_img/
$ cmake .
$ make

Then compile the solver itself.
$ cd ..

You can either create an executable for each compute step :
$ make fudaa_linux
The executables will be in ../exe_fudaa_linux

Or compile all the steps in a unique executable :
$ make appli
The executable will be in ../exe_appli
